<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />    
    <style type="text/css" id="hs-inline-css">

    /* CLIENT-SPECIFIC STYLES */
    body, table, td, a{-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%;font-family:<?php echo $font_family; ?>; color:#555; font-size:14px;} /* Prevent WebKit and Windows mobile changing default text sizes */
    table, td{mso-table-lspace: 0pt; mso-table-rspace: 0pt;font-family:<?php echo $font_family; ?>; color:#555; font-size:14px;} /* Remove spacing between tables in Outlook 2007 and up */
    img{-ms-interpolation-mode: bicubic;} /* Allow smoother rendering of resized image in Internet Explorer */

    /* RESET STYLES */
    img{border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none;}
    table{border-collapse: collapse !important;}
    body{height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important;}

    /* iOS BLUE LINKS */
    a[x-apple-data-detectors] {
        color: inherit !important;
        text-decoration: none !important;
        font-size: inherit !important;
        font-family: inherit !important;
        font-weight: inherit !important;
        line-height: inherit !important;
    }    

    strong {
        color:#222;
    }

    a {
        color:#222;
    }

    .contenttable {
        padding:10px;
    }

    .contenttable_text {
        color:#555555
    }

    .contenttable_text_inner td {
        line-height:1.4em; 
    } 

    .fooevents-pdf-email-text {
        margin:20px 0 !important;
        font-size:14px !important;
        text-align: center !important;
        color:#bbb !important; 
        font-family:<?php echo $font_family; ?> !important;
    }

    @media screen and (max-width: 440px)
    {
        .contenttable {
            width: 100% !important;
        }
        .logo {
            margin: 0 auto !important;
        }
        .code {
            width:100px !important;
        }
        .contentheader td.wide {
            width:100% !important;
            display: block !important;
            text-align: center !important;
        } 
        .contentheader td.wide .code {
            width:auto !important;
            max-width:100% !important;
            display: inline !important;
            text-align: center !important;
        }
        .addtocart {
            width:60% !important;
            margin:10px 20% 0 !important; 
        }
    }

    </style>
</head>
<body style="background:<?php echo $ticket['WooCommerceEventsTicketBackgroundColor']; ?>;">
    <div style="padding:10px; background:<?php echo $ticket['WooCommerceEventsTicketBackgroundColor']; ?>; color:#555; ">