<div id="fooevents-orders-ticket-details">
    <div class="fooevents-notice">
        <p><em><?php _e("Tickets will only be generated when the order is changed to 'Completed'", 'woocommerce-events'); ?></em></p>
    </div>
    <div class="clear"></div>
    <?php foreach($WooCommerceEventsOrderTickets as $event) :?>
        <div class="fooevents-orders-ticket-details-container">
            
            <div class="fooevents-orders-ticket-details-tickets">
                <?php foreach($event['tickets'] as $ticket) : ?>                 
                    <div class="fooevents-orders-ticket-details-tickets-inner">  

                        <div id="fooevents-ticket-details-head">
                            <h1><?php echo(empty($ticket['WooCommerceEventsAttendeeName']))? $ticket['WooCommerceEventsPurchaserFirstName'].' '.$ticket['WooCommerceEventsPurchaserLastName'] : $ticket['WooCommerceEventsAttendeeName'].' '.$ticket['WooCommerceEventsAttendeeLastName']; ?></h1>
                            <div class="clear">&nbsp;</div>
                        </div>                            

                        <table id="fooevents-order-attendee-details" cellpadding="0" cellspacing="0"> 
                            <?php if(!empty($ticket['WooCommerceEventsBookingOptions']['slot'])): ?>
                                <tr>
                                    <td><strong><?php echo sprintf(__( 'Booking %s', 'woocommerce-events'), $ticket['WooCommerceEventsBookingOptions']['slot_term']); ?>:</strong></td>
                                    <td><?php echo $ticket['WooCommerceEventsBookingOptions']['slot']; ?></td>
                                </tr>
                                <tr>
                                    <td><strong><?php echo sprintf(__( 'Booking %s', 'woocommerce-events'), $ticket['WooCommerceEventsBookingOptions']['date_term']); ?>:</strong></td>
                                    <td><?php echo $ticket['WooCommerceEventsBookingOptions']['date']; ?></td>
                                </tr>
                            <?php endif; ?>
                            <?php if(!empty($ticket['WooCommerceEventsSeatingFields'])): ?>
                                <tr>
                                    <td><strong><?php _e('Row:', 'woocommerce-events'); ?></strong></td>
                                    <td><?php echo $ticket['WooCommerceEventsSeatingFields']['fooevents_seat_row_name']?></td>
                                </tr>
                                <tr>
                                    <td><strong><?php _e('Seat:', 'woocommerce-events'); ?></strong></td>
                                    <td><?php echo $ticket['WooCommerceEventsSeatingFields']['fooevents_seat_number']?></td>
                                </tr>
                            <?php endif; ?>                            
                            <?php if(!empty($ticket['WooCommerceEventsAttendeeEmail'])): ?>
                                <tr>
                                    <td><strong><?php _e('Email:', 'woocommerce-events'); ?></strong></td>
                                    <td><a href="mailto:<?php echo($ticket['WooCommerceEventsAttendeeEmail']); ?>"><?php echo($ticket['WooCommerceEventsAttendeeEmail']); ?></a></td>
                                </tr>
                            <?php endif; ?>
                            <?php if(!empty($ticket['WooCommerceEventsAttendeeTelephone'])): ?>
                                <tr>
                                    <td>
                                        <strong><?php _e('Telephone:', 'woocommerce-events'); ?></strong></td>
                                    <td><?php echo(empty($ticket['WooCommerceEventsAttendeeTelephone']))? $ticket['WooCommerceEventsPurchaserPhone'] : $ticket['WooCommerceEventsAttendeeTelephone']; ?></td>
                                </tr>
                            <?php endif; ?>
                            <?php if(!empty($ticket['WooCommerceEventsAttendeeCompany'])): ?>
                                <tr>
                                    <td><strong><?php _e('Company:', 'woocommerce-events'); ?></strong></td>
                                    <td><?php echo(empty($ticket['WooCommerceEventsAttendeeCompany']))? '' : $ticket['WooCommerceEventsAttendeeCompany']; ?></td>
                                </tr>
                            <?php endif; ?>
                            <?php if(!empty($ticket['WooCommerceEventsAttendeeDesignation'])): ?>
                                <tr>
                                    <td><strong><?php _e('Designation:', 'woocommerce-events'); ?></strong></td>
                                    <td><?php echo(empty($ticket['WooCommerceEventsAttendeeDesignation']))? '' : $ticket['WooCommerceEventsAttendeeDesignation']; ?></td>
                                </tr>
                            <?php endif; ?>
                            <?php if(!empty($ticket['WooCommerceEventsVariations']) || !empty($ticket['WooCommerceEventsCustomAttendeeFields'])): ?>
                                <?php if(!empty($ticket['WooCommerceEventsVariations'])): ?>
                                    <?php foreach($ticket['WooCommerceEventsVariations'] as $variation_name => $variation_val): ?>
                                    <tr>
                                        <td><strong><?php echo $variation_name; ?>:</strong></td>
                                        <td><?php echo $variation_val; ?></td>
                                    </tr>
                                    <?php endforeach; ?>
                                    <?php endif; ?>
                                    <?php if(!empty($ticket['WooCommerceEventsCustomAttendeeFields'])): ?>
                                    <?php foreach($ticket['WooCommerceEventsCustomAttendeeFields'] as $key => $field): ?>
                                    <tr>
                                        <td><strong><?php echo $field['field'][$key.'_label']; ?>:</strong></td>
                                        <td><?php echo $field['value']; ?></td>
                                    </tr>
                                    <?php endforeach; ?>
                                <?php endif; ?>                            
                            <?php endif; ?>
                        </table>
                        <div class="clear"></div>
                    </div>
                <?php endforeach; ?>
            </div>
            <div class="fooevents-orders-ticket-details-events">
                <div class="fooevents-orders-ticket-details-events-inner">
                    <h3><?php echo $event['WooCommerceEventsName']; ?></h3>
                    <table class="fooevents-order-event-details"> 
                        <?php if(!empty($event['WooCommerceEventsDate']) && trim($event['WooCommerceEventsDate']) != ''): ?>
                            <tr>
                                <td><strong><?php _e('Date:', 'woocommerce-events'); ?></strong></td>
                                <td><?php echo(empty($event['WooCommerceEventsDate']))? "" : $event['WooCommerceEventsDate']; ?></td>
                            </tr>
                        <?php endif; ?>
                        <?php if(!empty($event['WooCommerceEventsStartTime']) && trim($event['WooCommerceEventsStartTime']) != ''): ?>
                            <tr>
                                <td><strong><?php _e('Time:', 'woocommerce-events'); ?></strong></td>
                                <td><?php echo(empty($event['WooCommerceEventsStartTime']))? "" : $event['WooCommerceEventsStartTime']; ?> <?php echo(empty($event['WooCommerceEventsEndTime']))? "" : ' - '. $event['WooCommerceEventsEndTime']; ?> </td>
                            </tr>
                        <?php endif; ?>
                        <?php if(!empty($event['WooCommerceEventsLocation']) && trim($event['WooCommerceEventsLocation']) != ''): ?>
                            <tr>
                                <td><strong><?php _e('Venue:', 'woocommerce-events'); ?></strong></td>
                                <td><?php echo(empty($event['WooCommerceEventsLocation']))? "" : $event['WooCommerceEventsLocation']; ?></td>
                            </tr> 
                        <?php endif; ?> 
                        <tr>
                            <td colspan="2">
                                <a href="<?php echo $event['WooCommerceEventsURL']; ?>" target="_BLANK" class="button">View</a>
                                <a href="post.php?post=<?php echo $event['WooCommerceEventsProductID'] ?>&action=edit" target="_BLANK" class="button">Edit</a>
                            </td>
                        </tr>                    
                    </table>
                    <div class="clear"></div>
                </div>      
            </div>            
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
    <?php endforeach; ?>
</div>
