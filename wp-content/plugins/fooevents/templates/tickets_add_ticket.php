<div id="fooevents-add-ticket-container">
    <div id="fooevents-ticket-details-inner">

        <h3><?php _e('Event Details', 'woocommerce-events'); ?></h3>
        
        <div id="fooevents-ticket-details">
            <div class="ticket-details-row">
                <label><?php _e('Select Event:', 'woocommerce-events'); ?></label>
                <select name="WooCommerceEventsEvent" id="WooCommerceEventsEvent" class="required fooevents-search-list">
                    <option value="">Please select...</option>
                    <?php foreach($events as $event) :?>
                        <option value="<?php echo $event->ID; ?>"><?php echo $event->post_title; ?> [<?php echo $event->ID; ?>]</option>
                    <?php endforeach; ?>
                </select> 
            </div>
            <div id="fooevents-event-variation-options"></div>
            <div class="clear"></div>  

            <div class="ticket-details-row">
                <h3><?php _e('Purchaser Details', 'woocommerce-events'); ?></h3>

                <div class="ticket-details-group">
                    <label><?php _e('Select Existing User:', 'woocommerce-events'); ?></label>
                    <select name="WooCommerceEventsClientID" id="WooCommerceEventsClientID" class="required fooevents-search-list">
                        <option value="0">Select...</option>
                        <?php foreach($users as $user) :?>
                            <option value="<?php echo $user->ID; ?>"><?php echo $user->data->display_name; ?> - <?php echo $user->data->display_name; ?> [<?php echo $user->ID; ?>]</option>
                        <?php endforeach; ?>
                    </select> 
                </div>

                <div class="ticket-details-group">
                    <label><?php _e('Username:', 'woocommerce-events'); ?></label>
                    <input type="text" name="WooCommerceEventsPurchaserUserName" id="WooCommerceEventsPurchaserUserName" value="" class="required"/>
                </div>

                <div class="ticket-details-group">
                    <label><?php _e('Email:', 'woocommerce-events'); ?></label>
                    <input type="text" name="WooCommerceEventsPurchaserEmail" id="WooCommerceEventsPurchaserEmail" value="" class="required"/>
                </div>

                <div class="ticket-details-group">
                    <label><?php _e('Display Name:', 'woocommerce-events'); ?></label>
                    <input type="text" name="WooCommerceEventsPurchaserFirstName" id="WooCommerceEventsPurchaserFirstName" value=""  class="required"/>
                </div>

            </div>
            <div class="ticket-details-row">
                <h3><?php _e('Attendee Details', 'woocommerce-events'); ?></h3>

                <div class="ticket-details-group">
                    <label><?php _e('First Name:', 'woocommerce-events'); ?></label>
                    <input type="text" name="WooCommerceEventsAttendeeName" id="WooCommerceEventsAttendeeName" value="" class="required"/>
                </div>

                <div class="ticket-details-group">
                    <label><?php _e('Last Name:', 'woocommerce-events'); ?></label>
                    <input type="text" name="WooCommerceEventsAttendeeLastName" id="WooCommerceEventsAttendeeLastName" value="" class="required"/>
                </div>

                <div class="ticket-details-group">
                    <label><?php _e('Email:', 'woocommerce-events'); ?></label>
                    <input type="text" name="WooCommerceEventsAttendeeEmail" id="WooCommerceEventsAttendeeEmail" value="" class="required"/>
                </div>

            </div>
        </div>

        <div class="clear"></div>  

        <table id="fooevents-add-ticket-bookings-container" width="100%">
            <tbody></tbody>
        </table>

        <div class="clear"></div>  

        <div id="fooevents-add-ticket-attendee-fields-container">
        </div>

        <div id="fooevents-add-ticket-seating-container">
        </div>
        <div class="clear"></div>  
    </div>
    <div class="clear"></div>    
</div>
<div id="fooevents-event-details-container">
    <div id="fooevents-event-details">
        <table class="fooevents-event-details" class="form-table">
            <tbody>
                <tr valign="top">
                    <td cosspan="2">
                        <h3><?php _e('Event Overview', 'woocommerce-events'); ?></h3>
                        <i><?php _e('Select an event in the <strong>Event Details</strong> section.', 'woocommerce-events'); ?></i>
                    </td>
                </tr>
            </tbody>
        </table>
        <input type="hidden" name="add_ticket" id="add_ticket" value="true" class="required"/>
        <div class="clear"></div>
    </div>
</div>
<div class="clear"></div>  