<div id="fooevents_expiration" class="panel woocommerce_options_panel fooevents-settings">
    <p><h2><b><?php _e('Event Expiration', 'woocommerce-events'); ?></b></h2></p>
    <div class="options_group">
            <p class="form-field">
                <label><?php _e('Event expiration date:', 'woocommerce-events'); ?></label>
                <input type="text" id="WooCommerceEventsExpire" name="WooCommerceEventsExpire" value="<?php echo esc_attr($WooCommerceEventsExpire); ?>"/>
                <img class="help_tip" data-tip="<?php _e("The date when the event will automatically expire.", 'woocommerce-events'); ?>" src="<?php echo plugins_url(); ?>/woocommerce/assets/images/help.png" height="16" width="16" />
            </p>
    </div>
    <div class="options_group">
        <p class="form-field">
               <label><?php _e('Expiration message:', 'woocommerce-events'); ?></label>
               <?php wp_editor($WooCommerceEventsExpireMessage, 'WooCommerceEventsExpireMessage'); ?>
               <img class="help_tip" data-tip="<?php _e("The message that will be displayed on the product page when the event has expired.", 'woocommerce-events'); ?>" src="<?php echo plugins_url(); ?>/woocommerce/assets/images/help.png" height="16" width="16" />
        </p>
    </div>
    <p><h2><b><?php _e('Ticket Expiration', 'woocommerce-events'); ?></b></h2></p>
    <div class="options_group">
        <p class="form-field">
               <label><?php _e('Expiration type:', 'woocommerce-events'); ?></label>
               <input type="radio" name="WooCommerceEventsTicketExpirationType" value="select" <?php echo ($WooCommerceEventsTicketExpirationType !== 'time')? 'CHECKED' : '' ?>/> <?php _e('Fixed date', 'woocommerce-events'); ?> <br />
               <input type="radio" name="WooCommerceEventsTicketExpirationType" value="time" <?php echo ($WooCommerceEventsTicketExpirationType == 'time')? 'CHECKED' : '' ?>/> <?php _e('Elapsed time', 'woocommerce-events'); ?> <br />
               <img class="help_tip" data-tip="<?php _e('Select either a fixed date or elapsed time since the ticket was purchased to automatically expire tickets.', 'woocommerce-events'); ?>" src="<?php echo plugins_url(); ?>/woocommerce/assets/images/help.png" height="16" width="16" />
        </p>
    </div>
    <div class="options_group">
            <p class="form-field">
                <label><?php _e('Select fixed date:', 'woocommerce-events'); ?></label>
                <input type="text" id="WooCommerceEventsTicketsExpireSelect" name="WooCommerceEventsTicketsExpireSelect" value="<?php echo esc_attr($WooCommerceEventsTicketsExpireSelect); ?>"/>
                <img class="help_tip" data-tip="<?php _e("Select the fixed ticket expiration date.", 'woocommerce-events'); ?>" src="<?php echo plugins_url(); ?>/woocommerce/assets/images/help.png" height="16" width="16" />
            </p>
    </div>
    <div class="options_group">
        <p class="form-field">
            <label><?php _e('Select elapsed time:', 'woocommerce-events'); ?></label>
            <select name="WooCommerceEventsTicketsExpireValue" id="WooCommerceEventsTicketsExpireValue">
                <?php for($x=1; $x<=60; $x++) :?>
                <option value="<?php echo $x; ?>" <?php echo ($WooCommerceEventsTicketsExpireValue == $x) ? 'SELECTED' : ''; ?>><?php echo $x; ?></option>
                <?php endfor; ?>
            </select>
            <select name="WooCommerceEventsTicketsExpireUnit" id="WooCommerceEventsTicketsExpireUnit">
                <option value="year" <?php echo ($WooCommerceEventsTicketsExpireUnit == 'year') ? 'SELECTED' : ''; ?>><?php _e("Years", 'woocommerce-events'); ?></option>
                <option value="month" <?php echo ($WooCommerceEventsTicketsExpireUnit == 'month') ? 'SELECTED' : ''; ?>><?php _e("Months", 'woocommerce-events'); ?></option>
                <option value="week" <?php echo ($WooCommerceEventsTicketsExpireUnit == 'week') ? 'SELECTED' : ''; ?>><?php _e("Weeks", 'woocommerce-events'); ?></option>
                <option value="day" <?php echo ($WooCommerceEventsTicketsExpireUnit == 'day') ? 'SELECTED' : ''; ?>><?php _e("Days", 'woocommerce-events'); ?></option>
                <option value="hour" <?php echo ($WooCommerceEventsTicketsExpireUnit == 'hour') ? 'SELECTED' : ''; ?>><?php _e("Hours", 'woocommerce-events'); ?></option>
                <option value="minute" <?php echo ($WooCommerceEventsTicketsExpireUnit == 'minute') ? 'SELECTED' : ''; ?>><?php _e("Minutes", 'woocommerce-events'); ?></option>
            </select>
            <img class="help_tip" data-tip="<?php _e("Select the unit of time to be used for elapsed time.", 'woocommerce-events'); ?>" src="<?php echo plugins_url(); ?>/woocommerce/assets/images/help.png" height="16" width="16" />
        </p>
    </div>
    <?php echo $bookingsExpirationOptions; ?>
</div>