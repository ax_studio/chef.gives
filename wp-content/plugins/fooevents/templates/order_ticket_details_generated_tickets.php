<div id="fooevents-orders-ticket-details">
    <div class="fooevents-notice">
        <p><em><?php _e("The following ticket/s have been generated for this order.", 'woocommerce-events'); ?></em></p>
    </div>
    <div class="clear"></div>
    <?php foreach($WooCommerceEventsOrderTickets as $event) :?>
        <div class="fooevents-orders-ticket-details-container">

            <div class="fooevents-orders-ticket-details-tickets">
                <?php foreach($event['tickets'] as $ticket) : ?>
                    <div class="fooevents-orders-ticket-details-tickets-inner"> 

                        <div id="fooevents-ticket-details-head">
                            <img src="<?php echo $this->Config->barcodeURL.$ticket["WooCommerceEventsTicketHash"].'-'.$ticket['WooCommerceEventsTicketID'] ?>.png" class="ticket-code" />
                            <?php if(!empty($ticket['WooCommerceEventsAttendeeName']) && !empty($ticket['WooCommerceEventsAttendeeLastName'])): ?>
                            <h1><?php echo $ticket['WooCommerceEventsAttendeeName'].' '.$ticket['WooCommerceEventsAttendeeLastName']; ?></h1>  
                            <?php else: ?>
                            <h1><?php echo $ticket['customerFirstName'].' '.$ticket['customerLastName']; ?></h1>  
                            <?php endif; ?>
                            <h3><a href="post.php?post=<?php echo $ticket['ID'] ?>&action=edit" target="_BLANK">#<?php echo $ticket['WooCommerceEventsTicketID']; ?></a></h3>
                            <div class="clear"></div>
                        </div> 
                            
                        <table id="fooevents-order-attendee-details" cellpadding="0" cellspacing="0"> 
                            <?php if(!empty($ticket['WooCommerceEventsBookingSlot'])): ?> 
                                <tr>
                                    <td><strong><?php echo $ticket['WooCommerceEventsBookingsSlotTerm']; ?>:</strong></td>
                                    <td><?php echo $ticket['WooCommerceEventsBookingSlot']; ?></td>
                                </tr>
                                <tr>
                                    <td><strong><?php echo $ticket['WooCommerceEventsBookingsDateTerm']; ?>:</strong></td>
                                    <td><?php echo $ticket['WooCommerceEventsBookingDate']; ?></td>
                                </tr>
                            <?php endif; ?>
                            <?php if(!empty($ticket['WooCommerceEventsSeatingFields'])): ?>
                                <tr>
                                    <td><strong><?php _e('Row:', 'woocommerce-events'); ?></strong></td>
                                    <td><?php echo $ticket['WooCommerceEventsSeatingFields']['fooevents_seat_row_name']?></span></td>
                                </tr>
                                <tr>
                                    <td><strong><?php _e('Seat:', 'woocommerce-events'); ?></strong></td>
                                    <td><?php echo $ticket['WooCommerceEventsSeatingFields']['fooevents_seat_number']?></td>
                                </tr>
                            <?php endif; ?>
                            <?php if(!empty($ticket['WooCommerceEventsAttendeeEmail'])): ?>
                                <tr>
                                    <td><strong><?php _e('Email:', 'woocommerce-events'); ?></strong></td>
                                    <td><a href="mailto:<?php echo($ticket['WooCommerceEventsAttendeeEmail']); ?>"><?php echo($ticket['WooCommerceEventsAttendeeEmail']); ?></a></td>
                                </tr>
                            <?php endif; ?>
                            <?php if(!empty($ticket['WooCommerceEventsAttendeeTelephone'])): ?>
                                <tr>
                                    <td><strong><?php _e('Telephone:', 'woocommerce-events'); ?></strong></td>
                                    <td><?php echo(empty($ticket['WooCommerceEventsAttendeeTelephone']))? '' : $ticket['WooCommerceEventsAttendeeTelephone']; ?></td>
                                </tr>
                            <?php endif; ?>
                            <?php if(!empty($ticket['WooCommerceEventsAttendeeCompany'])): ?>
                            <tr>
                                <td><strong><?php _e('Company:', 'woocommerce-events'); ?></strong></td>
                                <td><?php echo(empty($ticket['WooCommerceEventsAttendeeCompany']))? '' : $ticket['WooCommerceEventsAttendeeCompany']; ?></td>
                            </tr>
                            <?php endif; ?>
                            <?php if(!empty($ticket['WooCommerceEventsAttendeeDesignation'])): ?>
                            <tr>
                                <td><strong><?php _e('Designation:', 'woocommerce-events'); ?></strong></td>
                                <td><?php echo(empty($ticket['WooCommerceEventsAttendeeDesignation']))? '' : $ticket['WooCommerceEventsAttendeeDesignation']; ?></td>
                            </tr>
                            <?php endif; ?>
                            
                            <?php if(!empty($ticket['WooCommerceEventsVariations']) || !empty($ticket['WooCommerceEventsCustomAttendeeFields'])): ?>
                            <?php if(!empty($ticket['WooCommerceEventsVariations'])): ?>
                            <?php foreach($ticket['WooCommerceEventsVariations'] as $variation_name => $variation_val): ?>
                            <tr>
                                <td><strong><?php echo $variation_name; ?>:</strong></td>
                                <td><?php echo $variation_val; ?></td>
                            </tr>
                            <?php endforeach; ?>
                            <?php endif; ?>
                            <?php if(!empty($ticket['WooCommerceEventsCustomAttendeeFields'])): ?>
                            <?php foreach($ticket['WooCommerceEventsCustomAttendeeFields'] as $key => $field): ?>
                            <tr>
                                <td><strong><?php echo $field['field'][$key.'_label']; ?>:</strong></td>
                                <td><?php echo $field['value']; ?></td>
                            </tr>
                            <?php endforeach; ?>
                            <?php endif; ?>
                            <?php endif; ?>
                        </table>
                        <div class="clear"></div>
                    </div>
                <?php endforeach; ?>
            </div>
            <div class="fooevents-orders-ticket-details-events">
                <div class="fooevents-orders-ticket-details-events-inner">
                    <h3><?php echo $event['WooCommerceEventsName']; ?></h3>
                        <table id="fooevents-order-attendee-details" cellpadding="0" cellspacing="0"> 
                        <?php if(!empty($event['WooCommerceEventsDate']) && trim($event['WooCommerceEventsDate']) != ''): ?>
                            <tr>
                                <td><strong><?php _e('Date:', 'woocommerce-events'); ?></strong></td>
                                <td><?php echo(empty($event['WooCommerceEventsDate']))? "" : $event['WooCommerceEventsDate']; ?></td>
                            </tr>
                        <?php endif; ?>
                        <?php if(!empty($event['WooCommerceEventsStartTime']) && trim($event['WooCommerceEventsStartTime']) != ''): ?>
                            <tr>
                                <td><strong><?php _e('Time:', 'woocommerce-events'); ?></strong></td>
                                <td><?php echo(empty($event['WooCommerceEventsStartTime']))? "" : $event['WooCommerceEventsStartTime']; ?> <?php echo(empty($event['WooCommerceEventsEndTime']))? "" : ' - '. $event['WooCommerceEventsEndTime']; ?> </td>
                            </tr>
                        <?php endif; ?>
                        <?php if(!empty($event['WooCommerceEventsLocation']) && trim($event['WooCommerceEventsLocation']) != ''): ?>
                            <tr>
                                <td><strong><?php _e('Venue:', 'woocommerce-events'); ?></strong></td>
                                <td><?php echo(empty($event['WooCommerceEventsLocation']))? "" : $event['WooCommerceEventsLocation']; ?></td>
                            </tr> 
                        <?php endif; ?>
                        <tr>
                            <td colspan="2">
                                <a href="<?php echo $event['WooCommerceEventsURL']; ?>" target="_BLANK" class="button">View</a>
                                <a href="post.php?post=<?php echo $event['WooCommerceEventsProductID'] ?>&action=edit" target="_BLANK" class="button">Edit</a>
                            </td>
                        </tr>
                    </table>
                    <div class="clear"></div>
                </div>
            </div>
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
    <?php endforeach; ?>
</div>