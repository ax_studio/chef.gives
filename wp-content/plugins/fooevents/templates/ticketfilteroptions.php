<select name="event_id">
    <option value=""><?php esc_attr_e('All Products', 'woocommerce-events'); ?></option>
    <?php foreach($events as $event) :?>
    <option value="<?php echo $event->ID; ?>" <?php echo ($event_id == $event->ID)? 'SELECTED' : '' ?>><?php echo $event->post_title; ?></option>
    <?php endforeach; ?>
</select>