<div id="fooevents-ticket-details-container">
    <div id="fooevents-ticket-details-inner">
        <div id="fooevents-ticket-details-head">
            <img src="<?php echo $barcode_url.$ticket['WooCommerceEventsTicketHash'].'-'.$ticket['WooCommerceEventsTicketID'].'.png'; ?>" class="ticket-code" />
            <h1><?php echo $ticket['WooCommerceEventsAttendeeName']." ".$ticket['WooCommerceEventsAttendeeLastName']; ?></h1>
            <h3>#<?php echo $ticket['WooCommerceEventsTicketID']; ?></h3>

            <?php echo $pdf_ticket_link; ?>
            <div class="clear"></div>
        </div>
        <div id="fooevents-ticket-details">
            <div class="ticket-details-row">
                <label><?php _e('First Name:', 'woocommerce-events'); ?></label>
                <input type="text" name="WooCommerceEventsAttendeeName" value="<?php echo $ticket['WooCommerceEventsAttendeeName']; ?>" />
            </div>
            <div class="ticket-details-row">
                <label><?php _e('Last Name:', 'woocommerce-events'); ?></label>
                <input type="text" name="WooCommerceEventsAttendeeLastName" value="<?php echo $ticket['WooCommerceEventsAttendeeLastName']; ?>" />
            </div>
            <div class="ticket-details-row">
                <label><?php _e('Email:', 'woocommerce-events'); ?></label>
                <input type="text" name="WooCommerceEventsAttendeeEmail" value="<?php echo $ticket['WooCommerceEventsAttendeeEmail']; ?>" />
            </div>
            <div class="ticket-details-row">
                <label><?php _e('Telephone:', 'woocommerce-events'); ?></label>
                <input type="text" name="WooCommerceEventsAttendeeTelephone" value="<?php echo $ticket['WooCommerceEventsAttendeeTelephone']; ?>" />
            </div>
            <div class="ticket-details-row">
                <label><?php _e('Company:', 'woocommerce-events'); ?></label>
                <input type="text" name="WooCommerceEventsAttendeeCompany" value="<?php echo $ticket['WooCommerceEventsAttendeeCompany']; ?>" />
            </div>
            <div class="ticket-details-row">
                <label><?php _e('Designation:', 'woocommerce-events'); ?></label>
                <input type="text" name="WooCommerceEventsAttendeeDesignation" value="<?php echo $ticket['WooCommerceEventsAttendeeDesignation']; ?>" />
            </div>
            <div class="clear"></div>
            <?php echo $booking_options; ?>
            <?php echo $custom_attendee_options; ?>
            <?php echo $seating_options; ?>
        </div>
        <div class="clear"></div>
    </div>
    <div class="clear"></div>
    <input type="hidden" id="fooevents_ticket_raw_id" name="fooevents_ticket_raw_id" value="<?php echo $post->ID; ?>" />
    <input type="hidden" id="fooevents_event_id" name="fooevents_event_id" value="<?php echo $ticket['WooCommerceEventsProductID']; ?>" />
</div>
<div id="fooevents-event-details-container">
    <table id="fooevents-event-details">

        <?php if(!empty($ticket['WooCommerceEventsVariations'])): ?>
            <tr>
                <td colspan="2">
                    <h3><?php _e('Variation', 'woocommerce-events'); ?></h3>
                </td>
            </tr>
            <?php foreach($ticket['WooCommerceEventsVariations'] as $variationNameOutput => $variationValue): ?>
            <tr>
                <td>
                    <strong><?php echo $variationNameOutput; ?>:</strong>
                </td>
                <td>
                    <?php echo $variationValue;?>
                </td>
            </tr>        
            <?php endforeach; ?>
        <?php endif; ?>  

        <tr>
            <td colspan="2">
                <h3><?php echo $ticket['WooCommerceEventsName']; ?></h3>
            </td>
        </tr>
        <?php if(!empty($ticket['WooCommerceEventsDate']) && trim($ticket['WooCommerceEventsDate']) != ''): ?>
        <tr>
            <td>
                <strong><?php _e('Date: ', 'woocommerce-events'); ?></strong>
            </td>
            <td>
                <?php echo $ticket['WooCommerceEventsDate'];  ?>
            </td>
        </tr>
        <?php endif; ?>     
        <?php if(!empty($ticket['WooCommerceEventsStartTime']) && trim($ticket['WooCommerceEventsStartTime']) != ''): ?>
            <tr>
                <td><strong><?php _e('Time:', 'woocommerce-events'); ?></strong></td>
                <td><?php echo(empty($ticket['WooCommerceEventsStartTime']))? "" : $ticket['WooCommerceEventsStartTime']; ?> <?php echo(empty($ticket['WooCommerceEventsEndTime']))? "" : ' - '. $ticket['WooCommerceEventsEndTime']; ?> </td>
            </tr>
        <?php endif; ?>
        <?php if(!empty($ticket['WooCommerceEventsLocation']) && trim($ticket['WooCommerceEventsLocation']) != ''): ?>        
        <tr>
            <td>
                <strong><?php _e('Venue: ', 'woocommerce-events'); ?></strong>
            </td>
            <td>
                <?php echo $ticket['WooCommerceEventsLocation']; ?>
            </td>
        </tr>
        <?php endif; ?>
        <tr>
            <td colspan="2">
                <a href="<?php echo $ticket['WooCommerceEventsURL']; ?>" target="_BLANK" class="button">View</a> <a href="post.php?post=<?php echo $ticket['WooCommerceEventsProductID'] ?>&action=edit" target="_BLANK" class="button">Edit</a>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <h3><?php _e('Order Details', 'woocommerce-events'); ?></h3>
            </td>
        </tr>
        <tr>
            <td>
                <strong><?php _e('Order ID: ', 'woocommerce-events'); ?></strong>
            </td>
            <td>
                <a href="post.php?post=<?php echo $ticket['WooCommerceEventsOrderID']; ?>&action=edit" target="_BLANK">#<?php echo $ticket['WooCommerceEventsOrderID']; ?></a></td>
        </tr>
        <?php if(!empty($ticket['WooCommerceEventsOrderStatus']) && trim($ticket['WooCommerceEventsOrderStatus']) != ''): ?>   
        <tr>
            <td>
                <strong><?php _e('Order Status: ', 'woocommerce-events'); ?></strong>
            </td>
            <td>
                <?php echo $ticket['WooCommerceEventsOrderStatus']; ?>
            </td>
        </tr>
        <?php endif; ?>
        <?php if(!empty($ticket['WooCommerceEventsOrderTotal']) && trim($ticket['WooCommerceEventsOrderTotal']) != ''): ?>   
        <tr>
            <td>
                <strong><?php _e('Order Amount: ', 'woocommerce-events'); ?></strong>
            </td>
            <td>
                <?php echo $ticket['WooCommerceEventsOrderTotal']; ?>
            </td>
        </tr>
        <?php endif; ?>
        <tr>
            <td colspan="2">
                <h3><?php _e('Purchaser Details', 'woocommerce-events'); ?></h3>
            </td>
        </tr>
        <?php if(!empty($ticket['customerFirstName']) && trim($ticket['customerFirstName']) != ''): ?> 
        <tr>
            <td>
                <strong><?php _e('Name: ', 'woocommerce-events'); ?></strong>
            </td>
            <td>
                <a href="user-edit.php?user_id=<?php echo $ticket['customerID']; ?>" target="_BLANK">
                <?php echo $ticket['customerFirstName']; ?> <?php echo $ticket['customerLastName']; ?> [#<?php echo $ticket['customerID']; ?>]</a>
            </td>
        </tr>
        <?php endif; ?>
        <?php if(!empty($ticket['customerPhone']) && trim($ticket['customerPhone']) != ''): ?> 
        <tr>
            <td>
                <strong><?php _e('Phone: ', 'woocommerce-events'); ?></strong>
            </td>
            <td>
                <?php echo $ticket['customerPhone']; ?>
            </td>
        </tr>
        <?php endif; ?>
        <?php if(!empty($ticket['customerEmail']) && trim($ticket['customerEmail']) != ''): ?> 
        <tr>
            <td>
                <strong><?php _e('Email: ', 'woocommerce-events'); ?></strong>
            </td>
            <td>
                <a href="mailto:<?php echo $ticket['customerEmail']; ?>"><?php echo $ticket['customerEmail']; ?></a>
            </td>
        </tr>
        <?php endif; ?> 
    </table>
    <div class="clear"></div>
</div>
<div class="clear"></div>
