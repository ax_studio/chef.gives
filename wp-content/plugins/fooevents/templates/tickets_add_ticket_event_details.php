<tr valign="top">
    <td colspan="2">
        <h3><?php echo $event['WooCommerceEventsName']; ?></h3> 
    </td>
</tr>
<tr valign="top">
    <td>
        <strong><?php _e('Date: ', 'woocommerce-events'); ?></strong>
    </td>
    <td>
        <?php echo $event['WooCommerceEventsDate'];  ?>
    </td>
</tr>
<tr valign="top">
    <td>
        <strong><?php _e('Start time: ', 'woocommerce-events'); ?></strong>
    </td>
    <td>
         <?php echo $event['WooCommerceEventsStartTime']; ?> <?php echo(!empty($event['WooCommerceEventsPeriod'])) ? $event['WooCommerceEventsPeriod'] : ''; ?>
    </td>
</tr>
<tr valign="top">
    <td>
        <strong><?php _e('End time: ', 'woocommerce-events'); ?></strong>
    </td>
    <td>
        <?php echo $event['WooCommerceEventsEndTime']; ?> <?php echo(!empty($event['WooCommerceEventsEndPeriod'])) ? $event['WooCommerceEventsEndPeriod'] : ''; ?>
    </td>
</tr>
<tr valign="top">
    <td>
        <strong><?php _e('Venue: ', 'woocommerce-events'); ?></strong>
    </td>
    <td>
        <?php echo $event['WooCommerceEventsLocation']; ?>
    </td>
</tr>
<tr valign="top">
    <td>
        <strong><?php _e('GPS Coordinates: ', 'woocommerce-events'); ?></strong>
    </td>
    <td>
        <?php echo $event['WooCommerceEventsGPS']; ?>
    </td>
</tr>
<tr valign="top">
    <td>
        <strong><?php _e('Phone: ', 'woocommerce-events'); ?></strong> 
    </td>
    <td>
        <?php echo $event['WooCommerceEventsSupportContact']; ?>
    </td>
</tr>
<tr valign="top">
    <td>
        <strong><?php _e('Email: ', 'woocommerce-events'); ?></strong> 
    </td>
    <td>
        <a href="mailto:<?php echo $event['WooCommerceEventsEmail']; ?>"><?php echo $event['WooCommerceEventsEmail']; ?></a>
    </td>
</tr>
<tr>
    <td colspan="2">
        <a href="<?php echo $event['WooCommerceEventsURL']; ?>" target="_BLANK" class="button">View</a> <a href="post.php?post=<?php echo $event['WooCommerceEventsProductID'] ?>&action=edit" target="_BLANK" class="button">Edit</a>
    </td>
</tr>