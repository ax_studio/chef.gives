<div id="fooevents_options" class="panel woocommerce_options_panel">
    <p><h2><b><?php _e('Event Settings', 'fooevents-custom-attendee-fields'); ?></b></h2></p>
    <div class="options_group">
            <p class="form-field">
                   <label><?php _e('Is this product an event?', 'woocommerce-events'); ?></label>
                   <select name="WooCommerceEventsEvent" id="WooCommerceEventsProductIsEvent">
                        <option value="NotEvent" <?php echo ($WooCommerceEventsEvent == 'NotEvent')? 'SELECTED' : '' ?>><?php _e('No', 'woocommerce-events'); ?></option>
                        <option value="Event" <?php echo ($WooCommerceEventsEvent == 'Event')? 'SELECTED' : '' ?>><?php _e('Yes', 'woocommerce-events'); ?></option>
                   </select>
                   <img class="help_tip" data-tip="<?php _e('This option enables event and ticketing functionality.', 'woocommerce-events'); ?>" src="<?php echo plugins_url(); ?>/woocommerce/assets/images/help.png" height="16" width="16" />
            </p>
    </div>
    <div id="WooCommerceEventsForm">
        <div class="options_group" id ="WooCommerceEventsTypeHolder">
            <p class="form-field">
                <label><?php _e('Event type:', 'woocommerce-events'); ?></label>
                <input type="radio" name="WooCommerceEventsType" value="single" <?php echo ($WooCommerceEventsType == 'single')? 'CHECKED' : '' ?>> <?php _e('Single', 'woocommerce-events'); ?><img class="help_tip" data-tip="<?php _e("Standard one-day events.", 'fooevents-multiday-events'); ?>" src="<?php echo plugins_url(); ?>/woocommerce/assets/images/help.png" height="16" width="16" target="new" /><br>
                <input type="radio" name="WooCommerceEventsType" value="sequential" <?php echo ($WooCommerceEventsType == 'sequential')? 'CHECKED' : '' ?> <?php echo($multi_day_enabled == true)? '' : 'DISABLED'; ?>> <?php _e('Sequential days', 'woocommerce-events'); ?><a href="https://www.fooevents.com/products/fooevents-multi-day/"><img src="<?php echo plugins_url(); ?>/woocommerce/assets/images/help.png" class="help_tip" data-tip="<?php _e("Events that occur over multiple days and repeat for a set number of sequential days. Note: Requires the FooEvents Multi-day plugin.", 'fooevents-multiday-events'); ?>" height="16" width="16" target="new" /></a><br> 
                <input type="radio" name="WooCommerceEventsType" value="select" <?php echo ($WooCommerceEventsType == 'select')? 'CHECKED' : '' ?> <?php echo($multi_day_enabled == true)? '' : 'DISABLED'; ?>> <?php _e('Select days', 'woocommerce-events'); ?><a href="https://www.fooevents.com/products/fooevents-multi-day/"><img src="<?php echo plugins_url(); ?>/woocommerce/assets/images/help.png" class="help_tip" data-tip="<?php _e("Events that repeat over multiple calendar days. Note: Requires the FooEvents Multi-day plugin.", 'fooevents-multiday-events'); ?>" height="16" width="16" target="new" /></a><br> 
                <input type="radio" name="WooCommerceEventsType" value="bookings" <?php echo ($WooCommerceEventsType == 'bookings')? 'CHECKED' : '' ?> <?php echo($bookings_enabled == true)? '' : 'DISABLED'; ?>> <?php _e('Bookable', 'woocommerce-events'); ?><a href="https://www.fooevents.com/fooevents-bookings/"><img src="<?php echo plugins_url(); ?>/woocommerce/assets/images/help.png" class="help_tip" data-tip="<?php _e("Events that require customers to select from available date and time slots (bookings and repeat events). Requires the FooEvents Bookings plugin.", 'fooevents-multiday-events'); ?>" height="16" width="16" target="new" /></a><br> 
                <input type="radio" name="WooCommerceEventsType" value="seating" <?php echo ($WooCommerceEventsType == 'seating')? 'CHECKED' : '' ?> <?php echo($seating_enabled == true)? '' : 'DISABLED'; ?>> <?php _e('Seating', 'woocommerce-events'); ?><a href="https://www.fooevents.com/fooevents-seating/"><img src="<?php echo plugins_url(); ?>/woocommerce/assets/images/help.png" class="help_tip" data-tip="<?php _e("Events that include the ability for customers to select row and seat numbers from a seating chart. Requires the FooEvents Seating plugin.", 'fooevents-multiday-events'); ?>" height="16" width="16" target="new" /></a><br> 
                
            </p>
        </div>
        <?php echo $numDays; ?>
        <?php echo $multiDaySelectDateContainer; ?>
        <div class="options_group" id="WooCommerceEventsDateContainer">
                <p class="form-field">
                       <label><?php _e('Start date:', 'woocommerce-events'); ?></label>
                       <input type="text" id="WooCommerceEventsDate" name="WooCommerceEventsDate" value="<?php echo esc_attr($WooCommerceEventsDate); ?>"/>
                       <img class="help_tip" data-tip="<?php _e("The date that the event is scheduled to take place. This is used as a label on your website and it's also used by the FooEvents Calendar to display the event.", 'woocommerce-events'); ?>" src="<?php echo plugins_url(); ?>/woocommerce/assets/images/help.png" height="16" width="16" />
                </p>
        </div>
        <?php echo $endDate; ?>
        <div class="options_group" id="WooCommerceEventsTimeContainer">
                <p class="form-field">
                        <label><?php _e('Start time:', 'woocommerce-events'); ?></label>
                        <select name="WooCommerceEventsHour" id="WooCommerceEventsHour">
                            <?php for($x=0; $x<=23; $x++) :?>
                            <?php $x = sprintf("%02d", $x); ?>
                            <option value="<?php echo $x; ?>" <?php echo ($WooCommerceEventsHour == $x) ? 'SELECTED' : ''; ?>><?php echo $x; ?></option>
                            <?php endfor; ?>
                        </select>
                        <select name="WooCommerceEventsMinutes" id="WooCommerceEventsMinutes">
                            <?php for($x=0; $x<=59; $x++) :?>
                            <?php $x = sprintf("%02d", $x); ?>
                            <option value="<?php echo $x; ?>" <?php echo ($WooCommerceEventsMinutes == $x) ? 'SELECTED' : ''; ?>><?php echo $x; ?></option>
                            <?php endfor; ?>
                        </select>
                        <select name="WooCommerceEventsPeriod" id="WooCommerceEventsPeriod" <?php echo ($WooCommerceEventsHour > 12 || $WooCommerceEventsHourEnd > 12) ? 'disabled' : ''; ?>>
                            <option value="">-</option>
                            <option value="a.m." <?php echo ($WooCommerceEventsHour <= 12 && $WooCommerceEventsHourEnd <= 12 && $WooCommerceEventsPeriod == 'a.m.') ? 'SELECTED' : ''; ?>>a.m.</option>
                            <option value="p.m." <?php echo ($WooCommerceEventsHour <= 12 && $WooCommerceEventsHourEnd <= 12 && $WooCommerceEventsPeriod == 'p.m.') ? 'SELECTED' : ''; ?>>p.m.</option>
                        </select>
                        <img class="help_tip" data-tip="<?php _e('The time that the event is scheduled to start.', 'woocommerce-events'); ?>" src="<?php echo plugins_url(); ?>/woocommerce/assets/images/help.png" height="16" width="16" />
                </p>
        </div>
        <div class="options_group" id="WooCommerceEventsEndTimeContainer">
                <p class="form-field">
                        <label><?php _e('End time:', 'woocommerce-events'); ?></label>
                        <select name="WooCommerceEventsHourEnd" id="WooCommerceEventsHourEnd">
                            <?php for($x=0; $x<=23; $x++) :?>
                            <?php $x = sprintf("%02d", $x); ?>
                            <option value="<?php echo $x; ?>" <?php echo ($WooCommerceEventsHourEnd == $x) ? 'SELECTED' : ''; ?>><?php echo $x; ?></option>
                            <?php endfor; ?>
                        </select>
                        <select name="WooCommerceEventsMinutesEnd" id="WooCommerceEventsMinutesEnd">
                            <?php for($x=0; $x<=59; $x++) :?>
                            <?php $x = sprintf("%02d", $x); ?>
                            <option value="<?php echo $x; ?>" <?php echo ($WooCommerceEventsMinutesEnd == $x) ? 'SELECTED' : ''; ?>><?php echo $x; ?></option>
                            <?php endfor; ?>
                        </select>
                        <select name="WooCommerceEventsEndPeriod" id="WooCommerceEventsEndPeriod" <?php echo ($WooCommerceEventsHour > 12 || $WooCommerceEventsHourEnd > 12) ? 'disabled' : ''; ?>>
                            <option value="">-</option>
                            <option value="a.m." <?php echo ($WooCommerceEventsHour <= 12 && $WooCommerceEventsHourEnd <= 12 && $WooCommerceEventsEndPeriod == 'a.m.') ? 'SELECTED' : ''; ?>>a.m.</option>
                            <option value="p.m." <?php echo ($WooCommerceEventsHour <= 12 && $WooCommerceEventsHourEnd <= 12 && $WooCommerceEventsEndPeriod == 'p.m.') ? 'SELECTED' : ''; ?>>p.m.</option>
                        </select>
                        <img class="help_tip" data-tip="<?php _e('The time that the event is scheduled to end.', 'woocommerce-events'); ?>" src="<?php echo plugins_url(); ?>/woocommerce/assets/images/help.png" height="16" width="16" />
                </p>
        </div>
        <div class="options_group" id="WooCommerceEventsTimezoneContainer">
                <p class="form-field">
                        <label><?php _e('Time zone:', 'woocommerce-events'); ?></label>
                        <select name="WooCommerceEventsTimeZone" id="WooCommerceEventsTimeZone">
                            <option value="" <?php echo ( $WooCommerceEventsTimeZone == '' ) ? 'SELECTED' : ''?>>(Not set)</option>
                        <?php foreach($tzlist as $tz) : ?>
                            <option value="<?php echo $tz; ?>" <?php echo ($WooCommerceEventsTimeZone == $tz) ? 'SELECTED' : ''?>><?php echo str_replace("_", " ", str_replace("/", " / ", $tz)); ?></option>
                        <?php endforeach; ?>     
                        </select>
                        <img class="help_tip" data-tip="<?php _e("The time zone where the event is taking place. If no time zone is set then the attendee's local time zone will be used for the 'Add to Calendar' functionality in the ticket email.", 'woocommerce-events'); ?>" src="<?php echo plugins_url(); ?>/woocommerce/assets/images/help.png" height="16" width="16" />
                </p>
        </div>
        <?php echo $eventbrite_option; ?>
        <div class="options_group">
                <p class="form-field">
                       <label><?php _e('Venue:', 'woocommerce-events'); ?></label>
                       <input type="text" id="WooCommerceEventsLocation" name="WooCommerceEventsLocation" value="<?php echo esc_attr($WooCommerceEventsLocation); ?>"/>
                       <img class="help_tip" data-tip="<?php _e('The venue where the event will be hosted.', 'woocommerce-events'); ?>" src="<?php echo plugins_url(); ?>/woocommerce/assets/images/help.png" height="16" width="16" />
                </p>
        </div>
        <div class="options_group">
                <p class="form-field">
                       <label><?php _e('GPS coordinates:', 'woocommerce-events'); ?></label>
                       <input type="text" id="WooCommerceEventsGPS" name="WooCommerceEventsGPS" value="<?php echo esc_attr($WooCommerceEventsGPS); ?>"/>
                       <img class="help_tip" data-tip="<?php _e("GPS coordinates for the venue.", 'woocommerce-events'); ?>" src="<?php echo plugins_url(); ?>/woocommerce/assets/images/help.png" height="16" width="16" />
                </p>
        </div>
        <div class="options_group">
                <p class="form-field">
                       <label><?php _e('Google Maps coordinates:', 'woocommerce-events'); ?></label>
                       <input type="text" id="WooCommerceEventsGoogleMaps" name="WooCommerceEventsGoogleMaps" value="<?php echo esc_attr($WooCommerceEventsGoogleMaps); ?>"/>
                       <img class="help_tip" data-tip="<?php _e('GPS coordinates that are used to calculate the pin position for Google Maps on the event page. A valid Google Maps API key must first be saved in FooEvents settings.', 'woocommerce-events'); ?>" src="<?php echo plugins_url(); ?>/woocommerce/assets/images/help.png" height="16" width="16" />
                       <?php if(empty($globalWooCommerceEventsGoogleMapsAPIKey)) :?>
                       <br /><br />
                       <?php _e('Google Maps API key not set.','woocommerce-events'); ?> <a href="admin.php?page=fooevents-settings&tab=integration"><?php _e('Please check the Event Integration settings.', 'woocommerce-events'); ?></a>
                       <?php endif; ?>
                </p>
        </div>
        <div class="options_group">
                <p class="form-field">
                       <label><?php _e('Directions:', 'woocommerce-events'); ?></label>
                       <textarea name="WooCommerceEventsDirections" id="WooCommerceEventsDirections"><?php echo esc_attr($WooCommerceEventsDirections); ?></textarea>
                       <img class="help_tip" data-tip="<?php _e("Directions to the venue.", 'woocommerce-events'); ?>" src="<?php echo plugins_url(); ?>/woocommerce/assets/images/help.png" height="16" width="16" />
                </p>
        </div>
        <div class="options_group">
                <p class="form-field">
                       <label><?php _e('Phone number:', 'woocommerce-events'); ?></label>
                       <input type="text" id="WooCommerceEventsSupportContact" name="WooCommerceEventsSupportContact" value="<?php echo esc_attr($WooCommerceEventsSupportContact); ?>"/>
                       <img class="help_tip" data-tip="<?php _e("Event organizer's contact number.", 'woocommerce-events'); ?>" src="<?php echo plugins_url(); ?>/woocommerce/assets/images/help.png" height="16" width="16" />
                </p>
        </div>
        <div class="options_group">
                <p class="form-field">
                       <label><?php _e('Email address:', 'woocommerce-events'); ?></label>
                       <input type="text" id="WooCommerceEventsEmail" name="WooCommerceEventsEmail" value="<?php echo esc_attr($WooCommerceEventsEmail); ?>"/>
                       <img class="help_tip" data-tip="<?php _e("Event organizer's email address.", 'woocommerce-events'); ?>" src="<?php echo plugins_url(); ?>/woocommerce/assets/images/help.png" height="16" width="16" />
                </p>
        </div>
        
        
        <div class="options_group">
            <div style="padding-left: 30px; padding-right: 30px;">
                <p class="form-field">
                    <label><?php _e('Thank-you page text:', 'woocommerce-events'); ?></label>
                    <?php wp_editor( $WooCommerceEventsThankYouText, 'WooCommerceEventsThankYouText' ); ?>
                    <img class="help_tip" data-tip="<?php _e("The copy that will be displayed on the thank-you page after ticket purchase.", 'woocommerce-events'); ?>" src="<?php echo plugins_url(); ?>/woocommerce/assets/images/help.png" height="16" width="16" />
                </p>
            </div>
        </div>
        <div class="options_group">
            <div style="padding-left: 30px; padding-right: 30px;">
                <p class="form-field">
                    <label><?php _e('Event details tab text:', 'woocommerce-events'); ?></label>
                    <?php wp_editor( $WooCommerceEventsEventDetailsText, 'WooCommerceEventsEventDetailsText' ); ?>
                    <img class="help_tip" data-tip="<?php _e("The copy that will be displayed in the event details tab.", 'woocommerce-events'); ?>" src="<?php echo plugins_url(); ?>/woocommerce/assets/images/help.png" height="16" width="16" />
                </p>
            </div>
        </div>
        <div class="options_group">
            <p class="form-field">
                <label><?php _e('Calendar background color:', 'fooevents-calendar'); ?></label>
                <input type="text" class="woocommerce-events-color-field" id="WooCommerceEventsBackgroundColor" name="WooCommerceEventsBackgroundColor" value="<?php echo esc_html($WooCommerceEventsBackgroundColor); ?>"/>
                <img class="help_tip" data-tip="<?php _e('Color of the calendar background for the event. Also changes the background color of the date icon in the FooEvents Check-ins app.', 'fooevents-calendar'); ?>" src="<?php echo plugins_url(); ?>/woocommerce/assets/images/help.png" height="16" width="16" />
            </p>
        </div>
        <div class="options_group">
            <p class="form-field">
                <label><?php _e('Calendar text color:', 'fooevents-calendar'); ?></label>
                <input type="text" class="woocommerce-events-color-field" id="WooCommerceEventsTextColor" name="WooCommerceEventsTextColor" value="<?php echo esc_html($WooCommerceEventsTextColor); ?>"/>
                <img class="help_tip" data-tip="<?php _e('Color of the calendar text for the event. Also changes the font color of the date icon in the FooEvents Check-ins app.', 'fooevents-calendar'); ?>" src="<?php echo plugins_url(); ?>/woocommerce/assets/images/help.png" height="16" width="16" />
            </p>
        </div>
        <div class="options_group">
                <p class="form-field">
                       <label><?php _e('Capture attendee full name and email address? ', 'woocommerce-events'); ?></label>
                       <input type="checkbox" name="WooCommerceEventsCaptureAttendeeDetails" value="on" <?php echo ($WooCommerceEventsCaptureAttendeeDetails == 'on')? 'CHECKED' : ''; ?>>
                       <img class="help_tip" data-tip="<?php _e('This will add attendee capture fields on the checkout screen.', 'woocommerce-events'); ?>" src="<?php echo plugins_url(); ?>/woocommerce/assets/images/help.png" height="16" width="16" />
                </p>
        </div>
                <div class="options_group">
                <p class="form-field">
                       <label><?php _e('Capture attendee phone number?', 'woocommerce-events'); ?></label>
                       <input type="checkbox" name="WooCommerceEventsCaptureAttendeeTelephone" value="on" <?php echo ($WooCommerceEventsCaptureAttendeeTelephone == 'on')? 'CHECKED' : ''; ?>>
                       <img class="help_tip" data-tip="<?php _e('This will add a telephone number field to the attendee capture fields on the checkout screen.', 'woocommerce-events'); ?>" src="<?php echo plugins_url(); ?>/woocommerce/assets/images/help.png" height="16" width="16" />
                </p>
        </div>
        <div class="options_group">
                <p class="form-field">
                       <label><?php _e('Capture attendee company name?', 'woocommerce-events'); ?></label>
                       <input type="checkbox" name="WooCommerceEventsCaptureAttendeeCompany" value="on" <?php echo ($WooCommerceEventsCaptureAttendeeCompany == 'on')? 'CHECKED' : ''; ?>>
                       <img class="help_tip" data-tip="<?php _e('This will add a company field to the attendee capture fields on the checkout screen.', 'woocommerce-events'); ?>" src="<?php echo plugins_url(); ?>/woocommerce/assets/images/help.png" height="16" width="16" />
                </p>
        </div>
        <div class="options_group">
                <p class="form-field">
                       <label><?php _e('Capture attendee designation?', 'woocommerce-events'); ?></label>
                       <input type="checkbox" name="WooCommerceEventsCaptureAttendeeDesignation" value="on" <?php echo ($WooCommerceEventsCaptureAttendeeDesignation == 'on')? 'CHECKED' : ''; ?>>
                       <img class="help_tip" data-tip="<?php _e('This will add a designation field to the attendee capture fields on the checkout screen.', 'woocommerce-events'); ?>" src="<?php echo plugins_url(); ?>/woocommerce/assets/images/help.png" height="16" width="16" />
                </p>
        </div>
        <?php if (is_plugin_active( 'fooevents_seating/fooevents-seating.php' ) || is_plugin_active_for_network('fooevents_seating/fooevents-seating.php') ) : ?>
        <div class="options_group">
                <p class="form-field">
                        <label><?php _e('Display "View seating chart" option on checkout page?', 'woocommerce-events'); ?></label>
                        <input type="checkbox" name="WooCommerceEventsViewSeatingChart" value="on" <?php echo (empty($WooCommerceEventsViewSeatingChart) || $WooCommerceEventsViewSeatingChart == 'on')? 'CHECKED' : ''; ?>>
                        <img class="help_tip" data-tip="<?php _e("This will display a 'View seating chart' link on the checkout page. Before enabling this option, please ensure that you have setup a seating chart on the Event Seating tab." , 'woocommerce-events'); ?>" src="<?php echo plugins_url(); ?>/woocommerce/assets/images/help.png" height="16" width="16" />
                </p>
        </div>
        <?php endif; ?>
        <?php if (is_plugin_active( 'fooevents_bookings/fooevents-bookings.php' ) || is_plugin_active_for_network('fooevents_bookings/fooevents-bookings.php') ) : ?>
            <div class="options_group">
                <p class="form-field">
                        <label><?php _e('Display booking slots and dates on product pages?', 'woocommerce-events'); ?></label>
                        <input type="checkbox" name="WooCommerceEventsViewBookingsOptions" value="on" <?php echo (empty($WooCommerceEventsViewBookingsOptions) || $WooCommerceEventsViewBookingsOptions == 'off')? '' : 'CHECKED'; ?>>
                        <img class="help_tip" data-tip="<?php _e("This will display the booking date and slot drop-down options on the product page." , 'woocommerce-events'); ?>" src="<?php echo plugins_url(); ?>/woocommerce/assets/images/help.png" height="16" width="16" />
                </p>
            </div>
            <div class="options_group">
                <p class="form-field">
                        <label><?php _e('Hide booking time in slot drop-down?', 'woocommerce-events'); ?></label>
                        <input type="checkbox" name="WooCommerceEventsHideBookingsDisplayTime" value="on" <?php echo (empty($WooCommerceEventsHideBookingsDisplayTime) || $WooCommerceEventsHideBookingsDisplayTime == 'off')? '' : 'CHECKED'; ?>>
                        <img class="help_tip" data-tip="<?php _e("This will hide the time of the booking in the selected slot drop-down." , 'woocommerce-events'); ?>" src="<?php echo plugins_url(); ?>/woocommerce/assets/images/help.png" height="16" width="16" />
                </p>
            </div>
            <div class="options_group">
                <p class="form-field">
                        <label><?php _e('Hide stock availability notice?', 'woocommerce-events'); ?></label>
                        <input type="checkbox" name="WooCommerceEventsHideBookingsStockAvailability" value="on" <?php echo (empty($WooCommerceEventsHideBookingsStockAvailability) || $WooCommerceEventsHideBookingsStockAvailability == 'off')? '' : 'CHECKED'; ?>>
                        <img class="help_tip" data-tip="<?php _e("Hide the stock availability notice. This is typically used when offering unlimited stock." , 'woocommerce-events'); ?>" src="<?php echo plugins_url(); ?>/woocommerce/assets/images/help.png" height="16" width="16" />
                </p>
            </div>
            <div class="options_group">
                <p class="form-field">
                        <label><?php _e('Show stock availability in the date and slot drop-downs?', 'woocommerce-events'); ?></label>
                        <input type="checkbox" name="WooCommerceEventsViewBookingsStockDropdowns" value="on" <?php echo (empty($WooCommerceEventsViewBookingsStockDropdowns) || $WooCommerceEventsViewBookingsStockDropdowns == 'off')? '' : 'CHECKED'; ?>>
                        <img class="help_tip" data-tip="<?php _e("This will include the amount of remaining stock for a specific date and slot combination within the drop-down selectors." , 'woocommerce-events'); ?>" src="<?php echo plugins_url(); ?>/woocommerce/assets/images/help.png" height="16" width="16" />
                </p>
            </div>
            <div class="options_group">
                <p class="form-field">
                        <label><?php _e('Show out of stock booking dates?', 'woocommerce-events'); ?></label>
                        <input type="checkbox" name="WooCommerceEventsViewOutOfStockBookings" value="on" <?php echo (empty($WooCommerceEventsViewOutOfStockBookings) || $WooCommerceEventsViewOutOfStockBookings == 'off')? '' : 'CHECKED'; ?>>
                        <img class="help_tip" data-tip="<?php _e("Show dates that have no stock." , 'woocommerce-events'); ?>" src="<?php echo plugins_url(); ?>/woocommerce/assets/images/help.png" height="16" width="16" />
                </p>
            </div>
        <?php endif; ?>
    </div>
</div>