<select name="fooevents_filter">
    <option value=""><?php esc_attr_e('All Products', 'woocommerce-events'); ?></option>
    <option value="events" <?php echo ('events' == $fooevents_filter)? 'selected' : ''; ?>><?php esc_attr_e('Events', 'woocommerce-events'); ?></option>
    <option value="non-events" <?php echo('non-events' == $fooevents_filter)? 'selected' : '' ?>><?php esc_attr_e('Non-Events', 'woocommerce-events'); ?></option>
    <option value="expired-events" <?php echo('expired-events' == $fooevents_filter)? 'selected' : '' ?>><?php esc_attr_e('Expired Events', 'woocommerce-events'); ?></option>
</select>