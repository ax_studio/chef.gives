(function($) {

    jQuery('.fooevents-copy-from-purchaser').on('click', function(){

        var billing_first_name = jQuery('#billing_first_name').val();
        var billing_last_name = jQuery('#billing_last_name').val();
        var billing_email = jQuery('#billing_email').val();

        var parent = jQuery(this).parent('p').parent('div');

        parent.find('input').closest('.fooevents-attendee-first-name input').val(billing_first_name);
        parent.find('input').closest('.fooevents-attendee-last-name input').val(billing_last_name);
        parent.find('input').closest('.fooevents-attendee-email input').val(billing_email);

        return false;

    });

})(jQuery);