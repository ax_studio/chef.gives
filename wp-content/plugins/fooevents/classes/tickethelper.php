<?php if(!defined('ABSPATH')) exit;
class FooEvents_Ticket_Helper {
    
    public $Config;
    private $BarcodeHelper;
    private $ICSHelper;
    public $MailHelper;
    public $Validation;
    public $CheckoutHelper;
    public $ZoomAPIHelper;
    
    public function __construct($config) {
        
        $this->Config = $config;
        $this->register_ticket_post_type();
        
        //BarcodeHelper
        require_once($this->Config->classPath.'barcodehelper.php');
        $this->BarcodeHelper = new FooEvents_Barcode_Helper($this->Config);

        //ICSHelper
        require_once($this->Config->classPath.'icshelper.php');
        $this->ICSHelper = new FooEvents_ICS_helper($this->Config);
        
        //MailHelper
        require_once($this->Config->classPath.'mailhelper.php');
        $this->MailHelper = new FooEvents_Mail_Helper($this->Config);

        //ZoomAPIHelper
        require_once($this->Config->classPath.'zoomapihelper.php');
        $this->ZoomAPIHelper = new FooEvents_Zoom_API_Helper($this->Config);

        add_action('manage_edit-event_magic_tickets_columns', array(&$this, 'add_admin_columns'), 10, 1);
        add_action('manage_event_magic_tickets_posts_custom_column', array(&$this, 'add_admin_column_content'), 10, 1);
        add_action('pre_get_posts', array($this, 'status_orderby'));
        add_action('add_meta_boxes', array(&$this, 'add_tickets_meta_boxes'), 1, 2);
        add_action('save_post', array(&$this, 'save_edit_ticket_meta_boxes'), 1, 2);
        add_action('save_post', array(&$this, 'save_add_ticket_meta_boxes'), 1, 2);
        add_action('template_redirect', array( $this, 'redirect_ticket' ) );
        add_action('post_row_actions', array( $this, 'remove_ticket_view' ), 10, 2 );
        add_action('parse_query', array( $this, 'filter_unpaid_tickets' ) );
        add_action('admin_footer-edit.php', array( $this, 'display_bulk_resend' ));
        add_action('admin_action_resend_tickets',  array( $this, 'bulk_resend' ));
        add_action('wp_ajax_fetch_woocommerce_variations', array($this, 'fetch_woocommerce_variations'));
        add_action('wp_ajax_fetch_wordpress_user', array($this, 'fetch_wordpress_user'));
        add_action('wp_ajax_fetch_capture_attendee_details', array($this, 'fetch_capture_attendee_details'));
        add_action('wp_ajax_fooevents_validate_add_ticket', array($this, 'validate_add_ticket'));
        add_action('wp_ajax_fooevents_validate_edit_ticket', array($this, 'validate_edit_ticket'));
        add_action('admin_enqueue_scripts', array($this, 'disable_auto_save'), 1 );
        add_action('admin_init', array($this, 'disable_revisions'), 1 );
        
        add_action('wp_ajax_resend_ticket', array($this, 'resend_ticket'));
        add_action('wp_ajax_get_event_variations', array($this, 'get_event_variations'));
        add_action('wp_ajax_get_event_details', array($this, 'get_event_details'));
        
        add_filter('pre_get_posts', array(&$this, 'tickets_where'), 10, 1);
        add_filter('manage_edit-event_magic_tickets_sortable_columns', array($this, 'sortable_admin_columns'));
        add_filter('restrict_manage_posts', array($this, 'filter_ticket_options'));

    }
    
    /**
     * Registers the ticket post type.
     * 
     */
    private function register_ticket_post_type() {

        $labels = array(
		'name'               => __('Tickets', 'woocommerce-events'),
		'singular_name'      => __('Ticket', 'woocommerce-events'),
		'add_new'            => __('Add New', 'woocommerce-events'),
		'add_new_item'       => __('Add New Ticket', 'woocommerce-events'),
		'edit_item'          => __('Edit Ticket', 'woocommerce-events'),
		'new_item'           => __('New Ticket', 'woocommerce-events'),
		'all_items'          => __('Tickets', 'woocommerce-events'),
		'view_item'          => __('View Ticket', 'woocommerce-events'),
		'search_items'       => __('Search Tickets', 'woocommerce-events'),
		'not_found'          => __('No tickets found', 'woocommerce-events'),
		'not_found_in_trash' => __('No tickets found in the Trash', 'woocommerce-events'), 
		'parent_item_colon'  => '',
		'menu_name'          => __('FooEvents', 'woocommerce-events'));
        
        $args = array(
                    'labels'        => $labels,
                    'description'   => __('Event Tickets', 'woocommerce-events'),
                    'public'        => true,
                    'exclude_from_search' => true,
                    'menu_position' => 5,
                    'supports'      => array('custom-fields'),
                    'has_archive'   => true,
                    'capabilities'  => array(
                        'publish_posts' => 'publish_event_magic_tickets',
                        'edit_posts' => 'edit_event_magic_tickets',
                        'edit_published_posts' => 'edit_published_event_magic_tickets',
                        'edit_others_posts' => 'edit_others_event_magic_tickets',
                        'delete_posts' => 'delete_event_magic_tickets',
                        'delete_others_posts' => 'delete_others_event_magic_tickets',
                        'read_private_posts' => 'read_private_event_magic_tickets',
                        'edit_post' => 'edit_event_magic_ticket',
                        'delete_post' => 'delete_event_magic_ticket',
                        'read_post' => 'read_event_magic_ticket',
                        'edit_published_post' => 'edit_published_event_magic_ticket',
                        'publish_post' => 'publish_event_magic_ticket',
                    ),
                    'capability_type' => array('event_magic_ticket','event_magic_tickets'),
                    'map_meta_cap'  => true,
                    'menu_icon'     => 'dashicons-tickets-alt',
                    'has_archive'   => false,
                    'publicly_queryable'    => false,
                    'show_in_menu'  => 'fooevents'
                );
        
        register_post_type( 'event_magic_tickets', $args );	
        
    }

    /**
     * Adds admin columns to the event ticket custom post type.
     * 
     * @param array $columns
     * @return array $columns
     */
    public function add_admin_columns($columns) {

        $columns['cb'] = __('Select', 'woocommerce-events');
        $columns['title'] = __('Title', 'woocommerce-events');
        $columns['Order'] = __('Order', 'woocommerce-events');
        $columns['Event'] = __('Event', 'woocommerce-events');
        $columns['Purchaser'] = __('Purchaser', 'woocommerce-events');
        $columns['Attendee'] = __('Attendee', 'woocommerce-events');
        $columns['PurchaseDate'] = __('Purchase Date', 'woocommerce-events');
        
        if (is_plugin_active('fooevents_bookings/fooevents-bookings.php') || is_plugin_active_for_network('fooevents_bookings/fooevents-bookings.php')) {
            
            $columns['Bookings'] = __('Bookings', 'woocommerce-events');
            
        }
        
        $columns['Status'] = __('Status', 'woocommerce-events');
        
        return $columns;
    }
    
    /**
     * Adds column content to the event ticket custom post type.
     * 
     * @param string $column
     * @global object $post
     * 
     */
    public function add_admin_column_content($column) {
        
        global $post;
        global $woocommerce;
        
        $order_id = get_post_meta($post->ID, 'WooCommerceEventsOrderID', true);
        $customer_id = get_post_meta($post->ID, 'WooCommerceEventsCustomerID', true);
        $order = array();
        
        try {
            
            $order = new WC_Order( $order_id );
            
        } catch (Exception $e) {
            
        }   

        switch($column) {
            case 'Event' :
                
                $WooCommerceEventsProductID = get_post_meta($post->ID, 'WooCommerceEventsProductID', true);
                echo '<a href="'.get_site_url().'/wp-admin/post.php?post='.$WooCommerceEventsProductID.'&action=edit">'.get_the_title($WooCommerceEventsProductID).'</a>';
                break;
            
            case 'Order' :
                
                $WooCommerceEventsOrderID = get_post_meta($post->ID, 'WooCommerceEventsOrderID', true);
                echo '<a href="'.get_site_url().'/wp-admin/post.php?post='.$WooCommerceEventsOrderID.'&action=edit">'.$WooCommerceEventsOrderID.'</a>';
                break; 
            
            case 'Purchaser' :
                
                if(empty($order)) {
                    
                   echo "<i>Warning: WooCommerce order has been deleted.</i><br /><br />"; 
                    
                }
                
                if(!empty($customer_id) && !($customer_id instanceof WP_Error)) {
    
                    $WooCommerceEventsPurchaserFirstName = get_post_meta($post->ID, 'WooCommerceEventsPurchaserFirstName', true);
                    $WooCommerceEventsPurchaserLastName = get_post_meta($post->ID, 'WooCommerceEventsPurchaserLastName', true);
                    $WooCommerceEventsPurchaserEmail = get_post_meta($post->ID, 'WooCommerceEventsPurchaserEmail', true);
                    echo '<a href="'.get_site_url().'/wp-admin/user-edit.php?user_id='.$customer_id.'">'.$WooCommerceEventsPurchaserFirstName.' '.$WooCommerceEventsPurchaserLastName.' - ( '.$WooCommerceEventsPurchaserEmail.' )</a>';
                    
                } else {
                    
                    //guest account
                    try {
                        
                        if(!empty($order)) {
                            
                            echo $order->get_billing_first_name().' '.$order->get_billing_last_name().' - ( '.$order->get_billing_email().' )';

                        }
                         
                    } catch (Exception $e) {
            
                    }   
                
                }
                
                break;
                
            case 'Attendee' : 
                
                $WooCommerceEventsAttendeeName = get_post_meta($post->ID, 'WooCommerceEventsAttendeeName', true);
                $WooCommerceEventsAttendeeLastName = get_post_meta($post->ID, 'WooCommerceEventsAttendeeLastName', true);
                $WooCommerceEventsAttendeeEmail = get_post_meta($post->ID, 'WooCommerceEventsAttendeeEmail', true);
                echo $WooCommerceEventsAttendeeName.' '.$WooCommerceEventsAttendeeLastName.'- '.$WooCommerceEventsAttendeeEmail;
                
                break;
            
            case 'PurchaseDate' :
                
                echo $post->post_date;
                
                break;
            
            case 'Status' :
                
                $WooCommerceEventsProductID = get_post_meta($post->ID, 'WooCommerceEventsProductID', true);
                $WooCommerceEventsNumDays = (int)get_post_meta($WooCommerceEventsProductID, 'WooCommerceEventsNumDays', true);
                $WooCommerceEventsMultidayStatus = '';
                $WooCommerceEventsStatus = get_post_meta($post->ID, 'WooCommerceEventsStatus', true);
                
                if (!function_exists('is_plugin_active') || !function_exists('is_plugin_active_for_network')) {

                    require_once(ABSPATH.'/wp-admin/includes/plugin.php' );

                }

                if (is_plugin_active('fooevents_multi_day/fooevents-multi-day.php') || is_plugin_active_for_network('fooevents_multi_day/fooevents-multi-day.php')) {

                    $Fooevents_Multiday_Events = new Fooevents_Multiday_Events();
                    $WooCommerceEventsMultidayStatus = $Fooevents_Multiday_Events->display_multiday_status_ticket_meta_all($post->ID);

                }
                
                if(empty($WooCommerceEventsMultidayStatus) || $WooCommerceEventsStatus == 'Unpaid' || $WooCommerceEventsStatus == 'Canceled' || $WooCommerceEventsStatus == 'Cancelled' || $WooCommerceEventsNumDays == 1) {

                    echo $WooCommerceEventsStatus;

                } else {
                    
                    echo $WooCommerceEventsMultidayStatus;
                    
                }
                
                break;
                
            case 'Options' :

                break;
        }
        
    }
    
    /**
     * Make columns sortable
     * @param array $columns
     * @return array
     */
    public function sortable_admin_columns($columns) {
        
        $columns['Status']          = 'Status';
        $columns['Order']           = 'Order';
        $columns['Event']           = 'Event';
        $columns['Purchaser']       = 'Purchaser';
        $columns['Attendee']        = 'Attendee';
        $columns['PurchaseDate']    = 'PurchaseDate';
        $columns['Bookings']    = 'Bookings';
        
        return $columns;
        
    }
    
    /**
     * Make the status field sortable
     * @param obj $query
     * @return obj
     */
    public function status_orderby($query) {
        
        if(!is_admin()) {
            
            return;
            
        }
        
        if(in_array($query->get('post_type'),array('event_magic_tickets'))) {

            $orderby = $query->get('orderby');

            if('Status' == $orderby) {
                
                $query->set('meta_key','WooCommerceEventsStatus');
                $query->set('orderby','meta_value');
                
            }
            
            if('Attendee' == $orderby) {
                
                $query->set('meta_key','WooCommerceEventsAttendeeName');
                $query->set('orderby','meta_value');
                
            }
            
            if('Purchaser' == $orderby) {
                
                $query->set('meta_key','WooCommerceEventsPurchaserFirstName');
                $query->set('orderby','meta_value');
                
            }
            
            if('Event' == $orderby) {
                
                $query->set('meta_key','WooCommerceEventsProductName');
                $query->set('orderby','meta_value');

            }
            
        }

        return $query;
        
    }

    /**
     * Adds meta boxes to the tickets custom post type page.
     * 
     */
    public function add_tickets_meta_boxes() {
        
        $screens = array('event_magic_tickets');
        
        foreach($screens as $screen) {
            
            if(isset($_GET['post'])) {
            
                add_meta_box(
                            'woocommerce_events_ticket_details',
                            __( 'Ticket Details', 'woocommerce-events' ),
                             array(&$this, 'edit_ticket_admin'),
                            $screen, 'normal', 'high'
                    );

                add_meta_box(
                            'woocommerce_events_ticket_status',
                            __( 'Ticket Status', 'woocommerce-events' ),
                             array(&$this, 'edit_tickets_status_box'),
                            $screen, 'side', 'default'
                    );

                add_meta_box(
                            'woocommerce_events_ticket_resend_ticket',
                            __( 'Resend Ticket', 'woocommerce-events' ),
                             array(&$this, 'edit_tickets_resend_tickets_box'),
                            $screen, 'side', 'low'
                    );
            
            }
            
            if(!isset($_GET['post'])) {
                
                add_meta_box(
                            'woocommerce_events_ticket_add_event',
                            __( 'Event', 'woocommerce-events' ),
                             array(&$this, 'add_ticket_admin'),
                            $screen, 'normal', 'high'
                );

            }
            
        }
        
    }
    
    /**
     * Displays manual add ticket form.
     * 
     */
    public function add_ticket_admin() {

        $events = new WP_Query( array('post_type' => array('product'), 'posts_per_page' => -1, 'meta_query' => array( array( 'key' => 'WooCommerceEventsEvent', 'value' => 'Event' ) )) );
        $events = $events->get_posts();
        
        $users = get_users();
 
        require($this->Config->templatePath.'tickets_add_ticket.php');
        
    }
    
    /**
     * Displays edit/details ticket page meta box
     * 
     * @global object $post
     * @global object $woocommerce
     * @global object $wpdb
     */
    public function edit_ticket_admin() {

        if (!function_exists('is_plugin_active') || !function_exists('is_plugin_active_for_network')) {

            require_once(ABSPATH.'/wp-admin/includes/plugin.php' );

        }
        
        global $post;

        $order_id = get_post_meta($post->ID, 'WooCommerceEventsOrderID', true);
        $customer_id = get_post_meta($post->ID, 'WooCommerceEventsCustomerID', true);
        
        $ticket = $this->get_ticket_data($post->ID);

        $barcode_url =  $this->Config->barcodeURL;
        
        $booking_options = '';
        if (is_plugin_active('fooevents_bookings/fooevents-bookings.php') || is_plugin_active_for_network('fooevents_bookings/fooevents-bookings.php')) {

            $FooEvents_Bookings = new FooEvents_Bookings();
            $booking_options = $FooEvents_Bookings->ticket_details_booking_fields($post->ID, $ticket['WooCommerceEventsProductID']);

        }
 
        $custom_attendee_options = '';
        $seating_options = '';
        $pdf_ticket_link = '';

        if (is_plugin_active('fooevents_multi_day/fooevents-multi-day.php') || is_plugin_active_for_network('fooevents_multi_day/fooevents-multi-day.php')) {

            $Fooevents_Multiday_Events = new Fooevents_Multiday_Events();
            $WooCommerceEventsMultidayStatus = $Fooevents_Multiday_Events->display_multiday_status_ticket_meta_all($post->ID);

        }

        if (is_plugin_active('fooevents_custom_attendee_fields/fooevents-custom-attendee-fields.php') || is_plugin_active_for_network('fooevents_custom_attendee_fields/fooevents-custom-attendee-fields.php')) {

            $Fooevents_Custom_Attendee_Fields = new Fooevents_Custom_Attendee_Fields();
            $custom_attendee_options = $Fooevents_Custom_Attendee_Fields->ticket_details_attendee_fields($post->ID, $ticket['WooCommerceEventsProductID']);
            
        }

        if (is_plugin_active('fooevents_seating/fooevents-seating.php') || is_plugin_active_for_network('fooevents_seating/fooevents-seating.php')) {
            
            $Fooevents_Seating = new Fooevents_Seating();
            $seating_options = $Fooevents_Seating->ticket_details_seating_fields($ticket['WooCommerceEventsProductID'], $post->ID);
            
        }
        
        if (is_plugin_active('fooevents_pdf_tickets/fooevents-pdf-tickets.php') || is_plugin_active_for_network('fooevents_pdf_tickets/fooevents-pdf-tickets.php')) {
                                     
            $FooEvents_PDF_Tickets = new FooEvents_PDF_Tickets();
            $pdf_ticket_link = $FooEvents_PDF_Tickets->display_ticket_download($post->ID, $this->Config->barcodePath, $this->Config->eventPluginURL);

        }

        require($this->Config->templatePath.'tickets_edit_ticket.php');
        
    }
    
    /**
     * Get ticket check-in times    
     * @param int $ID
     * @retunrn array
     */
    public function get_checkin_times($ID) {
        
        global $wpdb;
        
        $table_name = $wpdb->prefix . 'fooevents_check_in';
        
        $formatedCheckinTimes = array();
        
        $times = $wpdb->get_results("
                    SELECT * FROM ".$table_name."
                    WHERE tid = ".$ID."
                "); 
        
        foreach($times as $time) {
            
            $formatedCheckinTimes[$time->day] = date_i18n(get_option('date_format') . ' ' . get_option('time_format') . ' (P)', $time->checkin);
            
        }

        if (!function_exists('is_plugin_active') || !function_exists('is_plugin_active_for_network')) {

            require_once(ABSPATH.'/wp-admin/includes/plugin.php' );

        }
        
        if (is_plugin_active('fooevents_multi_day/fooevents-multi-day.php') || is_plugin_active_for_network('fooevents_multi_day/fooevents-multi-day.php')) {

            return $formatedCheckinTimes;
            
        } elseif(!empty($formatedCheckinTimes)) {
            
            $retformatedCheckinTimes[1] = $formatedCheckinTimes[1];
            
            return $retformatedCheckinTimes;
            
        } else {
            
            return array();
            
        }
        
    }
    
    /**
     * Add ticket status meta box.
     * 
     * @global object $post
     */
    public function edit_tickets_status_box() {

        global $post;

        $WooCommerceEventsStatus = get_post_meta($post->ID, 'WooCommerceEventsStatus', true);
        $WooCommerceEventsProductID = get_post_meta($post->ID, 'WooCommerceEventsProductID', true);
        $WooCommerceEventsType = get_post_meta($WooCommerceEventsProductID, 'WooCommerceEventsType', true);
        $WooCommerceEventsNumDays = (int)get_post_meta($WooCommerceEventsProductID, 'WooCommerceEventsNumDays', true);
        $WooCommerceEventsMultidayStatus = '';
        
        if (!function_exists('is_plugin_active') || !function_exists('is_plugin_active_for_network')) {

            require_once(ABSPATH.'/wp-admin/includes/plugin.php' );

        }
        
        if ((is_plugin_active('fooevents_multi_day/fooevents-multi-day.php') || is_plugin_active_for_network('fooevents_multi_day/fooevents-multi-day.php')) && $WooCommerceEventsNumDays > 1 && ($WooCommerceEventsType == 'sequential' || $WooCommerceEventsType == 'select')) {
        
            $Fooevents_Multiday_Events = new Fooevents_Multiday_Events();
            $WooCommerceEventsMultidayStatus = $Fooevents_Multiday_Events->display_multiday_status_ticket_form_meta($post->ID);
            
        }
        
        require($this->Config->templatePath.'tickets_edit_ticket_status.php');
        
    }
    
    /**
     * Add resend ticket box
     * 
     * @global object $post
     * @global object $woocommerce
     */
    public function edit_tickets_resend_tickets_box() {
        
        global $post;
        global $woocommerce;

        $order_id = get_post_meta($post->ID, 'WooCommerceEventsOrderID', true);
        $customer_id = get_post_meta($post->ID, 'WooCommerceEventsCustomerID', true);
        $event_id = get_post_meta($post->ID, 'WooCommerceEventsProductID', true);
        $sendToAttendee = get_post_meta($event_id, 'WooCommerceEventsEmailAttendee', true);
        
        $order = array();
        try {
            
            $order = new WC_Order( $order_id );
            
        } catch(Exception $e) {
            
        }  
        
        $purchaser = array();
        $attendeeEmail = get_post_meta($post->ID, 'WooCommerceEventsAttendeeEmail', true);

        if ($sendToAttendee == "on" && !empty($attendeeEmail)) {
            
            $purchaser['customerEmail'] = $attendeeEmail;

        } else {
            
            if (!empty($order)) {
            
                $purchaser['customerEmail'] = $order->get_billing_email();
                
            } else {
                
                $purchaser['customerEmail'] =  '';
                        
            }
                    
        }
        
        require($this->Config->templatePath.'tickets_edit_ticket_resend_ticket.php');
        
    }
    
    /**
     * Saves tickets meta box settings
     * 
     * @param int $post_ID
     * @global object $post
     * @global object $woocommerce
     */
    public function save_edit_ticket_meta_boxes($post_ID) {
        
        global $post;
        global $woocommerce;
        global $wpdb;
        $table_name = $wpdb->prefix . 'fooevents_check_in';
        
        if (is_object($post) && isset($_POST) && empty($_POST['add_ticket']) && isset($_POST['fooevents_validation']) && $_POST['fooevents_validation'] == 'true') {

            if($post->post_type == "event_magic_tickets") {

                if(isset($_POST['WooCommerceEventsAttendeeName'])) {

                    $WooCommerceEventsAttendeeName = sanitize_text_field($_POST['WooCommerceEventsAttendeeName']);
                    update_post_meta($post_ID, 'WooCommerceEventsAttendeeName', $WooCommerceEventsAttendeeName);

                }
                
                if(isset($_POST['WooCommerceEventsAttendeeLastName'])) {

                    $WooCommerceEventsAttendeeLastName = sanitize_text_field($_POST['WooCommerceEventsAttendeeLastName']);
                    update_post_meta($post_ID, 'WooCommerceEventsAttendeeLastName', $WooCommerceEventsAttendeeLastName);

                }
                
                if(isset($_POST['WooCommerceEventsAttendeeEmail'])) {

                    $WooCommerceEventsAttendeeEmail = sanitize_text_field($_POST['WooCommerceEventsAttendeeEmail']);
                    update_post_meta($post_ID, 'WooCommerceEventsAttendeeEmail', $WooCommerceEventsAttendeeEmail);

                }
                
                if(isset($_POST['WooCommerceEventsAttendeeTelephone'])) {

                    $WooCommerceEventsAttendeeTelephone = sanitize_text_field($_POST['WooCommerceEventsAttendeeTelephone']);
                    update_post_meta($post_ID, 'WooCommerceEventsAttendeeTelephone', $WooCommerceEventsAttendeeTelephone);

                }
                
                if(isset($_POST['WooCommerceEventsAttendeeCompany'])) {

                    $WooCommerceEventsAttendeeCompany = sanitize_text_field($_POST['WooCommerceEventsAttendeeCompany']);
                    update_post_meta($post_ID, 'WooCommerceEventsAttendeeCompany', $WooCommerceEventsAttendeeCompany);

                }
                
                if(isset($_POST['WooCommerceEventsAttendeeDesignation'])) {

                    $WooCommerceEventsAttendeeDesignation = sanitize_text_field($_POST['WooCommerceEventsAttendeeDesignation']);
                    update_post_meta($post_ID, 'WooCommerceEventsAttendeeDesignation', $WooCommerceEventsAttendeeDesignation);

                }

                if (isset($_POST) && isset($_POST['ticket_status']) && $_POST['ticket_status'] == 'true' && isset($_POST['WooCommerceEventsStatus'])) {

                    update_post_meta($post_ID, 'WooCommerceEventsStatus', sanitize_text_field($_POST['WooCommerceEventsStatus'])); 

                }
                
                $WooCommerceEventsProductID = get_post_meta($post->ID, 'WooCommerceEventsProductID', true);
                $WooCommerceEventsNumDays = (int)get_post_meta($WooCommerceEventsProductID, 'WooCommerceEventsNumDays', true);
                
                if (!function_exists('is_plugin_active') || !function_exists('is_plugin_active_for_network')) {

                    require_once(ABSPATH.'/wp-admin/includes/plugin.php' );

                }

                if ((is_plugin_active('fooevents_multi_day/fooevents-multi-day.php') || is_plugin_active_for_network('fooevents_multi_day/fooevents-multi-day.php')) && $WooCommerceEventsNumDays > 1) {
                    
                    $Fooevents_Multiday_Events = new Fooevents_Multiday_Events();
                    $WooCommerceEventsMultidayStatus = $Fooevents_Multiday_Events->capture_multiday_status_ticket_meta($post_ID);

                } else {
                    
                    if(isset($_POST['WooCommerceEventsStatus']) && $_POST['WooCommerceEventsStatus'] == 'Checked In') {
                        
                        $timestamp = current_time('timestamp');
                        $wpdb->insert($table_name, array(
                                'tid' => $post->ID,
                                'eid' => $WooCommerceEventsProductID,
                                'day' => 1,
                                'checkin' => $timestamp
                            ));
                        
                    }
                    
                }

                $this->ZoomAPIHelper->register_ticket_attendee($post_ID);
                $this->ZoomAPIHelper->cancel_zoom_registrations(array($post));

                if (!empty($_POST['WooCommerceEventsResendTicket']) && !empty($_POST['WooCommerceEventsResendTicketEmail'])) {
                    
                    $this->resend_ticket($post->ID);
     
                }

            }
       
        }
        
    }

    /**
     * Display bulk ticket resend option.
     * 
     */
    public function display_bulk_resend() {

        global $post_type;

        if($post_type == 'event_magic_tickets') {
          ?>
          <script type="text/javascript">
            jQuery(document).ready(function() {
              jQuery('<option>').val('resend_tickets').text('<?php _e('Resend Tickets')?>').appendTo("select[name='action']");
              jQuery('<option>').val('resend_tickets').text('<?php _e('Resend Tickets')?>').appendTo("select[name='action2']");
            });
          </script>
          <?php
        }

    }
    
    /**
     * Bulk resend tickets.
     * 
     */
    public function bulk_resend() {
        
        $tickets = $_REQUEST['post'];
        
        foreach($tickets as $ticket) {
            
            $this->resend_ticket($ticket);
            
        }

    }
    
    /**
     * Redirects tickets custom most type
     * 
     */
    public function redirect_ticket() {
        
        $queried_post_type = get_query_var('post_type');
        if ( is_single() && 'event_magic_tickets' ==  $queried_post_type ) {
          wp_redirect( home_url(), 301 );
          exit;
        }
        
    }
    
    /**
     * Removes view link
     * 
     */
    public function remove_ticket_view($action, $post) {

        if ($post->post_type == "event_magic_tickets") {
            
            unset ($action['view']);
            
        }
                
        return $action;
        
    }
    
    /**
     * Removes unpaid tickets from the ticket list
     * 
     */
    public function filter_unpaid_tickets($query) {
        
        //if( is_admin() AND $query->query['post_type'] == 'event_magic_tickets' ) {    

            /*$query->query_vars['meta_key']      = 'WooCommerceEventsStatus';
            $query->query_vars['meta_value']    = 'Unpaid'; 
            $query->query_vars['meta_compare']  = '!=';*/

        //}

        return $query;
        
    }
    
    /**
     * Searches for post meta
     * 
     * @param object $query
     */
    public function tickets_where($query) {
        
        if(!is_admin()) {
            
            return;
            
        }
        
        if(in_array($query->get('post_type'),array('event_magic_tickets'))) {

            $custom_fields = array(
                "WooCommerceEventsAttendeeName",
                "WooCommerceEventsAttendeeEmail",
                "WooCommerceEventsCustomerID",
                "WooCommerceEventsVariations",
                "WooCommerceEventsPurchaserFirstName",
                "WooCommerceEventsPurchaserLastName",
                "WooCommerceEventsPurchaserEmail",
                "WooCommerceEventsStatus",
                "WooCommerceEventsTicketID",
                "WooCommerceEventsOrderID",
                "WooCommerceEventsProductName"
            );
            
            $globalWooCommerceEventsHideUnpaidTickets = get_option('globalWooCommerceEventsHideUnpaidTickets', true);
            
            $meta_query = array('relation' => 'AND');
            array_push($meta_query, array(
                'key' => "WooCommerceEventsStatus",
                'value' => '',
                'compare' => '!='
            ));
            
            if($globalWooCommerceEventsHideUnpaidTickets == 'yes') {
                
                array_push($meta_query, array(
                    'key' => "WooCommerceEventsStatus",
                    'value' => 'Unpaid',
                    'compare' => '!='
                ));
                
            }
            
            if(empty($query->query_vars['s']) && isset($_GET['s'])) {
                
                $query->query_vars['s'] = sanitize_text_field($_GET['s']);
                
            }
            
            $query->set("meta_query", $meta_query);
            
            $searchterm = $query->query_vars['s'];

            $query->query_vars['s'] = "";

            if ($searchterm != "") {
                $meta_query = array('relation' => 'OR');
                foreach($custom_fields as $cf) {
                    array_push($meta_query, array(
                        'key' => $cf,
                        'value' => $searchterm,
                        'compare' => 'LIKE'
                    ));
                }
  
                $query->set("meta_query", $meta_query);
            };
        
        }
        
        return $query;
        
    }
    
    /**
     * Fetch WooCommerce variations for manual add ticket 
     * 
     */
    public function fetch_woocommerce_variations() {
        
        global $woocommerce;
        
        if(!empty($_POST['productID'])) {
            
            $productID = sanitize_text_field($_POST['productID']);
            $product = wc_get_product($productID);

            $variations = '';
            if($product && $product->is_type('variable')){

                $variations = $product->get_available_variations();

            }

            if(!empty($variations)) {
                
                echo '<h2>Variations</h2>';
                echo '<p class="form-field">';
                echo '<label>Variation: </label>';    
                echo '<select id="WooCommerceEventsSelectedVariation" name="WooCommerceEventsSelectedVariation">';
                
                foreach($variations as $variation) {
                    
                    echo '<option value="'.$variation['variation_id'].'">';
                    
                    foreach($variation['attributes'] as $attributeType => $attribute) {
                        
                        $variationNameOutput = str_replace('attribute_', '', $attributeType);
                        $variationNameOutput = str_replace('pa_', '', $variationNameOutput);
                        $variationNameOutput = str_replace('_', ' ', $variationNameOutput);
                        $variationNameOutput = str_replace('-', ' ', $variationNameOutput);
                        $variationNameOutput = str_replace('Pa_', '', $variationNameOutput);
                        $variationNameOutput = ucwords($variationNameOutput); 
                        echo $variationNameOutput.": ".$attribute." ";
                        
                    }
                    
                    echo "</option>";

                }
                
                echo '</select>';
                echo '</p>';
                
            }

        }
        
        exit();
    }
    
    /**
     * Fetch WooCommerce user for manual add ticket 
     * 
     */
    public function fetch_wordpress_user() {
        
        global $woocommerce;
        $current_user = wp_get_current_user();

        if(current_user_can('publish_event_magic_tickets'))  {

            if(!empty($_POST['userID'])) {

                $return_user = array();
                $user = get_user_by('id', sanitize_text_field($_POST['userID']));

                $return_user['ID'] = $user->ID;
                $return_user['user_login'] = $user->data->user_login;
                $return_user['display_name'] = $user->data->display_name;
                $return_user['user_email'] = $user->data->user_email;

                echo json_encode($return_user);

            }

        }
        
        exit();
        
    }
    
    /**
     * Fetch WooCommerce attendee for manual add ticket 
     * 
     */
    public function fetch_capture_attendee_details() {
        
        $WooCommerceEventsCaptureAttendeeDetails  = get_post_meta(sanitize_text_field($_POST['productID']), 'WooCommerceEventsCaptureAttendeeDetails', true);
        
        echo json_encode(array("capture" => $WooCommerceEventsCaptureAttendeeDetails));
        
        exit();
    }
    
    /**
     * Validates edit ticket data
     * 
     */
    public function validate_edit_ticket() {
        
        parse_str($_POST['fields'], $fields);

        if(empty($fields['WooCommerceEventsAttendeeName'])) {
            
            echo json_encode(array('status' => 'error', 'message' => __('Attendee first name is required.', 'woocommerce-events')));
            exit();
            
        }
        
        if(empty($fields['WooCommerceEventsAttendeeLastName'])) {
            
            echo json_encode(array('status' => 'error', 'message' => __('Attendee last name is required.', 'woocommerce-events')));
            exit();
            
        }
        
        if(empty($fields['WooCommerceEventsAttendeeEmail'])) {
            
            echo json_encode(array('status' => 'error', 'message' => __('Purchaser email is required.', 'woocommerce-events')));
            exit();
            
        }
        
        if (!function_exists('is_plugin_active_for_network')) {
            
            require_once( ABSPATH . '/wp-admin/includes/plugin.php' );
            
        }
        
        if (is_plugin_active('fooevents_bookings/fooevents-bookings.php') || is_plugin_active_for_network('fooevents_bookings/fooevents-bookings.php')) {
        
            if(isset($fields['WooCommerceEventsBookingSlotID']) && isset($fields['WooCommerceEventsBookingDateID'])) {
                
                $FooEvents_Bookings = new FooEvents_Bookings();

                $bookings_validation = $FooEvents_Bookings->admin_edit_bookings_validate($fields['WooCommerceEventsBookingSlotID'], $fields['WooCommerceEventsBookingDateID'], $fields['fooevents_ticket_raw_id'], $fields['fooevents_event_id']);
                $bookings_validation = json_decode($bookings_validation, true);

                if($bookings_validation['type'] == "error") {
                    
                    echo json_encode(array('type' => 'error', 'message' => $bookings_validation['message']));
                    exit();
                    
                }
                
            }
            
        }
        
        if (is_plugin_active('fooevents_custom_attendee_fields/fooevents-custom-attendee-fields.php') || is_plugin_active_for_network('fooevents_custom_attendee_fields/fooevents-custom-attendee-fields.php')) {
            
            $Fooevents_Custom_Attendee_Fields = new Fooevents_Custom_Attendee_Fields();

            $custom_fields_validation = $Fooevents_Custom_Attendee_Fields->admin_edit_ticket_custom_fields_validate($fields['fooevents_event_id'], $fields);
            $custom_fields_validation = json_decode($custom_fields_validation, true);
            
            if($custom_fields_validation['type'] == "error") {

                echo json_encode(array('type' => 'error', 'message' => $custom_fields_validation['message']));
                exit();

            }
            
        }

        exit();
        
    }
    
    /**
     * Validates add ticket data
     * 
     */
    public function validate_add_ticket() {

        parse_str($_POST['fields'], $fields);

        if(empty($fields['WooCommerceEventsEvent'])) {
            
            echo json_encode(array('status' => 'error', 'message' => __('Event is required.', 'woocommerce-events')));
            exit();
            
        }
        
        if(empty($fields['WooCommerceEventsPurchaserUserName'])) {
            
            echo json_encode(array('status' => 'error', 'message' => __('Purchaser username is required.', 'woocommerce-events')));
            exit();
            
        }
        
        if(empty($fields['WooCommerceEventsPurchaserEmail'])) {
            
            echo json_encode(array('status' => 'error', 'message' => __('Purchaser email is required.', 'woocommerce-events')));
            exit();
            
        }
        
        if(empty($fields['WooCommerceEventsPurchaserFirstName'])) {
            
            echo json_encode(array('status' => 'error', 'message' => __('Purchaser first name is required.', 'woocommerce-events')));
            exit();
            
        }
        
        if(empty($fields['WooCommerceEventsAttendeeName'])) {
            
            echo json_encode(array('status' => 'error', 'message' => __('Attendee first name is required.', 'woocommerce-events')));
            exit();
            
        }
        
        if(empty($fields['WooCommerceEventsAttendeeLastName'])) {
            
            echo json_encode(array('status' => 'error', 'message' => __('Attendee last name is required.', 'woocommerce-events')));
            exit();
            
        }
        
        if(empty($fields['WooCommerceEventsAttendeeEmail'])) {
            
            echo json_encode(array('status' => 'error', 'message' => __('Attendee email is required.', 'woocommerce-events')));
            exit();
            
        }
        
        if(empty($fields['WooCommerceEventsClientID']) || $fields['WooCommerceEventsClientID'] == 0) {
            echo "";
            $usernames = $this->get_usernames();
            $userEmails = $this->get_user_emails();
        
            if(in_array($fields['WooCommerceEventsPurchaserEmail'], $userEmails)) {

                echo json_encode(array('type' => 'error', 'message' => __('User email address already exists.', 'woocommerce-events')));
                exit();

            }

            if(in_array($fields['WooCommerceEventsPurchaserUserName'], $usernames)) {

                echo json_encode(array('type' => 'error', 'message' => __('User display name already exists.', 'woocommerce-events')));
                exit();

            }
            
            
        }
        
        if (!function_exists('is_plugin_active_for_network')) {
            
            require_once( ABSPATH . '/wp-admin/includes/plugin.php' );
            
        }
        
        if (is_plugin_active('fooevents_bookings/fooevents-bookings.php') || is_plugin_active_for_network('fooevents_bookings/fooevents-bookings.php')) {
            
            if(isset($fields['WooCommerceEventsBookingSlotID']) && isset($fields['WooCommerceEventsBookingDateID'])) {
                
                $FooEvents_Bookings = new FooEvents_Bookings();

                $bookings_validation = $FooEvents_Bookings->admin_add_ticket_bookings_validate($fields['WooCommerceEventsEvent'], $fields['WooCommerceEventsBookingSlotID'], $fields['WooCommerceEventsBookingDateID']);
                $bookings_validation = json_decode($bookings_validation, true);
                
                if($bookings_validation['type'] == "error") {
                    
                    echo json_encode(array('type' => 'error', 'message' => $bookings_validation['message']));
                    exit();
                    
                }
                
            }
            
        }
        
        if (is_plugin_active('fooevents_custom_attendee_fields/fooevents-custom-attendee-fields.php') || is_plugin_active_for_network('fooevents_custom_attendee_fields/fooevents-custom-attendee-fields.php')) {
            
            $Fooevents_Custom_Attendee_Fields = new Fooevents_Custom_Attendee_Fields();
            
            $custom_fields_validation = $Fooevents_Custom_Attendee_Fields->admin_add_ticket_custom_fields_validate($fields['WooCommerceEventsEvent'], $fields);
            $custom_fields_validation = json_decode($custom_fields_validation, true);
            
            if($custom_fields_validation['type'] == "error") {

                echo json_encode(array('type' => 'error', 'message' => $custom_fields_validation['message']));
                exit();

            }
            
        }
        
        exit();
        
    }

    /**
     * Save manual add ticket
     * 
     * @param int $post_ID
     */
    public function save_add_ticket_meta_boxes($post_ID) {

        global $post;
        global $woocommerce;
        
        
        if(!empty($_POST['add_ticket']) && $_POST['fooevents_validation'] == 'true' && is_object($post) && $post->post_type == "event_magic_tickets") {

            if(wp_doing_ajax() || wp_doing_cron() || defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {

                return;

            }
            
            wp_dequeue_script('autosave');
            $userID = '';

            //Create new user
            if(empty($_POST['WooCommerceEventsClientID'])) {
                
                $random_password = wp_generate_password($length=12, $include_standard_special_chars=false);
                $userID = wp_create_user(sanitize_text_field($_POST['WooCommerceEventsPurchaserUserName']), $random_password, sanitize_text_field($_POST['WooCommerceEventsPurchaserEmail']));

                if ($userID instanceof WP_Error) {

                    if(array_key_exists("existing_user_email",$userID->errors)) {

                        wp_redirect('edit.php?post_type=event_magic_tickets&fooevents_error=3');

                    } else {

                        wp_redirect('edit.php?post_type=event_magic_tickets&fooevents_error=2');

                    }

                    exit();

                }
                
                if(!empty($userID)) {

                    wp_update_user( array ('ID' => $userID, 'display_name' => $_POST['WooCommerceEventsPurchaserFirstName']));    
                    wp_update_user( array ('ID' => $userID, 'role' => 'Customer'));    

                }
                
            } else {

                $userID = $_POST['WooCommerceEventsClientID'];

            }
            
            //Create ticket
            if(!empty($userID)) {
                
                $productVariation = '';
                $price = '';

                if(!empty($_POST['WooCommerceEventsSelectedVariation'])) {

                    $productVariation = new WC_Product_Variation(sanitize_text_field($_POST['WooCommerceEventsSelectedVariation']));
                    $price = wc_price($productVariation->get_price());

                }
                
                $productDetails=array();
                $variations = array();
                $x = 0;

                if(!empty($productVariation)) {

                    foreach($productVariation->get_variation_attributes() as $attribute=>$attribute_value){

                        $productDetails['variation'][$attribute] = $attribute_value;
                        $variations[$attribute] = (string)$attribute_value;
                        $x++;

                    }

                }
                
                $product = '';
                if(!empty($_POST['WooCommerceEventsSelectedVariation'])) {

                    $product = new WC_Product_Variation(sanitize_text_field($_POST['WooCommerceEventsSelectedVariation']));

                } else {

                    $product = new WC_Product(sanitize_text_field($_POST['WooCommerceEventsEvent']));

                }

                remove_action('save_post', array(&$this, 'save_add_ticket_meta_boxes'), 1, 2);
                
                $order = wc_create_order();
                $order->add_product($product, 1, $productDetails);
                $order->set_customer_id($userID);
                $order->calculate_totals();
                $order->update_status('completed', "", false);

                $post = array(

                    'ID' => $post_ID,
                    'post_author' => $userID,
                    'post_content' => "Ticket",
                    'post_status' => "publish",
                    'post_title' => 'Assigned Ticket',
                    'post_type' => "event_magic_tickets"

                );

                $user = get_user_by('id', $userID);
                $rand = rand(111111,999999);
                $ticketID = $post_ID.$rand;
                $post['post_title'] = '#'.$ticketID;
                $postID = wp_update_post( $post );

                if(empty($price)) {

                    $product = wc_get_product(sanitize_text_field($_POST['WooCommerceEventsEvent']));
                    $price = $product->get_price();
                    $price = wc_price($price);

                }
                
                //ticket fields
                update_post_meta($order->id, 'WooCommerceEventsOrderAdminAddTicket', true);   
                update_post_meta($post_ID, 'WooCommerceEventsCustomerID', $userID );   
                update_post_meta($post_ID, 'WooCommerceEventsProductID', sanitize_text_field($_POST['WooCommerceEventsEvent']));   
                update_post_meta($post_ID, 'WooCommerceEventsOrderID', $order->id );   
                update_post_meta($post_ID, 'WooCommerceEventsTicketID', $ticketID);
                update_post_meta($post_ID, 'WooCommerceEventsStatus', 'Not Checked In');
                update_post_meta($post_ID, 'WooCommerceEventsAttendeeName', sanitize_text_field($_POST['WooCommerceEventsAttendeeName']));
                update_post_meta($post_ID, 'WooCommerceEventsAttendeeLastName', sanitize_text_field($_POST['WooCommerceEventsAttendeeLastName']));
                update_post_meta($post_ID, 'WooCommerceEventsAttendeeEmail',  sanitize_text_field($_POST['WooCommerceEventsAttendeeEmail']));
                update_post_meta($post_ID, 'WooCommerceEventsAttendeeTelephone', '');
                update_post_meta($post_ID, 'WooCommerceEventsAttendeeCompany', '');
                update_post_meta($post_ID, 'WooCommerceEventsAttendeeDesignation', '');
                update_post_meta($post_ID, 'WooCommerceEventsPurchaserFirstName', $user->data->display_name);
                update_post_meta($post_ID, 'WooCommerceEventsPurchaserLastName', '');
                update_post_meta($post_ID, 'WooCommerceEventsPurchaserEmail', $user->data->user_email);
                
                if(!empty($_POST['WooCommerceEventsSelectedVariation'])) {
                    
                    update_post_meta($post_ID, 'WooCommerceEventsVariationID',sanitize_text_field($_POST['WooCommerceEventsSelectedVariation']));
                    
                }
                
                update_post_meta($post_ID, 'WooCommerceEventsVariations', $variations);
                update_post_meta($post_ID, 'WooCommerceEventsPrice', $price);
                
                $ticketHash = $this->generate_random_string(8);
                update_post_meta($post_ID, 'WooCommerceEventsTicketHash', $ticketHash);
                
                $product = get_post(sanitize_text_field($_POST['WooCommerceEventsEvent']));
                update_post_meta($post_ID, 'WooCommerceEventsProductName', $product->post_title);
                
                $this->ZoomAPIHelper->register_ticket_attendee($post_ID);
                
                if (!function_exists('is_plugin_active_for_network')) {

                    require_once( ABSPATH . '/wp-admin/includes/plugin.php' );

                }
                
                if (is_plugin_active('fooevents_bookings/fooevents-bookings.php') || is_plugin_active_for_network('fooevents_bookings/fooevents-bookings.php')) {
            
                    if(isset($_POST['WooCommerceEventsBookingSlotID']) && isset($_POST['WooCommerceEventsBookingDateID'])) {

                        $FooEvents_Bookings = new FooEvents_Bookings();
                        
                        $FooEvents_Bookings->admin_add_ticket_bookings_capture_booking($post_ID, $_POST['WooCommerceEventsEvent'], $_POST['WooCommerceEventsBookingSlotID'], $_POST['WooCommerceEventsBookingDateID']);
                        //$bookings_validation = json_decode($bookings_validation, true);

                    }

                }
                
                //exit();
                
            }
            
        }
        
        remove_action('save_post', array(&$this, 'save_add_ticket_meta_boxes'), 1, 2);
        
    }
    
    /**
     * Disable ticket post type auto save.
     * 
     */
    public function disable_auto_save() {

        if ('event_magic_tickets' == get_post_type()) {

            wp_dequeue_script( 'autosave' );
        
        }
        
    }
    
    /**
     * Disable ticket drafts.
     * 
     */
    public function disable_revisions() {
        
        remove_post_type_support('event_magic_tickets', 'revisions');
        
    }

    /**
     * Check if is edit page
     * @param string $new_edit
     * @return boolean
     */
    private function is_edit_page($new_edit = null) {
        
        global $pagenow;
        
        if (!is_admin()) { 
            
            return false;
        }    

        if($new_edit == "edit") {
            
            return in_array( $pagenow, array( 'edit.php',  ) );
            
        } elseif($new_edit == "new") {
            
            return in_array( $pagenow, array( 'post-new.php' ) );
            
        } else {
            
            return in_array( $pagenow, array( 'post.php', 'post-new.php' ) );
            
        }    
        
    }
    
    /**
     * Get usernames
     * 
     * @return array
     */
    private function get_usernames() {
        
        $users = get_users();
        $usernames = array();
        
        foreach($users as $user) {
            
            $usernames[] = $user->user_login;
            
        }
        
        return $usernames;
        
    }
    
    /**
     * Get user email addresses
     * 
     * @return array
     */
    private function get_user_emails() {
        
        $users = get_users();
        $usernames = array();

        foreach($users as $user) {
            
            $usernames[] = $user->user_email;
            
        }
        
        return $usernames;
        
    }
    
    /**
     * Get event variations on add ticket page
     * 
     */
    public function get_event_variations(){

        global $woocommerce;
        $event_id = sanitize_text_field($_POST['event_id']);

        $variations = array();
  
        $handle = new WC_Product_Variable($event_id);
        $variations1 = $handle->get_children();
        foreach ($variations1 as $value) {
            
            $single_variation=new WC_Product_Variation($value);
            $variations[$value] = implode(" / ", $single_variation->get_variation_attributes()).'-'.get_woocommerce_currency_symbol().$single_variation->price;
            
        }
        
        if(empty($variations)) {
            
            echo json_encode(array('status' => false, 'variations' => array()));
            
        } else {
            
            echo json_encode(array('status' => true, 'variations' => $variations));
            
        }
        
        exit();
    }
    
    /**
     * Get event details on add ticket page
     */
    public function get_event_details() {
   
        $event_id = sanitize_text_field($_POST['event_id']);
        $post = get_post($event_id);

        $event = array();
          
        $event['WooCommerceEventsName'] = $post->post_title;
        $event['WooCommerceEventsURL'] = get_permalink($event_id);
        $event['WooCommerceEventsProductID'] = $event_id;
        $event['WooCommerceEventsDate'] = get_post_meta($event_id, 'WooCommerceEventsDate', true);
        $event['WooCommerceEventsStartTime'] = get_post_meta($event_id, 'WooCommerceEventsHour', true).':'. get_post_meta($event_id, 'WooCommerceEventsMinutes', true);
        $event['WooCommerceEventsPeriod'] = get_post_meta($event_id, 'WooCommerceEventsPeriod', true);
        $event['WooCommerceEventsEndTime'] = get_post_meta($event_id, 'WooCommerceEventsHourEnd', true).':'. get_post_meta($event_id, 'WooCommerceEventsMinutesEnd', true);
        $event['WooCommerceEventsEndPeriod'] = get_post_meta($event_id, 'WooCommerceEventsEndPeriod', true);
        $event['WooCommerceEventsLocation'] = get_post_meta($event_id, 'WooCommerceEventsLocation', true);
        $event['WooCommerceEventsGPS'] = get_post_meta($event_id, 'WooCommerceEventsGPS', true);
        $event['WooCommerceEventsSupportContact'] = get_post_meta($event_id, 'WooCommerceEventsSupportContact', true);
        $event['WooCommerceEventsEmail'] = get_post_meta($event_id, 'WooCommerceEventsEmail', true);

        ob_start();
        require_once($this->Config->templatePath.'tickets_add_ticket_event_details.php');
        $event_details = ob_get_clean();
        
        echo $event_details;
        
        exit();
        
    }
    
    /**
     * Processes resend ticket
     * 
     * @param int $postID
     */
    public function resend_ticket($postID) {
        
        if(isset($_POST['postID'])) {
            
            $postID = $_POST['postID'];
            
        }

        $ticket = $this->get_ticket_data($postID);
        
        $productID = get_post_meta($postID, 'WooCommerceEventsProductID', true);
        $WooCommerceEventsEvent = get_post_meta($productID, 'WooCommerceEventsEvent', true);
        
        $customerDetails = array();
        
        $order_id = get_post_meta($postID, 'WooCommerceEventsOrderID', true);
        $customer_id = get_post_meta($postID, 'WooCommerceEventsCustomerID', true);
        $event_id = get_post_meta($postID, 'WooCommerceEventsProductID', true);
        $sendToAttendee = get_post_meta($event_id, 'WooCommerceEventsEmailAttendee', true);
        
        $order = array();
        try {
            $order = new WC_Order( $order_id );
        } catch (Exception $e) {
            
        }  

        $WooCommerceEventsEmailSubjectSingle = get_post_meta($productID, 'WooCommerceEventsEmailSubjectSingle', true);
        if(empty($WooCommerceEventsEmailSubjectSingle)) {

            $WooCommerceEventsEmailSubjectSingle  = __('{OrderNumber} Ticket', 'woocommerce-events');

        }
        $subject = str_replace('{OrderNumber}', '[#'.$order_id.']', $WooCommerceEventsEmailSubjectSingle);
        $ticketBody = '';
        
        if (!empty($order)) {
            
            if (!empty($order->get_billing_first_name())) {
                
                $customerDetails['customerFirstName']   = $order->get_billing_first_name();  
                
            } else {
                
                $customerDetails['customerFirstName']   = get_post_meta($postID, 'WooCommerceEventsPurchaserFirstName', true);
                
            }

            if (!empty($order->get_billing_last_name())) {
                
                $customerDetails['customerLastName']   = $order->get_billing_last_name();  
                
            } else {
                
                $customerDetails['customerLastName']   = get_post_meta($postID, 'WooCommerceEventsPurchaserLastName', true);
                
            }

            if (!empty($order->get_billing_email())) {
                
                $customerDetails['customerEmail']   = $order->get_billing_email();   
                
            } else {
                
                $customerDetails['customerEmail']   = get_post_meta($postID, 'WooCommerceEventsPurchaserEmail', true);
                
            }

        } else {
            
            $customerDetails['customerFirstName']   = '';
            $customerDetails['customerLastName']    = '';
            $customerDetails['customerEmail']       = '';
            
        }

        
        
        $WooCommerceEventsTicketTheme = get_post_meta($productID, 'WooCommerceEventsTicketTheme', true);
        if(empty($WooCommerceEventsTicketTheme)) {

            $WooCommerceEventsTicketTheme = $this->Config->emailTemplatePath;

        }
        
        $header = $this->MailHelper->parse_email_template($WooCommerceEventsTicketTheme.'/header.php', array(), $ticket); 
        $footer = $this->MailHelper->parse_email_template($WooCommerceEventsTicketTheme.'/footer.php', array(), $ticket);
        $ticket['ticketNumber'] = 1;
        $body = $this->MailHelper->parse_ticket_template($WooCommerceEventsTicketTheme.'/ticket.php', $ticket);

	    $ticketBody .= $body;

        $to = '';
        if (isset($_POST['WooCommerceEventsResendTicketEmail'])) {
            
            $to = sanitize_text_field($_POST['WooCommerceEventsResendTicketEmail']);
            
        } elseif(!empty($WooCommerceEventsAttendeeEmail) && $WooCommerceEventsAttendeeEmail != 1) {
            
            $to = $WooCommerceEventsAttendeeEmail;
            
        } else {

            $attendeeEmail = get_post_meta($postID, 'WooCommerceEventsAttendeeEmail', true);
            
            if ($sendToAttendee == "on" && !empty($attendeeEmail)) {

                $to = $attendeeEmail;    

            } else {

                $to = $customerDetails['customerEmail'];

            }

        }
        
        $attachments = array();
        if (!function_exists('is_plugin_active') || !function_exists('is_plugin_active_for_network')) {

            require_once(ABSPATH.'/wp-admin/includes/plugin.php' );

        }
        
        if (is_plugin_active('fooevents_pdf_tickets/fooevents-pdf-tickets.php') || is_plugin_active_for_network('fooevents_pdf_tickets/fooevents-pdf-tickets.php')) {

            $globalFooEventsPDFTicketsEnable = get_option( 'globalFooEventsPDFTicketsEnable' );
            $globalFooEventsPDFTicketsAttachHTMLTicket = get_option( 'globalFooEventsPDFTicketsAttachHTMLTicket' );

            if($globalFooEventsPDFTicketsEnable == 'yes') {

                $FooEvents_PDF_Tickets = new FooEvents_PDF_Tickets();
                $attachments[] = $FooEvents_PDF_Tickets->generate_ticket($productID, array($ticket), $this->Config->barcodePath, $this->Config->path);
                $FooEventsPDFTicketsEmailText = '<div class="fooevents-pdf-email-text">'.get_post_meta($productID, 'FooEventsPDFTicketsEmailText', true).'</div>';
                
                if($globalFooEventsPDFTicketsAttachHTMLTicket === 'yes') {

                    $header = $header.$FooEventsPDFTicketsEmailText;
                    $ticketBody = $body;
                    
                } else {

                    
                    $header = $FooEvents_PDF_Tickets->parse_email_template('email-header.php');
                    $footer = $FooEvents_PDF_Tickets->parse_email_template('email-footer.php');

                    $ticketBody = $FooEventsPDFTicketsEmailText.$footer;

                }

            }

        }

        // attach ics
        $WooCommerceEventsTicketAttachICS = get_post_meta($productID, 'WooCommerceEventsTicketAttachICS', true);

        if (!empty($WooCommerceEventsTicketAttachICS) && $WooCommerceEventsTicketAttachICS == 'on' && file_exists($this->Config->icsPath.$ticket['WooCommerceEventsTicketID'].'.ics')) {

            $attachments[] = $this->Config->icsPath.''.$ticket['WooCommerceEventsTicketID'].'.ics';

        }
        
        $mailStatus = $this->MailHelper->send_ticket($to, $subject, $header.$ticketBody.$footer, $attachments);
        
        if(isset($_POST['postID'])) {
            
            echo json_encode(array('message' => "Mail has been sent."));
            exit();
            
        }
        
    }
    
    /**
     * Retrieves ticket data from database.
     * 
     * @param int $ticketID
     * @return array
     */
    public function get_ticket_data($ticketID) {

        $ticket = array();

        $WooCommerceEventsProductID                 = get_post_meta($ticketID, 'WooCommerceEventsProductID', true);
        $WooCommerceEventsOrderID                   = get_post_meta($ticketID, 'WooCommerceEventsOrderID', true);
        $WooCommerceEventsTicketType                = get_post_meta($ticketID, 'WooCommerceEventsTicketType', true);
        $WooCommerceEventsTicketID                  = get_post_meta($ticketID, 'WooCommerceEventsTicketID', true);
        $WooCommerceEventsTicketHash                = get_post_meta($ticketID, 'WooCommerceEventsTicketHash', true);
        $WooCommerceEventsStatus                    = get_post_meta($ticketID, 'WooCommerceEventsStatus', true);
        $WooCommerceEventsTicketExpireTimestamp     = get_post_meta($ticketID, 'WooCommerceEventsTicketExpireTimestamp', true);
        
        $wp_date_format = get_option('date_format');
        $date_format = $wp_date_format . ' H:i';
        $ticket_expiration_date = '';
        if(!empty($WooCommerceEventsTicketExpireTimestamp)) {
            
            $ticket_expiration_date = date($date_format, $WooCommerceEventsTicketExpireTimestamp);
            
        }
        
        $ticket['WooCommerceEventsVariations']      = get_post_meta($ticketID, 'WooCommerceEventsVariations', true);
        $ticket['WooCommerceEventsPrice'] = get_post_meta($ticketID, 'WooCommerceEventsPrice', true);
        $ticket['WooCommerceEventsPriceSymbol'] = get_post_meta($ticketID, 'WooCommerceEventsPriceSymbol', true);

        $ticket['type'] = "HTML";

        if(!empty($ticket['WooCommerceEventsVariations'] ) && !is_array($ticket['WooCommerceEventsVariations'] )) {

            $ticket['WooCommerceEventsVariations'] = json_decode($ticket['WooCommerceEventsVariations'], JSON_UNESCAPED_UNICODE);

        }
        
        if(is_array($ticket['WooCommerceEventsVariations'])) {
            
            $variations = array();
            foreach($ticket['WooCommerceEventsVariations'] as $variationName => $variationValue) {
                
                $variationNameOutput = urldecode($variationName);
                $variationNameOutput = str_replace('attribute_', '', $variationNameOutput);
                $variationNameOutput = str_replace('pa_', '', $variationNameOutput);
                $variationNameOutput = str_replace('_', ' ', $variationNameOutput);
                $variationNameOutput = str_replace('-', ' ', $variationNameOutput);
                $variationNameOutput = str_replace('Pa_', '', $variationNameOutput);
                $variationNameOutput = ucwords($variationNameOutput);                    
                
                $variationValueOutput = urldecode($variationValue);
                $variationValueOutput = str_replace('_', ' ', $variationValueOutput);
                $variationValueOutput = str_replace('-', ' ', $variationValueOutput);
                $variationValueOutput = ucwords($variationValueOutput);
                
                $variations[$variationNameOutput] = $variationValueOutput;
                
            }
            
            $ticket['WooCommerceEventsVariations'] = $variations;
            
        }

        $ticket['WooCommerceEventsVariationID'] = get_post_meta($ticketID, 'WooCommerceEventsVariationID', true);

        $customer = get_post_meta($WooCommerceEventsOrderID, '_customer_user', true);
        
        $order = array();
        try {
            $order = new WC_Order( $WooCommerceEventsOrderID );
        } catch (Exception $e) {
            
        }  
        
        $customerDetails['customerID'] = $customer;

        if (!empty($order)) {
            
            $customerDetails['customerFirstName'] = $order->get_billing_first_name();
            $customerDetails['customerLastName'] = $order->get_billing_last_name();
            $customerDetails['customerEmail'] = $order->get_billing_email();
            $customerDetails['customerTelephone'] = $order->get_billing_phone();
            $customerDetails['customerID'] = $order->get_customer_id();
            
            if(empty($customerDetails['customerFirstName'])) {
                
                $user = get_user_by('id', $customer);
                $customerDetails['customerFirstName'] = $user->display_name;
            }
            
        } else {
            
            

            $customerDetails['customerFirstName'] = '';
            $customerDetails['customerLastName'] = '';
            $customerDetails['customerEmail'] = '';
            
        }
        
        $ticket['fooevents_custom_attendee_fields_options'] = '';
        $ticket['fooevents_seating_options'] = '';
        
        $customer = get_post_meta($WooCommerceEventsOrderID, '_customer_user', true);
        
        if (!function_exists('is_plugin_active') || !function_exists('is_plugin_active_for_network')) {

            require_once(ABSPATH.'/wp-admin/includes/plugin.php' );

        }

        if (is_plugin_active( 'fooevents_custom_attendee_fields/fooevents-custom-attendee-fields.php') || is_plugin_active_for_network('fooevents_custom_attendee_fields/fooevents-custom-attendee-fields.php')) {

            $Fooevents_Custom_Attendee_Fields = new Fooevents_Custom_Attendee_Fields();
            $ticket['fooevents_custom_attendee_fields_options'] = $Fooevents_Custom_Attendee_Fields->display_tickets_meta_custom_options_output_legacy($ticketID, $WooCommerceEventsProductID);
            $ticket['fooevents_custom_attendee_fields_options_array'] = $Fooevents_Custom_Attendee_Fields->display_tickets_meta_custom_options_output($ticketID, $WooCommerceEventsProductID);

        }
        
         if (is_plugin_active( 'fooevents_seating/fooevents-seating.php') || is_plugin_active_for_network('fooevents_seating/fooevents-seating.php')) {

            $Fooevents_Seating = new Fooevents_Seating();
            $ticket['fooevents_seating_options'] = $Fooevents_Seating->display_tickets_meta_seat_options_output_legacy($ticketID);
            $ticket['fooevents_seating_options_array'] = $Fooevents_Seating->display_tickets_meta_seat_options_output($ticketID);
            $ticket['WooCommerceEventsSeatingFields'] = get_post_meta($ticketID, 'WooCommerceEventsSeatingFields', true);

        }
        
        $WooCommerceEventsEvent = get_post_meta($WooCommerceEventsProductID, 'WooCommerceEventsEvent', true);
        $WooCommerceEventsCaptureAttendeeDetails = get_post_meta($WooCommerceEventsProductID, 'WooCommerceEventsCaptureAttendeeDetails', true);
        $WooCommerceEventsSendEmailTickets = get_post_meta($WooCommerceEventsProductID, 'WooCommerceEventsSendEmailTickets', true);
        $WooCommerceEventsEmailSubjectSingle = get_post_meta($WooCommerceEventsProductID, 'WooCommerceEventsEmailSubjectSingle', true);
        
        //update ticket as paid
        if($WooCommerceEventsStatus == 'Unpaid') {

            update_post_meta($ticketID, 'WooCommerceEventsStatus', 'Not Checked In');

        }

        $ticket['WooCommerceEventsEvent'] = get_post_meta($WooCommerceEventsProductID, 'WooCommerceEventsEvent', true);
        $ticket['WooCommerceEventsDate'] = get_post_meta($WooCommerceEventsProductID, 'WooCommerceEventsDate', true);
        $ticket['WooCommerceEventsEndDate'] = get_post_meta($WooCommerceEventsProductID, 'WooCommerceEventsEndDate', true);
        $ticket['WooCommerceEventsSelectDate'] = get_post_meta($WooCommerceEventsProductID, 'WooCommerceEventsSelectDate', true);
        $ticket['WooCommerceEventsMultiDayType'] = get_post_meta($WooCommerceEventsProductID, 'WooCommerceEventsMultiDayType', true);
        $ticket['WooCommerceEventsHour'] = get_post_meta($WooCommerceEventsProductID, 'WooCommerceEventsHour', true);
        $ticket['WooCommerceEventsMinutes'] = get_post_meta($WooCommerceEventsProductID, 'WooCommerceEventsMinutes', true);
        $ticket['WooCommerceEventsPeriod'] = get_post_meta($WooCommerceEventsProductID, 'WooCommerceEventsPeriod', true);
        $ticket['WooCommerceEventsHourEnd'] = get_post_meta($WooCommerceEventsProductID, 'WooCommerceEventsHourEnd', true);
        $ticket['WooCommerceEventsMinutesEnd'] = get_post_meta($WooCommerceEventsProductID, 'WooCommerceEventsMinutesEnd', true);
        $ticket['WooCommerceEventsEndPeriod'] = get_post_meta($WooCommerceEventsProductID, 'WooCommerceEventsEndPeriod', true);
        $ticket['WooCommerceEventsTimeZone'] = get_post_meta($WooCommerceEventsProductID, 'WooCommerceEventsTimeZone', true);
        $ticket['WooCommerceEventsLocation'] = get_post_meta($WooCommerceEventsProductID, 'WooCommerceEventsLocation', true);
        $ticket['WooCommerceEventsTicketLogo'] = get_post_meta($WooCommerceEventsProductID, 'WooCommerceEventsTicketLogo', true);
        $ticket['WooCommerceEventsTicketHeaderImage'] = get_post_meta($WooCommerceEventsProductID, 'WooCommerceEventsTicketHeaderImage', true);
        $ticket['WooCommerceEventsSupportContact'] = get_post_meta($WooCommerceEventsProductID, 'WooCommerceEventsSupportContact', true);
        $ticket['WooCommerceEventsEmail'] = get_post_meta($WooCommerceEventsProductID, 'WooCommerceEventsEmail', true);
        $ticket['WooCommerceEventsTicketBackgroundColor'] = get_post_meta($WooCommerceEventsProductID, 'WooCommerceEventsTicketBackgroundColor', true);
        $ticket['WooCommerceEventsTicketButtonColor'] = get_post_meta($WooCommerceEventsProductID, 'WooCommerceEventsTicketButtonColor', true);
        $ticket['WooCommerceEventsTicketTextColor'] = get_post_meta($WooCommerceEventsProductID, 'WooCommerceEventsTicketTextColor', true);
        $ticket['WooCommerceEventsTicketPurchaserDetails'] = get_post_meta($WooCommerceEventsProductID, 'WooCommerceEventsTicketPurchaserDetails', true);
        $ticket['WooCommerceEventsTicketAddCalendar'] = get_post_meta($WooCommerceEventsProductID, 'WooCommerceEventsTicketAddCalendar', true);
        $ticket['WooCommerceEventsTicketDisplayDateTime'] = get_post_meta($WooCommerceEventsProductID, 'WooCommerceEventsTicketDisplayDateTime', true);
        $ticket['WooCommerceEventsTicketDisplayBarcode'] = get_post_meta($WooCommerceEventsProductID, 'WooCommerceEventsTicketDisplayBarcode', true);
        $ticket['WooCommerceEventsTicketText'] = get_post_meta($WooCommerceEventsProductID, 'WooCommerceEventsTicketText', true);
        $ticket['WooCommerceEventsZoomText'] = $this->ZoomAPIHelper->get_ticket_text($WooCommerceEventsProductID, '', get_post_meta($ticketID, 'WooCommerceEventsAttendeeEmail', true));
        $ticket['WooCommerceEventsDirections'] = get_post_meta($WooCommerceEventsProductID, 'WooCommerceEventsDirections', true);
        $ticket['WooCommerceEventsTicketDisplayPrice'] = get_post_meta($WooCommerceEventsProductID, 'WooCommerceEventsTicketDisplayPrice', true);
        $ticket['WooCommerceEventsTicketDisplayZoom'] = get_post_meta($WooCommerceEventsProductID, 'WooCommerceEventsTicketDisplayZoom', true);
        $ticket['WooCommerceEventsTicketDisplayBookings'] = get_post_meta($WooCommerceEventsProductID, 'WooCommerceEventsTicketDisplayBookings', true);
        $ticket['WooCommerceEventsIncludeCustomAttendeeDetails'] = get_post_meta($WooCommerceEventsProductID, 'WooCommerceEventsIncludeCustomAttendeeDetails', true);

        $ticket['WooCommerceEventsTicketLogoPath'] = get_post_meta($WooCommerceEventsProductID, 'WooCommerceEventsTicketLogoPath', true);
        $ticket['WooCommerceEventsTicketType'] = $WooCommerceEventsTicketType;
        $ticket['WooCommerceEventsProductID'] = $WooCommerceEventsProductID;
        $ticket['WooCommerceEventsTicketID'] = $WooCommerceEventsTicketID;
        $ticket['WooCommerceEventsTicketHash'] = $WooCommerceEventsTicketHash;
        $ticket['WooCommerceEventsOrderID'] = $WooCommerceEventsOrderID;
        
        if(!empty($order)) {
            
            $ticket['WooCommerceEventsOrderStatus'] = ucfirst($order->get_status());
            $ticket['WooCommerceEventsOrderTotal'] = wc_price($order->get_total());
            $ticket['customerID'] = $order->get_customer_id();
            $ticket['customerPhone'] = $order->get_billing_phone();
        
        } else {
            
            $ticket['WooCommerceEventsOrderStatus'] = '';
            $ticket['WooCommerceEventsOrderTotal'] = '';
            $ticket['customerID'] = '';
            $ticket['customerPhone'] = '';
            
        }
        
        $ticket['WooCommerceEventsSendEmailTickets'] = $WooCommerceEventsSendEmailTickets;
        $ticket['WooCommerceEventsTicketExpireTimestamp'] = $WooCommerceEventsTicketExpireTimestamp;
        $ticket['WooCommerceEventsTicketExpireFormatted'] = $ticket_expiration_date;
        $ticket['ID'] = $ticketID;
        
        $event = get_post($WooCommerceEventsProductID);
        
        $ticket['WooCommerceEventsName'] = $event->post_title;
        $ticket['WooCommerceEventsURL'] = get_permalink($event->ID);
        
        $ticket['WooCommerceEventsDate'] = get_post_meta($event->ID, 'WooCommerceEventsDate', true);
        $ticket['WooCommerceEventsStartTime'] = get_post_meta($event->ID, 'WooCommerceEventsHour', true).':'. get_post_meta($event->ID, 'WooCommerceEventsMinutes', true).' '.get_post_meta($event->ID, 'WooCommerceEventsPeriod', true);
        $ticket['WooCommerceEventsEndTime'] = get_post_meta($event->ID, 'WooCommerceEventsHourEnd', true).':'. get_post_meta($event->ID, 'WooCommerceEventsMinutesEnd', true).' '.get_post_meta($event->ID, 'WooCommerceEventsEndPeriod', true);
        $ticket['WooCommerceEventsPeriod'] = get_post_meta($event->ID, 'WooCommerceEventsPeriod', true);
        $ticket['WooCommerceEventsEndPeriod'] = get_post_meta($event->ID, 'WooCommerceEventsEndPeriod', true);
        
        $ticket['WooCommerceEventsLocation'] = get_post_meta($event->ID, 'WooCommerceEventsLocation', true);
        $ticket['WooCommerceEventsGPS'] = get_post_meta($event->ID, 'WooCommerceEventsGPS', true);
        $ticket['WooCommerceEventsSupportContact'] = get_post_meta($event->ID, 'WooCommerceEventsSupportContact', true);
        $ticket['WooCommerceEventsEmail'] = get_post_meta($event->ID, 'WooCommerceEventsEmail', true);

        if (is_plugin_active('fooevents_bookings/fooevents-bookings.php') || is_plugin_active_for_network('fooevents_bookings/fooevents-bookings.php')) { 
            
            $FooEvents_Bookings = new FooEvents_Bookings();
            $bookings_data = $FooEvents_Bookings->get_ticket_slot_and_date($ticketID, $event->ID);
            
            $bookings_date_term = get_post_meta($WooCommerceEventsProductID,'WooCommerceEventsBookingsDateOverride', true);
            $bookings_slot_term = get_post_meta($WooCommerceEventsProductID,'WooCommerceEventsBookingsSlotOverride', true);
            $bookings_bookingdetails_term = get_post_meta($WooCommerceEventsProductID,'WooCommerceEventsBookingsBookingDetailsOverride', true);
            
            $ticket['WooCommerceEventsBookingSlot']= $bookings_data['slot'];
            $ticket['WooCommerceEventsBookingDate'] = $bookings_data['date'];

            if(empty($bookings_date_term)) {

                $ticket['WooCommerceEventsBookingsDateTerm'] = __('Date', 'fooevents-bookings');

            } else {

                $ticket['WooCommerceEventsBookingsDateTerm'] = $bookings_date_term;

            }

            if(empty($bookings_slot_term)) {

                $ticket['WooCommerceEventsBookingsSlotTerm'] = __('Slot', 'fooevents-bookings');

            } else {

                $ticket['WooCommerceEventsBookingsSlotTerm'] = $bookings_slot_term;

            }

        }

        if(is_plugin_active('fooevents_multi_day/fooevents-multi-day.php') || is_plugin_active_for_network('fooevents_multi_day/fooevents-multi-day.php')) { 

            if($ticket['WooCommerceEventsMultiDayType'] == 'select') {

                $ticket['WooCommerceEventsDate'] = $ticket['WooCommerceEventsSelectDate'][0];
                $ticket['WooCommerceEventsEndDate'] = $ticket['WooCommerceEventsSelectDate'][count($ticket['WooCommerceEventsSelectDate'])-1];

            }
        
        }
        
        $barcodeFileName = '';
        
        if(!empty($WooCommerceEventsTicketHash)) {
            
            $barcodeFileName = $WooCommerceEventsTicketHash.'-'.$WooCommerceEventsTicketID;
            
        } else {
            
            $barcodeFileName = $WooCommerceEventsTicketID;
            
        }
        
        $ticket['barcodeFileName'] = $barcodeFileName;
        
        $ticketDetails = get_post($WooCommerceEventsProductID);
        
        $ticket['WooCommerceEventsTicketText'] = apply_filters('meta_content', $ticket['WooCommerceEventsTicketText']);

        if(!empty($ticket['WooCommerceEventsTicketLogo'])) {
                
                $logo_id = $this->get_logo_id($ticket['WooCommerceEventsTicketLogo']);
                
                if($logo_id) {
                    
                    $ticket['WooCommerceEventsTicketLogoID'] = $this->get_logo_id($ticket['WooCommerceEventsTicketLogo']);
                    $ticket['WooCommerceEventsTicketLogoPath'] = get_attached_file($ticket['WooCommerceEventsTicketLogoID']);
                    
                } else {
                    
                    $ticket['WooCommerceEventsTicketLogoPath'] = $ticket['WooCommerceEventsTicketLogo'];
                    
                }

            }
	    
	    if(!empty($ticket['WooCommerceEventsTicketHeaderImage'])) {
                
            $header_image_id = $this->get_logo_id($ticket['WooCommerceEventsTicketHeaderImage']);
            
            if($header_image_id) {
                
                $ticket['WooCommerceEventsTicketHeaderImageID'] = $this->get_logo_id($ticket['WooCommerceEventsTicketHeaderImage']);
                $ticket['WooCommerceEventsTicketHeaderImagePath'] = get_attached_file($ticket['WooCommerceEventsTicketHeaderImageID']);
                
            } else {
                
                $ticket['WooCommerceEventsTicketHeaderImagePath'] = $ticket['WooCommerceEventsTicketHeaderImage'];
                
            }

        }
        
        $globalWooCommerceEventsTicketBackgroundColor = get_option('globalWooCommerceEventsTicketBackgroundColor', true);
        $globalWooCommerceEventsTicketButtonColor = get_option('globalWooCommerceEventsTicketButtonColor', true);
        $globalWooCommerceEventsTicketTextColor = get_option('globalWooCommerceEventsTicketTextColor', true);
        $globalWooCommerceEventsTicketLogo = get_option('globalWooCommerceEventsTicketLogo', true);

        if(empty($ticket['WooCommerceEventsTicketBackgroundColor'])) {

            $ticket['WooCommerceEventsTicketBackgroundColor'] = $globalWooCommerceEventsTicketBackgroundColor;

        }

        if(empty($ticket['WooCommerceEventsTicketButtonColor'])) {

            $ticket['WooCommerceEventsTicketButtonColor'] = $globalWooCommerceEventsTicketButtonColor;

        }

        if(empty($ticket['WooCommerceEventsTicketTextColor'])) {

            $ticket['WooCommerceEventsTicketTextColor'] = $globalWooCommerceEventsTicketTextColor;

        }

        if(empty($ticket['name'])) {

             $ticket['name'] = $ticketDetails->post_title;

        } 
        
        $timestamp = time();
        $key = md5($WooCommerceEventsTicketID + $timestamp + $this->Config->salt);                              
        $ticket['cancelLink'] = get_site_url().'/wp-admin/admin-ajax.php?action=woocommerce_events_cancel&id='.$WooCommerceEventsTicketID.'&t='.$timestamp.'&k='.$key;
            
        $ticket['WooCommerceEventsAttendeeName'] = get_post_meta($ticketID, 'WooCommerceEventsAttendeeName', true);
        $ticket['WooCommerceEventsAttendeeLastName'] = get_post_meta($ticketID, 'WooCommerceEventsAttendeeLastName', true);
        $ticket['WooCommerceEventsAttendeeTelephone'] = get_post_meta($ticketID, 'WooCommerceEventsAttendeeTelephone', true);
        $ticket['WooCommerceEventsAttendeeCompany'] = get_post_meta($ticketID, 'WooCommerceEventsAttendeeCompany', true);
        $ticket['WooCommerceEventsAttendeeDesignation'] = get_post_meta($ticketID, 'WooCommerceEventsAttendeeDesignation', true);
        $ticket['WooCommerceEventsAttendeeEmail'] = get_post_meta($ticketID, 'WooCommerceEventsAttendeeEmail', true);

        if(empty($ticket['WooCommerceEventsAttendeeName'])) {

            $ticket['WooCommerceEventsAttendeeName'] = $customerDetails['customerFirstName'];
            $ticket['WooCommerceEventsAttendeeLastName'] = $customerDetails['customerLastName'];
            $ticket['WooCommerceEventsAttendeeEmail'] = $customerDetails['customerEmail'];
            $ticket['WooCommerceEventsAttendeeTelephone'] = $customerDetails['customerTelephone'];

        }

        $ticket['customerFirstName'] = $customerDetails['customerFirstName']; 
        $ticket['customerLastName'] = $customerDetails['customerLastName'];
        $ticket['customerEmail'] = $customerDetails['customerEmail'];

        //generate barcode
        if (!file_exists($this->Config->barcodePath.$ticket['WooCommerceEventsTicketID'].'.png')) {

            $this->BarcodeHelper->generate_barcode($ticket['WooCommerceEventsTicketID'], $WooCommerceEventsTicketHash);

        }

        //generate ics

        $this->ICSHelper->generate_ICS($WooCommerceEventsProductID, $ticketID, !empty($ticket['WooCommerceEventsAttendeeEmail']) ? $ticket['WooCommerceEventsAttendeeEmail'] : '');
        $this->ICSHelper->save($ticket['WooCommerceEventsTicketID']);

        $ticket['FooEventsTicketFooterText'] = get_post_meta($WooCommerceEventsProductID, 'FooEventsTicketFooterText', true);
        
        if(empty($ticket['WooCommerceEventsTicketBackgroundColor'])) {

            $ticket['WooCommerceEventsTicketBackgroundColor'] = '#f5f5f5';

        }

        if(empty($ticket['WooCommerceEventsTicketButtonColor'])) {

            $ticket['WooCommerceEventsTicketButtonColor'] = '#16A75D';

        }

        if(empty($ticket['WooCommerceEventsTicketTextColor'])) {

            $ticket['WooCommerceEventsTicketTextColor'] = '#FFFFFF';

        }

        if ($ticket['WooCommerceEventsTimeZone'] != "") {

            $timeZoneDate = new DateTime();
            $timeZoneDate->setTimeZone(new DateTimeZone($ticket['WooCommerceEventsTimeZone']));
            $ticket['WooCommerceEventsTimeZone'] = $timeZoneDate->format('T');
            if((int)$ticket['WooCommerceEventsTimeZone'] > 0) {
                $ticket['WooCommerceEventsTimeZone'] = "UTC" . $ticket['WooCommerceEventsTimeZone'];
            }
        }

        return $ticket;
        
    }
    
    /**
     * Returns image url attachment
     * 
     * @global object $wpdb
     * @param string $image_url
     * @return boolean
     */
    public function get_logo_id($image_url) {
        global $wpdb;
        $attachment = $wpdb->get_col($wpdb->prepare("SELECT ID FROM $wpdb->posts WHERE guid='%s';", $image_url )); 

        if (!empty($attachment[0])) {

            return $attachment[0]; 

        } else {

            return false;

        }    
    }
    
    /**
     * Adds event filter selection to the ticket listing
     * 
     */
    public function filter_ticket_options() {
        
        global $wpdb;
        global $post_type;

        if($post_type == 'event_magic_tickets') {

            $prefix = $wpdb->base_prefix;

            $event_id = '';

            if (isset($_GET['event_id']) && '' != $_GET['event_id']) {

                $event_id = sanitize_text_field($_GET['event_id']);

            }

            $events = $wpdb->get_results(
                "   SELECT *
                    FROM ".$prefix."posts p 
                    LEFT JOIN ".$prefix."postmeta pm ON pm.post_id = p.ID AND pm.meta_key = 'WooCommerceEventsEvent'  
                    WHERE pm.meta_value = 'Event' 
                    ORDER BY p.post_title ASC
                "
            );

            require($this->Config->templatePath.'ticketfilteroptions.php');
        
        }
        
    }
    
    /**
     * Outputs notices to screen.
     * 
     * @param array $notices
     */
    private function output_notices($notices) {

        foreach ($notices as $notice) {

                echo "<div class='updated'><p>$notice</p></div>";

        }

    }

    /**
     * Generates random string used for ticket hash
     * 
     * @param int $length
     * @return string
     */
    private function generate_random_string($length = 10) {
        
        return substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);

    }

}