<?php if (!defined('ABSPATH')) exit;
class FooEvents_Orders_Helper {
    
    public $Config;
    public $TicketHelper;
    
    public function __construct($config) {

        $this->Config = $config;
        
        add_action('add_meta_boxes', array(&$this, 'add_orders_meta_boxes'));
        
    }
    
    /**
     * Adds meta boxes to the WooCommerce orders page.
     * 
     */
    public function add_orders_meta_boxes() {
        
        global $post;
        $screens = array('shop_orders', 'shop_order');
        
        $WooCommerceEventsOrderTickets = get_post_meta($post->ID, 'WooCommerceEventsOrderTickets', true); 
        $WooCommerceEventsOrderAdminAddTicket = get_post_meta($post->ID, 'WooCommerceEventsOrderAdminAddTicket', true); 
        
        foreach($screens as $screen) {
            
            if(isset($_GET['post'])) {
                
                if(!empty($WooCommerceEventsOrderTickets) || !empty($WooCommerceEventsOrderAdminAddTicket)) {
                    
                    add_meta_box(
                        'woocommerce_events_order_details',
                        __( 'Attendee Details', 'woocommerce-events' ),
                         array(&$this, 'add_orders_meta_boxes_details'),
                        $screen, 'normal'
                    );
                    
                }
                
            }
            
        }
        
    }
    
    /**
     * Outputs ticket details meta box
     * 
     * @param object $post
     */
    public function add_orders_meta_boxes_details($post) {
 
        $order = wc_get_order($post->ID);
        $order_status  = $order->get_status();

        if($order_status == 'completed') {
            
            $tickets_query = new WP_Query(array('post_type' => array('event_magic_tickets'), 'posts_per_page' => -1, 'meta_query' => array(array( 'key' => 'WooCommerceEventsOrderID', 'value' => $post->ID, 'compare' => '='))));
            $event_tickets = $tickets_query->get_posts();
            $WooCommerceEventsOrderTickets = $this->process_event_tickets_for_display($event_tickets);

            require($this->Config->templatePath.'order_ticket_details_generated_tickets.php');
            
        } else {
            
            $WooCommerceEventsOrderTickets = get_post_meta($post->ID, 'WooCommerceEventsOrderTickets', true); 
            $WooCommerceEventsOrderTickets = $this->process_order_tickets_for_display($WooCommerceEventsOrderTickets);

            require($this->Config->templatePath.'order_ticket_details.php');
            
        }
        
    }
    
    /**
    * Checks if a plugin is active.
    * 
    * @param string $plugin
    * @return boolean
    */
    private function is_plugin_active($plugin) {

        return in_array( $plugin, (array) get_option( 'active_plugins', array() ) );

    }
    
    /**
     * Formats tickets to be displayed
     * 
     * @param array $event_tickets
     * @return array
     * 
     */
    public function process_event_tickets_for_display($event_tickets){

        require_once($this->Config->classPath.'tickethelper.php');
        $TicketHelper =  new FooEvents_Ticket_Helper($this->Config);
        $processed_event_tickets = array();
        
        $x = 0;
        foreach($event_tickets as $ticket_raw) {
            
            $ticket = $TicketHelper->get_ticket_data($ticket_raw->ID); 
            
            $event = get_post($ticket['WooCommerceEventsProductID']);
            
            $processed_event_tickets[$ticket['WooCommerceEventsProductID']]['WooCommerceEventsProductID'] = $ticket['WooCommerceEventsProductID'];
            $processed_event_tickets[$ticket['WooCommerceEventsProductID']]['WooCommerceEventsName'] = $event->post_title;
            $processed_event_tickets[$ticket['WooCommerceEventsProductID']]['WooCommerceEventsURL'] = get_permalink($event->ID);

            $processed_event_tickets[$ticket['WooCommerceEventsProductID']]['WooCommerceEventsDate'] = get_post_meta($event->ID, 'WooCommerceEventsDate', true);
            $processed_event_tickets[$ticket['WooCommerceEventsProductID']]['WooCommerceEventsStartTime'] = get_post_meta($event->ID, 'WooCommerceEventsHour', true).':'. get_post_meta($event->ID, 'WooCommerceEventsMinutes', true).' '.get_post_meta($event->ID, 'WooCommerceEventsPeriod', true);
            $processed_event_tickets[$ticket['WooCommerceEventsProductID']]['WooCommerceEventsEndTime'] = get_post_meta($event->ID, 'WooCommerceEventsHourEnd', true).':'. get_post_meta($event->ID, 'WooCommerceEventsMinutesEnd', true).' '.get_post_meta($event->ID, 'WooCommerceEventsEndPeriod', true);

            $processed_event_tickets[$ticket['WooCommerceEventsProductID']]['WooCommerceEventsLocation'] = get_post_meta($event->ID, 'WooCommerceEventsLocation', true);
            $processed_event_tickets[$ticket['WooCommerceEventsProductID']]['WooCommerceEventsGPS'] = get_post_meta($event->ID, 'WooCommerceEventsGPS', true);
            $processed_event_tickets[$ticket['WooCommerceEventsProductID']]['WooCommerceEventsSupportContact'] = get_post_meta($event->ID, 'WooCommerceEventsSupportContact', true);
            $processed_event_tickets[$ticket['WooCommerceEventsProductID']]['WooCommerceEventsEmail'] = get_post_meta($event->ID, 'WooCommerceEventsEmail', true);
            
            if(!empty($ticket['WooCommerceEventsVariations'])) {

                $ticket_vars = array();
                foreach($ticket['WooCommerceEventsVariations'] as $variation_name => $variation_value) {

                    $variation_name_output = str_replace('attribute_', '', $variation_name);
                    $variation_name_output = str_replace('pa_', '', $variation_name_output);
                    $variation_name_output = str_replace('_', ' ', $variation_name_output);
                    $variation_name_output = str_replace('-', ' ', $variation_name_output);
                    $variation_name_output = str_replace('Pa_', '', $variation_name_output);
                    $variation_name_output = ucwords($variation_name_output);

                    $variation_value_output = str_replace('_', ' ', $variation_value);
                    $variation_value_output = str_replace('-', ' ', $variation_value_output);
                    $variation_value_output = ucwords($variation_value_output);

                    $ticket_vars[$variation_name_output] = $variation_value_output;

                }

                $ticket['WooCommerceEventsVariations'] = $ticket_vars;

            }

            $ticket_cust = array();

            if (is_plugin_active('fooevents_custom_attendee_fields/fooevents-custom-attendee-fields.php') || is_plugin_active_for_network('fooevents_custom_attendee_fields/fooevents-custom-attendee-fields.php')) {

                $Fooevents_Custom_Attendee_Fields = new Fooevents_Custom_Attendee_Fields();
                $ticket_cust = $Fooevents_Custom_Attendee_Fields->fetch_attendee_details_for_order_generated($ticket['WooCommerceEventsProductID'], $ticket_raw->ID);

            }

            $ticket['WooCommerceEventsCustomAttendeeFields'] = $ticket_cust;

            $processed_event_tickets[$ticket['WooCommerceEventsProductID']]['tickets'][$x] = $ticket;

            if (is_plugin_active( 'fooevents_bookings/fooevents-bookings.php' ) || is_plugin_active_for_network('fooevents_bookings/fooevents-bookings.php')) {

                    $Fooevents_Bookings = new Fooevents_Bookings();

                    if(!empty($ticket['WooCommerceEventsBookingOptions'])) {

                        $WooCommerceEventsBookingFields = $Fooevents_Bookings->process_capture_booking('', $ticket['WooCommerceEventsProductID'], $ticket['WooCommerceEventsBookingOptions']);

                    }

                    $bookings_date_term = get_post_meta($ticket['WooCommerceEventsProductID'],'WooCommerceEventsBookingsDateOverride', true);
                    $bookings_slot_term = get_post_meta($ticket['WooCommerceEventsProductID'],'WooCommerceEventsBookingsSlotOverride', true);
                    
                    if(!empty($WooCommerceEventsBookingFields)) {

                        $processed_event_tickets[$ticket['WooCommerceEventsProductID']]['tickets'][$x]['WooCommerceEventsBookingOptions']['slot'] = $WooCommerceEventsBookingFields['slot'];
                        $processed_event_tickets[$ticket['WooCommerceEventsProductID']]['tickets'][$x]['WooCommerceEventsBookingOptions']['date'] = $WooCommerceEventsBookingFields['date'];

                    }

            }
            
            $x++;
        }
        
        return $processed_event_tickets;
        
    }
    
    /**
     * Formats tickets to be displayed
     * 
     * @param array $event_tickets
     * @return array
     * 
     */
    private function process_order_tickets_for_display($WooCommerceEventsOrderTickets) {
        
        if (!function_exists('is_plugin_active_for_network')) {

            require_once(ABSPATH.'/wp-admin/includes/plugin.php' );

        }

        $processed_event_tickets = array();
        
        foreach($WooCommerceEventsOrderTickets as $event_tickets) {
            
            $x = 0;
            foreach($event_tickets as $ticket) {

                $event = get_post($ticket['WooCommerceEventsProductID']);
                
                $processed_event_tickets[$ticket['WooCommerceEventsProductID']]['WooCommerceEventsProductID'] = $ticket['WooCommerceEventsProductID'];
                $processed_event_tickets[$ticket['WooCommerceEventsProductID']]['WooCommerceEventsName'] = $event->post_title;
                $processed_event_tickets[$ticket['WooCommerceEventsProductID']]['WooCommerceEventsURL'] = get_permalink($event->ID);
                
                $processed_event_tickets[$ticket['WooCommerceEventsProductID']]['WooCommerceEventsDate'] = get_post_meta($event->ID, 'WooCommerceEventsDate', true);
                $processed_event_tickets[$ticket['WooCommerceEventsProductID']]['WooCommerceEventsStartTime'] = get_post_meta($event->ID, 'WooCommerceEventsHour', true).':'. get_post_meta($event->ID, 'WooCommerceEventsMinutes', true).' '.get_post_meta($event->ID, 'WooCommerceEventsPeriod', true);
                $processed_event_tickets[$ticket['WooCommerceEventsProductID']]['WooCommerceEventsEndTime'] = get_post_meta($event->ID, 'WooCommerceEventsHourEnd', true).':'. get_post_meta($event->ID, 'WooCommerceEventsMinutesEnd', true).' '.get_post_meta($event->ID, 'WooCommerceEventsEndPeriod', true);

                $processed_event_tickets[$ticket['WooCommerceEventsProductID']]['WooCommerceEventsLocation'] = get_post_meta($event->ID, 'WooCommerceEventsLocation', true);
                $processed_event_tickets[$ticket['WooCommerceEventsProductID']]['WooCommerceEventsGPS'] = get_post_meta($event->ID, 'WooCommerceEventsGPS', true);
                $processed_event_tickets[$ticket['WooCommerceEventsProductID']]['WooCommerceEventsSupportContact'] = get_post_meta($event->ID, 'WooCommerceEventsSupportContact', true);
                $processed_event_tickets[$ticket['WooCommerceEventsProductID']]['WooCommerceEventsEmail'] = get_post_meta($event->ID, 'WooCommerceEventsEmail', true);
                
                if(!empty($ticket['WooCommerceEventsVariations'])) {
                    
                    $ticket_vars = array();
                    foreach($ticket['WooCommerceEventsVariations'] as $variation_name => $variation_value) {
                        
                        $variation_name_output = str_replace('attribute_', '', $variation_name);
                        $variation_name_output = str_replace('pa_', '', $variation_name_output);
                        $variation_name_output = str_replace('_', ' ', $variation_name_output);
                        $variation_name_output = str_replace('-', ' ', $variation_name_output);
                        $variation_name_output = str_replace('Pa_', '', $variation_name_output);
                        $variation_name_output = ucwords($variation_name_output);

                        $variation_value_output = str_replace('_', ' ', $variation_value);
                        $variation_value_output = str_replace('-', ' ', $variation_value_output);
                        $variation_value_output = ucwords($variation_value_output);
                        
                        $ticket_vars[$variation_name_output] = $variation_value_output;
                        
                    }
                    
                    $ticket['WooCommerceEventsVariations'] = $ticket_vars;
                    
                }

                if(!empty($ticket['WooCommerceEventsCustomAttendeeFields'])) {

                    if (is_plugin_active('fooevents_custom_attendee_fields/fooevents-custom-attendee-fields.php') || is_plugin_active_for_network('fooevents_custom_attendee_fields/fooevents-custom-attendee-fields.php')) {

                        $Fooevents_Custom_Attendee_Fields = new Fooevents_Custom_Attendee_Fields();
                        $ticket_cust = $Fooevents_Custom_Attendee_Fields->fetch_attendee_details_for_order($ticket['WooCommerceEventsProductID'], $ticket['WooCommerceEventsCustomAttendeeFields']);

                    }
                    
                    $ticket['WooCommerceEventsCustomAttendeeFields'] = $ticket_cust;
                    
                }

                $processed_event_tickets[$ticket['WooCommerceEventsProductID']]['tickets'][$x] = $ticket;

                if (is_plugin_active( 'fooevents_bookings/fooevents-bookings.php' ) || is_plugin_active_for_network('fooevents_bookings/fooevents-bookings.php')) {

                        $Fooevents_Bookings = new Fooevents_Bookings();

                        if(!empty($ticket['WooCommerceEventsBookingOptions'])) {
                            
                            $WooCommerceEventsBookingFields = $Fooevents_Bookings->process_capture_booking('', $ticket['WooCommerceEventsProductID'], $ticket['WooCommerceEventsBookingOptions']);

                        }

                        $bookings_date_term = get_post_meta($ticket['WooCommerceEventsProductID'],'WooCommerceEventsBookingsDateOverride', true);
                        $bookings_slot_term = get_post_meta($ticket['WooCommerceEventsProductID'],'WooCommerceEventsBookingsSlotOverride', true);
                        
                        $slot_label = '';
                        if(empty($bookings_slot_term)) {

                            $slot_label = __('Slot', 'fooevents-bookings');

                        } else {

                            $slot_label = $bookings_slot_term;

                        }
                        
                        $date_label = '';
                        if(empty($bookings_date_term)) {

                            $date_label = __('Date', 'fooevents-bookings');

                        } else {

                            $date_label = $bookings_date_term;

                        }
                        
                        if(!empty($WooCommerceEventsBookingFields)) {

                            $processed_event_tickets[$ticket['WooCommerceEventsProductID']]['tickets'][$x]['WooCommerceEventsBookingOptions']['slot'] = $WooCommerceEventsBookingFields['slot'];
                            $processed_event_tickets[$ticket['WooCommerceEventsProductID']]['tickets'][$x]['WooCommerceEventsBookingOptions']['slot_term'] = $slot_label;
                            $processed_event_tickets[$ticket['WooCommerceEventsProductID']]['tickets'][$x]['WooCommerceEventsBookingOptions']['date'] = $WooCommerceEventsBookingFields['date'];
                            $processed_event_tickets[$ticket['WooCommerceEventsProductID']]['tickets'][$x]['WooCommerceEventsBookingOptions']['date_term'] = $date_label;

                        }

                }
                
                $x++;
                
            }
            
        }
        
        return $processed_event_tickets;
        
    }
    
}
