<?php

/**
 * Append additional data to output upon successful signin
 * @param type $output
 */
function fooevents_append_output_data($output) {

    //include config for plugin version
    require_once(WP_PLUGIN_DIR.'/fooevents/config.php');

    $tempConfig = new FooEvents_Config();

    $output['data']['plugin_version'] = (string)$tempConfig->pluginVersion;

    // Get app settings
    $output['data']['app_title'] = (string)get_option('globalWooCommerceEventsAppTitle', '');
    $output['data']['app_logo'] = preg_replace_callback('/[^\x20-\x7f]/', function($match) {
        return urlencode($match[0]);
    }, (string)get_option('globalWooCommerceEventsAppLogo', ''));
    
    $output['data']['app_color'] = (string)get_option('globalWooCommerceEventsAppColor', '');

    if ( strpos($output['data']['app_color'], '#') === false ) {
        $output['data']['app_color'] = '';
    }

    $output['data']['app_text_color'] = (string)get_option('globalWooCommerceEventsAppTextColor', '');

    if ( strpos($output['data']['app_text_color'], '#') === false ) {
        $output['data']['app_text_color'] = '';
    }

    $output['data']['app_background_color'] = (string)get_option('globalWooCommerceEventsAppBackgroundColor', '');

    if ( strpos($output['data']['app_background_color'], '#') === false ) {
        $output['data']['app_background_color'] = '';
    }

    $output['data']['app_signin_text_color'] = (string)get_option('globalWooCommerceEventsAppSignInTextColor', '');

    if ( strpos($output['data']['app_signin_text_color'], '#') === false ) {
        $output['data']['app_signin_text_color'] = '';
    }

    $output['data']['event_override'] = (string)get_option('globalWooCommerceEventsEventOverride', '');
    $output['data']['event_override_plural'] = (string)get_option('globalWooCommerceEventsEventOverridePlural', '');
    $output['data']['attendee_override'] = (string)get_option('globalWooCommerceEventsAttendeeOverride', '');
    $output['data']['attendee_override_plural'] = (string)get_option('globalWooCommerceEventsAttendeeOverridePlural', '');
    $output['data']['day_override'] = (string)get_option('WooCommerceEventsDayOverride', '');
    $output['data']['day_override_plural'] = (string)get_option('WooCommerceEventsDayOverridePlural', '');
    $output['data']['hide_personal_info'] = (string)get_option('globalWooCommerceEventsAppHidePersonalInfo', '');
    $output['data']['gmt_offset'] = (string)get_option('gmt_offset');

    if ( !function_exists('is_plugin_active') || !function_exists('is_plugin_active_for_network') ) {

        require_once(ABSPATH.'/wp-admin/includes/plugin.php');

    }

    if ( is_plugin_active('fooevents_multi_day/fooevents-multi-day.php') || is_plugin_active_for_network('fooevents_multi_day/fooevents-multi-day.php') ) {
        
        $output['data']['multiday_enabled'] = 'Yes';

    } else {
        
        $output['data']['multiday_enabled'] = 'No';
        
    }

    if ( is_plugin_active('fooevents_bookings/fooevents-bookings.php') || is_plugin_active_for_network('fooevents_bookings/fooevents-bookings.php') ) {
        
        $output['data']['bookings_enabled'] = 'Yes';

    } else {
        
        $output['data']['bookings_enabled'] = 'No';
        
    }

    if ( is_plugin_active('fooevents_seating/fooevents-seating.php') || is_plugin_active_for_network('fooevents_seating/fooevents-seating.php') ) {
        
        $output['data']['seating_enabled'] = 'Yes';

    } else {
        
        $output['data']['seating_enabled'] = 'No';
        
    }

    return $output;
}

/**
 * Get all events as an array
 * 
 * @return array eventsArray
 */

function getAllEvents($user = null) {

    $eventsArray = array();
    $args = array(
            'post_type' => 'product',
            'order' => 'ASC',
            'posts_per_page' => -1,
            'post_status' => array('publish', 'future'),
            'meta_query' => array(
                    array(
                            'key' => 'WooCommerceEventsEvent',
                            'value' => 'Event',
                            'compare' => '=',
                    ),
            ),
    );

    $appEvents = get_option('globalWooCommerceEventsAppEvents', 'all');

    if ( $appEvents != 'all' ) {

        if ( $appEvents === 'user' && $user != null ) {

            $args['author'] = $user->ID;

        } elseif ( $appEvents === 'id' ) {

            $appEventIDs = get_option('globalWooCommerceEventsAppEventIDs', array());
            
            if ( !empty($appEvents) ) {

                $args['post__in'] = $appEventIDs;

            }
        }
    }
    
    $query = new WP_Query($args);
    $events = $query->get_posts();

    $wp_date_format = get_option( 'date_format' );
    $date_format = $wp_date_format . ' H:i';

    foreach ( $events as &$event ) {
        
        $tempEvent = array();

        $event_meta = get_post_meta($event->ID);

        $tempEvent['WooCommerceEventsProductID'] = (string)$event->ID;
        $tempEvent['WooCommerceEventsName'] = (string)$event->post_title;
        $tempEvent['WooCommerceEventsDate'] = !empty($event_meta['WooCommerceEventsDate']) ? (string)$event_meta['WooCommerceEventsDate'][0] : date($wp_date_format, time());
        $tempEvent['WooCommerceEventsNumDays'] = !empty($event_meta['WooCommerceEventsNumDays']) ? (string)$event_meta['WooCommerceEventsNumDays'][0] : "1";

        if ( (int)$tempEvent['WooCommerceEventsNumDays'] > 1 ) {

            $tempEvent['WooCommerceEventsEndDate'] = !empty($event_meta['WooCommerceEventsEndDate']) ? (string)$event_meta['WooCommerceEventsEndDate'][0] : date($wp_date_format, time());
            
            $multiDayType = '';

            // Check if multiday event plugin is enabled
            if ( !function_exists('is_plugin_active') || !function_exists('is_plugin_active_for_network') ) {

                require_once(ABSPATH.'/wp-admin/includes/plugin.php');

            }
            
            if ( is_plugin_active('fooevents_multi_day/fooevents-multi-day.php') || is_plugin_active_for_network('fooevents_multi_day/fooevents-multi-day.php') ) {
        
                $Fooevents_Multiday_Events = new Fooevents_Multiday_Events();
                $multiDayType = $Fooevents_Multiday_Events->get_multi_day_type($event->ID);

            }

            if ( $multiDayType == 'select' ) {

                $tempEvent['WooCommerceEventsSelectDate'] = get_post_meta($event->ID, 'WooCommerceEventsSelectDate', true);

                if ( !empty($tempEvent['WooCommerceEventsSelectDate']) ) {
                    
                    if ( $tempEvent['WooCommerceEventsSelectDate'][0] != '' ) {

                        $tempEvent['WooCommerceEventsDate'] = $tempEvent['WooCommerceEventsSelectDate'][0];

                    }

                    if ( end($tempEvent['WooCommerceEventsSelectDate']) != '' ) {

                        $tempEvent['WooCommerceEventsEndDate'] = end($tempEvent['WooCommerceEventsSelectDate']);

                    }
                }

            }

        }

        $tempEvent['WooCommerceEventsHour'] = !empty($event_meta['WooCommerceEventsHour']) ? (string)$event_meta['WooCommerceEventsHour'][0] : '';
        $tempEvent['WooCommerceEventsMinutes'] = !empty($event_meta['WooCommerceEventsMinutes']) ? (string)$event_meta['WooCommerceEventsMinutes'][0] : '';
        $tempEvent['WooCommerceEventsPeriod'] = (string)strtoupper(str_replace('.', '', $event_meta['WooCommerceEventsPeriod'][0]));
        $tempEvent['WooCommerceEventsHourEnd'] = !empty($event_meta['WooCommerceEventsHourEnd']) ? (string)$event_meta['WooCommerceEventsHourEnd'][0] : '';
        $tempEvent['WooCommerceEventsMinutesEnd'] = !empty($event_meta['WooCommerceEventsMinutesEnd']) ? (string)$event_meta['WooCommerceEventsMinutesEnd'][0] : '';
        $tempEvent['WooCommerceEventsEndPeriod'] = (string)strtoupper(str_replace('.', '', $event_meta['WooCommerceEventsEndPeriod'][0]));
        $tempEvent['WooCommerceEventsTimeZone'] = !empty($event_meta['WooCommerceEventsTimeZone']) ? (string)$event_meta['WooCommerceEventsTimeZone'][0] : '';

        // Start Date
        $startPeriodFormat = $event_meta['WooCommerceEventsPeriod'][0] != '' ? ' A' : '';
        $startPeriod = $event_meta['WooCommerceEventsPeriod'][0] != '' ? ' ' . $event_meta['WooCommerceEventsPeriod'][0] : '';
        $WooCommerceEventsDate = convert_month_to_english($tempEvent['WooCommerceEventsDate']);
        $tempEvent['WooCommerceEventsDateFull'] = $WooCommerceEventsDate." ".$tempEvent['WooCommerceEventsHour'].':'.$tempEvent['WooCommerceEventsMinutes'].$startPeriod;

        $timezone = NULL;

        if ( $tempEvent['WooCommerceEventsTimeZone'] != '' ) {
            $timezone = new DateTimeZone($tempEvent['WooCommerceEventsTimeZone']);
        }

        $start_date = DateTime::createFromFormat($date_format.$startPeriodFormat, $tempEvent['WooCommerceEventsDateFull'], $timezone);

        if ( $start_date == false ) {

            $timestamp = strtotime($tempEvent['WooCommerceEventsDateFull']);

            if ( $timestamp == false ) {

                $timestamp = (string)time();

            }

            try {

                $start_date = new DateTime("@".$timestamp, $timezone);

            } catch (Exception $e) {

                $start_date = new DateTime("now", $timezone);

            }

        }

        $tempEvent['WooCommerceEventsDateTimestamp'] = (string)$start_date->getTimestamp();
        $tempEvent['WooCommerceEventsDateDay'] = $start_date->format('d');
        $tempEvent['WooCommerceEventsDateMonth'] = date_i18n('M', $tempEvent['WooCommerceEventsDateTimestamp']);
        $tempEvent['WooCommerceEventsDateYear'] = $start_date->format('Y');

        // End Date
        if ( (int)$tempEvent['WooCommerceEventsNumDays'] > 1 ) {

            $endPeriodFormat = $event_meta['WooCommerceEventsEndPeriod'][0] != '' ? ' A' : '';
            $endPeriod = $event_meta['WooCommerceEventsEndPeriod'][0] != '' ? ' ' . $event_meta['WooCommerceEventsEndPeriod'][0] : '';
            $WooCommerceEventsEndDate = convert_month_to_english($tempEvent['WooCommerceEventsEndDate']);
            $tempEvent['WooCommerceEventsEndDateFull'] = $WooCommerceEventsEndDate." ".$tempEvent['WooCommerceEventsHourEnd'].':'.$tempEvent['WooCommerceEventsMinutesEnd'].$endPeriod;

            $end_date = DateTime::createFromFormat($date_format.$endPeriodFormat, $tempEvent['WooCommerceEventsEndDateFull'], $timezone);

            if ( $end_date == false ) {

                $end_timestamp = strtotime($tempEvent['WooCommerceEventsEndDateFull']);

                if ( $end_timestamp == false ) {

                    $end_timestamp = (string)time();

                }

                try {

                    $end_date = new DateTime("@".$end_timestamp, $timezone);
        
                } catch (Exception $e) {
        
                    $end_date = new DateTime("now", $timezone);
        
                }

            }

            $tempEvent['WooCommerceEventsEndDateTimestamp'] = (string)$end_date->getTimestamp();
            $tempEvent['WooCommerceEventsEndDateDay'] = $end_date->format('d');
            $tempEvent['WooCommerceEventsEndDateMonth'] = date_i18n('M', $tempEvent['WooCommerceEventsEndDateTimestamp']);
            $tempEvent['WooCommerceEventsEndDateYear'] = $end_date->format('Y');

        }

        $event_image = get_the_post_thumbnail_url($event->ID);

        if ( $event_image == false ) {

            $event_image = !empty($event_meta['WooCommerceEventsTicketLogo']) ? (string)$event_meta['WooCommerceEventsTicketLogo'][0] : '';

        }

        $tempEvent['WooCommerceEventsTicketLogo'] = $event_image;
        $tempEvent['WooCommerceEventsTicketHeaderImage'] = !empty($event_meta['WooCommerceEventsTicketHeaderImage']) ? (string)$event_meta['WooCommerceEventsTicketHeaderImage'][0] : '';
        $tempEvent['WooCommerceEventsLocation'] = !empty($event_meta['WooCommerceEventsLocation']) ? (string)$event_meta['WooCommerceEventsLocation'][0] : '';
        $tempEvent['WooCommerceEventsSupportContact'] = !empty($event_meta['WooCommerceEventsSupportContact']) ? (string)$event_meta['WooCommerceEventsSupportContact'][0] : '';
        $tempEvent['WooCommerceEventsEmail'] = !empty($event_meta['WooCommerceEventsEmail']) ? (string)$event_meta['WooCommerceEventsEmail'][0] : '';
        $tempEvent['WooCommerceEventsGPS'] = !empty($event_meta['WooCommerceEventsGPS']) ? (string)$event_meta['WooCommerceEventsGPS'][0] : '';
        $tempEvent['WooCommerceEventsGoogleMaps'] = !empty($event_meta['WooCommerceEventsGoogleMaps']) ? (string)$event_meta['WooCommerceEventsGoogleMaps'][0] : '';
        $tempEvent['WooCommerceEventsDirections'] = !empty($event_meta['WooCommerceEventsDirections']) ? (string)$event_meta['WooCommerceEventsDirections'][0] : '';
        $tempEvent['WooCommerceEventsBackgroundColor'] = !empty($event_meta['WooCommerceEventsBackgroundColor']) ? (string)$event_meta['WooCommerceEventsBackgroundColor'][0] : '';
        
        if ( strpos($tempEvent['WooCommerceEventsBackgroundColor'], '#') === false ) {
            $tempEvent['WooCommerceEventsBackgroundColor'] = '';
        }

        $tempEvent['WooCommerceEventsTextColor'] = !empty($event_meta['WooCommerceEventsTextColor']) ? (string)$event_meta['WooCommerceEventsTextColor'][0] : '';

        if ( strpos($tempEvent['WooCommerceEventsTextColor'], '#') === false ) {
            $tempEvent['WooCommerceEventsTextColor'] = '';
        }

        $attendeeTerm = !empty($event_meta['WooCommerceEventsAttendeeOverride']) ? (string)$event_meta['WooCommerceEventsAttendeeOverride'][0] : '';

        if ( empty($attendeeTerm) ) {

            $attendeeTerm = (string)get_option('globalWooCommerceEventsAttendeeOverride', true);

        }

        if ( !empty($attendeeTerm) && $attendeeTerm != 1 ) {

            $tempEvent['WooCommerceEventsAttendeeOverride'] = $attendeeTerm;

        }

        $attendeeTermPlural = !empty($event_meta['WooCommerceEventsAttendeeOverridePlural']) ? (string)$event_meta['WooCommerceEventsAttendeeOverridePlural'][0] : '';

        if ( empty($attendeeTermPlural) ) {

            $attendeeTermPlural = (string)get_option('globalWooCommerceEventsAttendeeOverridePlural', true);

        }

        if ( !empty($attendeeTermPlural) && $attendeeTermPlural != 1 ) {

            $tempEvent['WooCommerceEventsAttendeeOverridePlural'] = $attendeeTermPlural;

        }

        $dayTerm = !empty($event_meta['WooCommerceEventsDayOverride']) ? (string)$event_meta['WooCommerceEventsDayOverride'][0] : '';

        if ( empty($dayTerm) ) {

            $dayTerm = (string)get_option('WooCommerceEventsDayOverride', true);

        }

        if ( !empty($dayTerm) && $dayTerm != 1) {

            $tempEvent['WooCommerceEventsDayOverride'] = $dayTerm;

        }

        $dayTermPlural = !empty($event_meta['WooCommerceEventsDayOverridePlural']) ? (string)$event_meta['WooCommerceEventsDayOverridePlural'][0] : '';

        if ( empty($dayTermPlural) ) {

            $dayTermPlural = (string)get_option('WooCommerceEventsDayOverridePlural', true);

        }

        if ( !empty($dayTermPlural) && $dayTermPlural != 1) {

            $tempEvent['WooCommerceEventsDayOverridePlural'] = $dayTermPlural;

        }

        $tempEvent['WooCommerceEventsBookingsBookingDetailsOverride'] = !empty($event_meta['WooCommerceEventsBookingsBookingDetailsOverride']) ? (string)$event_meta['WooCommerceEventsBookingsBookingDetailsOverride'][0] : '';
        $tempEvent['WooCommerceEventsBookingsBookingDetailsOverridePlural'] = !empty($event_meta['WooCommerceEventsBookingsBookingDetailsOverridePlural']) ? (string)$event_meta['WooCommerceEventsBookingsBookingDetailsOverridePlural'][0] : '';

        $tempEvent['WooCommerceEventsBookingsSlotOverride'] = !empty($event_meta['WooCommerceEventsBookingsSlotOverride']) ? (string)$event_meta['WooCommerceEventsBookingsSlotOverride'][0] : '';
        $tempEvent['WooCommerceEventsBookingsSlotOverridePlural'] = !empty($event_meta['WooCommerceEventsBookingsSlotOverridePlural']) ? (string)$event_meta['WooCommerceEventsBookingsSlotOverridePlural'][0] : '';

        $tempEvent['WooCommerceEventsBookingsDateOverride'] = !empty($event_meta['WooCommerceEventsBookingsDateOverride']) ? (string)$event_meta['WooCommerceEventsBookingsDateOverride'][0] : '';
        $tempEvent['WooCommerceEventsBookingsDateOverridePlural'] = !empty($event_meta['WooCommerceEventsBookingsDateOverridePlural']) ? (string)$event_meta['WooCommerceEventsBookingsDateOverridePlural'][0] : '';
        
        $eventType = 'single';

        if ( !empty($event_meta['WooCommerceEventsType']) ) {

            $eventType = $event_meta['WooCommerceEventsType'][0];

        } else {

            if ( (int)$tempEvent['WooCommerceEventsNumDays'] > 1 ) {
                
                if ( !empty($event_meta['WooCommerceEventsMultiDayType']) ) {

                    $eventType = $event_meta['WooCommerceEventsMultiDayType'][0];

                }

            }
        
        }

        $tempEvent['WooCommerceEventsType'] = $eventType;

        if ( !function_exists('is_plugin_active') || !function_exists('is_plugin_active_for_network') ) {

            require_once(ABSPATH.'/wp-admin/includes/plugin.php');

        }

        if ( (is_plugin_active('fooevents_bookings/fooevents-bookings.php') || is_plugin_active_for_network('fooevents_bookings/fooevents-bookings.php')) && $eventType == "bookings" ) {

            $FooEvents_Bookings = new FooEvents_Bookings();

            $fooevents_bookings_options_serialized = $event_meta['fooevents_bookings_options_serialized'][0];
            $fooevents_bookings_options = json_decode($fooevents_bookings_options_serialized, true);
            
            $booking_slots = $FooEvents_Bookings->process_booking_options($fooevents_bookings_options);

            foreach ( $booking_slots as $key => &$options ) {

                $options['add_date_ids'] = array_keys($options['add_date']);

                foreach ( $options['add_date'] as $add_date_key => &$date_options ) {

                    $date_options['stock'] = (string)$date_options['stock'];

                    $slot_date = convert_month_to_english($date_options['date']);
        
                    $date_options['date_timestamp'] = (string)strtotime($slot_date . ' 12:00');

                }

            }

            $tempEvent['WooCommerceEventsBookingOptionIDs'] = array_keys($booking_slots);
            $tempEvent['WooCommerceEventsBookingOptions'] = $booking_slots;

        }

        $eventsArray[] = $tempEvent;

        unset($tempEvent);

    }

    return $eventsArray;

}

/**
 * Get a single ticket's data
 * 
 * @param string $ticket_id
 * @param bool $hide_personal_info
 * @return array ticketArray
 */
function getTicketData($ticket_id, $hide_personal_info) {

    $tempTicket = array();

    $order_id = get_post_meta($ticket_id, 'WooCommerceEventsOrderID', true);

    try {

        $order = new WC_Order( $order_id );

        $tempTicket['customerFirstName'] = (string)$order->get_billing_first_name();
        $tempTicket['customerLastName'] = (string)$order->get_billing_last_name();
        $tempTicket['customerEmail'] = $hide_personal_info ? '***' : (string)$order->get_billing_email();
        $tempTicket['customerPhone'] = $hide_personal_info ? '***' : (string)$order->get_billing_phone();

        if ( trim($tempTicket['customerFirstName']) == "" ) {

            $tempTicket['customerFirstName'] = get_post_meta($ticket_id, 'WooCommerceEventsPurchaserFirstName', true);
            $tempTicket['customerLastName'] = get_post_meta($ticket_id, 'WooCommerceEventsPurchaserLastName', true);
            $tempTicket['customerEmail'] = $hide_personal_info ? '***' : get_post_meta($ticket_id, 'WooCommerceEventsPurchaserEmail', true);

        }

        $tempTicket['WooCommerceEventsAttendeeName'] = (string)get_post_meta($ticket_id, 'WooCommerceEventsAttendeeName', true);
        $tempTicket['WooCommerceEventsAttendeeLastName'] = (string)get_post_meta($ticket_id, 'WooCommerceEventsAttendeeLastName', true);
        $tempTicket['WooCommerceEventsAttendeeEmail'] = $hide_personal_info ? '***' : (string)get_post_meta($ticket_id, 'WooCommerceEventsAttendeeEmail', true);           
        $tempTicket['WooCommerceEventsAttendeeTelephone'] = $hide_personal_info ? '***' : (string)get_post_meta($ticket_id, 'WooCommerceEventsAttendeeTelephone', true);           
        $tempTicket['WooCommerceEventsAttendeeCompany'] = $hide_personal_info ? '***' : (string)get_post_meta($ticket_id, 'WooCommerceEventsAttendeeCompany', true);            
        $tempTicket['WooCommerceEventsAttendeeDesignation'] = $hide_personal_info ? '***' : (string)get_post_meta($ticket_id, 'WooCommerceEventsAttendeeDesignation', true);
        $tempTicket['WooCommerceEventsTicketID'] = (string)get_post_meta($ticket_id, 'WooCommerceEventsTicketID', true);
        $tempTicket['WooCommerceEventsStatus'] = (string)get_post_meta($ticket_id, 'WooCommerceEventsStatus', true); 
        $tempTicket['WooCommerceEventsMultidayStatus'] = (string)get_post_meta($ticket_id, 'WooCommerceEventsMultidayStatus', true);
        $tempTicket['WooCommerceEventsTicketType'] = (string)get_post_meta($ticket_id, 'WooCommerceEventsTicketType', true);
        $tempTicket['WooCommerceEventsVariationID'] = (string)get_post_meta($ticket_id, 'WooCommerceEventsVariationID', true);
        $tempTicket['WooCommerceEventsProductID'] = (string)get_post_meta($ticket_id, 'WooCommerceEventsProductID', true);

        $ticketNumDays = (string)get_post_meta($tempTicket['WooCommerceEventsProductID'], "WooCommerceEventsNumDays", true);

        $tempTicket['WooCommerceEventsNumDays'] = $ticketNumDays == "" ? "1" : $ticketNumDays;
        
        $tempTicket['WooCommerceEventsOrderID'] = (string)get_post_meta($ticket_id, 'WooCommerceEventsOrderID', true);
        $tempTicket['WooCommerceEventsTicketPrice'] = (string)get_post_meta($ticket_id, 'WooCommerceEventsPrice', true);
        $tempTicket['WooCommerceEventsTicketPriceText'] = (string)html_entity_decode(strip_tags(get_post_meta($ticket_id, 'WooCommerceEventsPrice', true)), ENT_HTML5, "UTF-8");
        
        $tempTicket['WooCommerceEventsBookingSlotID'] = (string)get_post_meta($ticket_id, 'WooCommerceEventsBookingSlotID', true);
        $tempTicket['WooCommerceEventsBookingSlot'] = (string)get_post_meta($ticket_id, 'WooCommerceEventsBookingSlot', true);
        $tempTicket['WooCommerceEventsBookingDate'] = (string)get_post_meta($ticket_id, 'WooCommerceEventsBookingDate', true);

        $WooCommerceEventsBookingDate = convert_month_to_english($tempTicket['WooCommerceEventsBookingDate']);
        
        $tempTicket['WooCommerceEventsBookingDateTimestamp'] = (string)strtotime($WooCommerceEventsBookingDate . ' 12:00');

        $WooCommerceEventsVariations = get_post_meta($ticket_id, 'WooCommerceEventsVariations', true);

        $WooCommerceEventsVariationsOutput = array();

        if ( !empty($WooCommerceEventsVariations) ) {

            foreach ( $WooCommerceEventsVariations as $variationName => $variationValue ) {

                $variationNameOutput = str_replace('attribute_', '', $variationName);
                $variationNameOutput = str_replace('pa_', '', $variationNameOutput);
                $variationNameOutput = str_replace('_', ' ', $variationNameOutput);
                $variationNameOutput = str_replace('-', ' ', $variationNameOutput);
                $variationNameOutput = str_replace('Pa_', '', $variationNameOutput);
                $variationNameOutput = ucwords($variationNameOutput);                    

                $variationValueOutput = str_replace('_', ' ', $variationValue);
                $variationValueOutput = str_replace('-', ' ', $variationValueOutput);
                $variationValueOutput = ucwords($variationValueOutput);

                $WooCommerceEventsVariationsOutput[$variationNameOutput] = (string)$variationValueOutput;

            }

        }

        $tempTicket['WooCommerceEventsVariations'] = $WooCommerceEventsVariationsOutput;

        // Check if custom attendee fields plugin is enabled
        if ( !function_exists('is_plugin_active') || !function_exists('is_plugin_active_for_network') ) {

            require_once(ABSPATH.'/wp-admin/includes/plugin.php');

        }
        
        // Custom Attendee Fields
        $custom_values = array();

        if ( is_plugin_active('fooevents_custom_attendee_fields/fooevents-custom-attendee-fields.php') || is_plugin_active_for_network('fooevents_custom_attendee_fields/fooevents-custom-attendee-fields.php') ) {

            $Fooevents_Custom_Attendee_Fields = new Fooevents_Custom_Attendee_Fields();

            $fooevents_custom_attendee_fields_options = $Fooevents_Custom_Attendee_Fields->display_tickets_meta_custom_options_output($ticket_id, $tempTicket['WooCommerceEventsProductID']);
            
            foreach ( $fooevents_custom_attendee_fields_options as $key => $field ) {

                $custom_values[$field['label']] = $field['value'];
                
            }

        }

        $tempTicket['WooCommerceEventsCustomAttendeeFields'] = $custom_values;

        // Seating
        $ticket_meta = get_post_meta($ticket_id);
        $row_name = '';
        $seat_number = '';

        foreach ( $ticket_meta as $meta_key => $meta_value ) {

            if ( strpos($meta_key, 'fooevents_seat_row_name_') !== false ) {

                $row_name = (string)$meta_value[0];

            } elseif ( strpos($meta_key, 'fooevents_seat_number_') !== false ) {

                $seat_number = (string)$meta_value[0];

            }

        }

        $tempTicket['WooCommerceEventsRowName'] = $row_name;
        $tempTicket['WooCommerceEventsSeatNumber'] = $seat_number;
        $tempTicket['WooCommerceEventsTicketExpireTimestamp'] = (string)get_post_meta($ticket_id, 'WooCommerceEventsTicketExpireTimestamp', true);
        
        return $tempTicket;

    }

    catch ( Exception $e ) {

        return array();

    }

}

/**
 * Get all tickets for an event as an array
 * 
 * @param string $eventID
 * @return array ticketsArray
 */

function getEventTickets($eventID) {

    global $woocommerce;
    $ticketsArray = array();
    $ticketStatusOptions = array();
    
    $eventID = sanitize_text_field($eventID);
    
    $hide_personal_info = get_option('globalWooCommerceEventsAppHidePersonalInfo', false);

    $globalWooCommerceHideUnpaidTicketsApp = get_option('globalWooCommerceHideUnpaidTicketsApp', true);

    if ( $globalWooCommerceHideUnpaidTicketsApp == 'yes' ) {

        $ticketStatusOptions = array('key' => 'WooCommerceEventsStatus', 'compare' => '!=', 'value' => 'Unpaid');

    }

    $events_query = new WP_Query( array('post_type' => array('event_magic_tickets'), 'posts_per_page' => -1, 'fields' => 'ids', 'meta_query' => array( array( 'key' => 'WooCommerceEventsProductID', 'value' => $eventID ), $ticketStatusOptions )) );
    $ticket_ids = $events_query->get_posts();

    foreach ( $ticket_ids as $ticket_id ) {

        $tempTicket = getTicketData($ticket_id, $hide_personal_info);

        if ( !empty($tempTicket) ) {

            $ticketsArray[] = $tempTicket;

        }

        unset($tempTicket);

    }

    return $ticketsArray;

}

/**
 * Get all updated tickets for an event as an array
 * 
 * @param string $eventID
 * @param int $since
 * @return array ticketsArray
 */

function getEventUpdatedTickets($eventID, $since) {

    global $woocommerce;
    global $wpdb;

    $table_name = $wpdb->prefix . 'fooevents_check_in';
    $postmeta_table_name = $wpdb->prefix . 'postmeta';
    
    $ticketsArray = array();
    $ticketStatusOptions = array();
    
    $eventID = sanitize_text_field($eventID);
    $since = sanitize_text_field($since);

    $tickets = $wpdb->get_results("
        SELECT * FROM ".$table_name."
        LEFT JOIN ".$postmeta_table_name." ON
            ".$table_name.".tid = ".$postmeta_table_name.".post_id AND
            ".$postmeta_table_name.".meta_key = 'WooCommerceEventsTicketID'
        WHERE
            eid = ".$eventID." AND
            checkin >= ".$since."
    ");

    foreach ( $tickets as $ticket ) {

        $ticketsArray[] = array(
            'WooCommerceEventsTicketID' => $ticket->meta_value,
            'Day' => (string)$ticket->day
        );

    }

    return $ticketsArray;

}

/**
 * Get a single ticket if it exists
 * 
 * @param string $ticketID
 * @return array ticket
 */

function getSingleTicket($ticketID) {

    $ticketID = sanitize_text_field($ticketID);

    $ticket_query = new WP_Query( array('post_type' => array('event_magic_tickets'), 'meta_query' => array( array( 'key' => 'WooCommerceEventsTicketID', 'value' => $ticketID ) )) );

    $ticket_posts = $ticket_query->get_posts();

    $output = array();

    $hide_personal_info = get_option('globalWooCommerceEventsAppHidePersonalInfo', false);

    if ( !empty($ticket_posts) ) {

        $ticket_post = $ticket_posts[0];

        $tempTicket = getTicketData($ticket_post->ID, $hide_personal_info);

        if ( !empty($tempTicket) ) {

            $output['data'] = $tempTicket;

        } else {

            $output['status'] = 'error';

        }

    } else {

        $output['status'] = 'error';

    }

    return $output;
}

/**
 * Update ticket ID with the provided status
 * @param type $ticketID
 * @param type $status
 */
function updateTicketStatus($ticketID, $status) {

    global $wpdb;
    $table_name = $wpdb->prefix . 'fooevents_check_in';
    
    $events_query = new WP_Query( array('post_type' => array('event_magic_tickets'), 'meta_query' => array( array( 'key' => 'WooCommerceEventsTicketID', 'value' => $ticketID ) )) );
    $ticket = $events_query->get_posts();
    $ticket = $ticket[0];

    $eventID = get_post_meta($ticket->ID, 'WooCommerceEventsProductID', true);
    
    $timestamp = current_time('timestamp');

    if ( !empty($status) ) {

        $statusChanged = false;

        $currentStatus = get_post_meta($ticket->ID, 'WooCommerceEventsStatus', true);

        if ( $currentStatus != $status ) {

            $statusChanged = true;

            update_post_meta( $ticket->ID, 'WooCommerceEventsStatus', strip_tags( $status ));

        }

        // Check if multiday event plugin is enabled
        if ( !function_exists('is_plugin_active') || !function_exists('is_plugin_active_for_network') ) {

            require_once(ABSPATH.'/wp-admin/includes/plugin.php');

        }
        
        if ( is_plugin_active('fooevents_multi_day/fooevents-multi-day.php') || is_plugin_active_for_network('fooevents_multi_day/fooevents-multi-day.php') ) {
            
            $WooCommerceEventsNumDays = (int)get_post_meta($eventID, "WooCommerceEventsNumDays", true);
            
            if ( $WooCommerceEventsNumDays > 1 ) {

                $WooCommerceEventsMultidayStatus = array();
                
                for ( $day = 1; $day <= $WooCommerceEventsNumDays; $day++ ) {

                    $WooCommerceEventsMultidayStatus[$day] = strip_tags( $status );

                    if ( $status == 'Checked In' ) {

                        $wpdb->insert($table_name, array(
                            'tid' => $ticket->ID,
                            'eid' => $eventID,
                            'day' => $day,
                            'checkin' => $timestamp
                        ));
        
                    }

                }
                
                $WooCommerceEventsMultidayStatus = json_encode($WooCommerceEventsMultidayStatus);
                
                update_post_meta($ticket->ID, 'WooCommerceEventsMultidayStatus', strip_tags($WooCommerceEventsMultidayStatus));
                
            } else {
            
                if ( $status == 'Checked In' ) {
    
                    $wpdb->insert($table_name, array(
                        'tid' => $ticket->ID,
                        'eid' => $eventID,
                        'day' => 1,
                        'checkin' => $timestamp
                    ));
    
                }
                
            }
            
        } else {
            
            if ( $status == 'Checked In' ) {

                $wpdb->insert($table_name, array(
                    'tid' => $ticket->ID,
                    'eid' => $eventID,
                    'day' => 1,
                    'checkin' => $timestamp
                ));

            }
            
        }

        return $statusChanged ? 'Status updated' : 'Status unchanged';

    } else {

        return 'Status is required';

    }
}

/**
 * Update multiple ticket IDs with the provided statuses
 * @param type $ticketsStatus
 */
function updateTicketMultipleStatus($ticketsStatus) {

    $output = array();

    $ticketsStatus = json_decode($ticketsStatus, true);

    if ( !empty($ticketsStatus) ) {

        foreach($ticketsStatus as $tempTicketID => $status) {

            if ( strpos($tempTicketID, "_") !== false ) {

                $tempTicketArray = explode("_", $tempTicketID);
                
                $ticketID = $tempTicketArray[0];
                $day = $tempTicketArray[1];
                
                $output['message'][$ticketID] = updateTicketMultidayStatus($ticketID, $status, $day);

            } else {

                $output['message'][$tempTicketID] = updateTicketStatus($tempTicketID, strip_tags( $status ));

            }
        }

    } else {

        $output['message'] = 'Status is required';

    }

    return $output;
}

/**
 * Update ticket ID status for a specified day in a multiday event
 * @param type $ticketID
 * @param type $status
 * @param type $day
 */
function updateTicketMultidayStatus($ticketID, $status, $day) {
    
    global $wpdb;
    $table_name = $wpdb->prefix . 'fooevents_check_in';
    
    $ticketID = sanitize_text_field($ticketID);
    $status = sanitize_text_field($status);
    $day = sanitize_text_field($day);
    
    $events_query = new WP_Query( array('post_type' => array('event_magic_tickets'), 'meta_query' => array( array( 'key' => 'WooCommerceEventsTicketID', 'value' => $ticketID ) )) );
    $ticket = $events_query->get_posts();

    if ( !empty($ticket) ) {
        
        $ticket = $ticket[0];
        $eventID = get_post_meta($ticket->ID, 'WooCommerceEventsProductID', true);
        
        $timestamp = current_time('timestamp');

        if ( $status == 'Checked In' ) {

            $wpdb->insert($table_name, array(
                'tid' => $ticket->ID,
                'eid' => $eventID,
                'day' => $day,
                'checkin' => $timestamp
            ));

        }
        
        $WooCommerceEventsMultidayStatus = get_post_meta($ticket->ID, "WooCommerceEventsMultidayStatus", true);
        $WooCommerceEventsMultidayStatus = json_decode($WooCommerceEventsMultidayStatus, true);
        
        $statusChanged = $WooCommerceEventsMultidayStatus[$day] != $status;

        $WooCommerceEventsMultidayStatus[$day] = $status;

        $all_days_same_status = true;

        foreach ( $WooCommerceEventsMultidayStatus as $day => $multiday_status ) {

            if ( $multiday_status != $status ) {

                $all_days_same_status = false;

                break;

            }

        }

        $WooCommerceEventsMultidayStatus = json_encode($WooCommerceEventsMultidayStatus);
        
        update_post_meta($ticket->ID, 'WooCommerceEventsMultidayStatus', strip_tags($WooCommerceEventsMultidayStatus));

        if ( $all_days_same_status ) {

            update_post_meta($ticket->ID, 'WooCommerceEventsStatus', strip_tags($status));

            if ( $status == 'Checked In' ) {

                $wpdb->insert($table_name, array(
                    'tid' => $ticket->ID,
                    'eid' => $eventID,
                    'day' => 1,
                    'checkin' => $timestamp
                ));

            }
            
        } else {

            update_post_meta($ticket->ID, 'WooCommerceEventsStatus', 'Not Checked In');

        }
        
        return $statusChanged ? 'Status updated' : 'Status unchanged';
    }
    else
    {
        return 'Status not updated';
    }
}

/**
 * Output the name of a custom field
 * @param type $field_name
 */
function fooevents_output_custom_field_name($field_name) {

    $field_name = str_replace('fooevents_custom_', "", $field_name);
    $field_name = str_replace('_', " ", $field_name);
    $field_name = ucwords($field_name);

    return $field_name;

}

/**
 * Array of month names for translation to English
 * 
 * @param string $event_date
 * @return string
 */
function convert_month_to_english($event_date) {
    
    $months = array(
        // French
        'janvier' => 'January',
        'février' => 'February',
        'mars' => 'March',
        'avril' => 'April',
        'mai' => 'May',
        'juin' => 'June',
        'juillet' => 'July',
        'aout' => 'August',
        'août' => 'August',
        'septembre' => 'September',
        'octobre' => 'October',

        // German
        'Januar' => 'January',
        'Februar' => 'February',
        'März' => 'March',
        'Mai' => 'May',
        'Juni' => 'June',
        'Juli' => 'July',
        'Oktober' => 'October',
        'Dezember' => 'December',

        // Spanish
        'enero' => 'January',
        'febrero' => 'February',
        'marzo' => 'March',
        'abril' => 'April',
        'mayo' => 'May',
        'junio' => 'June',
        'julio' => 'July',
        'agosto' => 'August',
        'septiembre' => 'September',
        'setiembre' => 'September',
        'octubre' => 'October',
        'noviembre' => 'November',
        'diciembre' => 'December',
        'novembre' => 'November',
        'décembre' => 'December',

        // Dutch
        'januari' => 'January',
        'februari' => 'February',
        'maart' => 'March',
        'april' => 'April',
        'mei' => 'May',
        'juni' => 'June',
        'juli' => 'July',
        'augustus' => 'August',
        'september' => 'September',
        'oktober' => 'October',
        'november' => 'November',
        'december' => 'December',

        // Italian
        'Gennaio' => 'January',
        'Febbraio' => 'February',
        'Marzo' => 'March',
        'Aprile' => 'April',
        'Maggio' => 'May',
        'Giugno' => 'June',
        'Luglio' => 'July',
        'Agosto' => 'August',
        'Settembre' => 'September',
        'Ottobre' => 'October',
        'Novembre' => 'November',
        'Dicembre' => 'December',

        // Polish
        'Styczeń' => 'January',
        'Luty' => 'February',
        'Marzec' => 'March',
        'Kwiecień' => 'April',
        'Maj' => 'May',
        'Czerwiec' => 'June',
        'Lipiec' => 'July',
        'Sierpień' => 'August',
        'Wrzesień' => 'September',
        'Październik' => 'October',
        'Listopad' => 'November',
        'Grudzień' => 'December',

        // Afrikaans
        'Januarie' => 'January',
        'Februarie' => 'February',
        'Maart' => 'March',
        'Mei' => 'May',
        'Junie' => 'June',
        'Julie' => 'July',
        'Augustus' => 'August',
        'Oktober' => 'October',
        'Desember' => 'December',

        // Turkish
        'Ocak' => 'January',
        'Şubat' => 'February',
        'Mart' => 'March',
        'Nisan' => 'April',
        'Mayıs' => 'May',
        'Haziran' => 'June',
        'Temmuz' => 'July',
        'Ağustos' => 'August',
        'Eylül' => 'September',
        'Ekim' => 'October',
        'Kasım' => 'November',
        'Aralık' => 'December',

        // Portuguese
        'janeiro' => 'January',
        'fevereiro' => 'February',
        'março' => 'March',
        'abril' => 'April',
        'maio' => 'May',
        'junho' => 'June',
        'julho' => 'July',
        'agosto' => 'August',
        'setembro' => 'September',
        'outubro' => 'October',
        'novembro' => 'November',
        'dezembro' => 'December',

        // Swedish
        'Januari' => 'January',
        'Februari' => 'February',
        'Mars' => 'March',
        'April' => 'April',
        'Maj' => 'May',
        'Juni' => 'June',
        'Juli' => 'July',
        'Augusti' => 'August',
        'September' => 'September',
        'Oktober' => 'October',
        'November' => 'November',
        'December' => 'December',

        // Czech
        'leden' => 'January',
        'únor' => 'February',
        'březen' => 'March',
        'duben' => 'April',
        'květen' => 'May',
        'červen' => 'June',
        'červenec' => 'July',
        'srpen' => 'August',
        'září' => 'September',
        'říjen' => 'October',
        'listopad' => 'November',
        'prosinec' => 'December',

        // Norwegian
        'januar' => 'January',
        'februar' => 'February',
        'mars' => 'March',
        'april' => 'April',
        'mai' => 'May',
        'juni' => 'June',
        'juli' => 'July',
        'august' => 'August',
        'september' => 'September',
        'oktober' => 'October',
        'november' => 'November',
        'desember' => 'December',

        // Danish
        'januar' => 'January',
        'februar' => 'February',
        'marts' => 'March',
        'april' => 'April',
        'maj' => 'May',
        'juni' => 'June',
        'juli' => 'July',
        'august' => 'August',
        'september' => 'September',
        'oktober' => 'October',
        'november' => 'November',
        'december' => 'December',

        // Finnish
        'tammikuu' => 'January',
        'helmikuu' => 'February',
        'maaliskuu' => 'March',
        'huhtikuu' => 'April',
        'toukokuu' => 'May',
        'kesäkuu' => 'June',
        'heinäkuu' => 'July',
        'elokuu' => 'August',
        'syyskuu' => 'September',
        'lokakuu' => 'October',
        'marraskuu' => 'November',
        'joulukuu' => 'December',

        // Russian
        'Январь' => 'January', 
        'Февраль' => 'February', 
        'Март' => 'March', 
        'Апрель' => 'April', 
        'Май' => 'May', 
        'Июнь' => 'June', 
        'Июль' => 'July', 
        'Август' => 'August', 
        'Сентябрь' => 'September', 
        'Октябрь' => 'October', 
        'Ноябрь' => 'November', 
        'Декабрь' => 'December',

        // Icelandic
        'Janúar' => 'January', 
        'Febrúar' => 'February', 
        'Mars' => 'March', 
        'Apríl' => 'April', 
        'Maí' => 'May', 
        'Júní' => 'June', 
        'Júlí' => 'July', 
        'Ágúst' => 'August', 
        'September' => 'September', 
        'Oktober' => 'October', 
        'Nóvember' => 'November', 
        'Desember' => 'December',

        // Latvian
        'janvāris' => 'January',
        'februāris' => 'February',
        'marts' => 'March',
        'aprīlis' => 'April',
        'maijs' => 'May',
        'jūnijs' => 'June',
        'jūlijs' => 'July',
        'augusts' => 'August',
        'septembris' => 'September',
        'oktobris' => 'October',
        'novembris' => 'November',
        'decembris' => 'December',

        // Lithuanian
        'Sausis' => 'January',
        'Vasaris' => 'February',
        'Kovas' => 'March',
        'Balandis' => 'April',
        'Gegužė' => 'May',
        'Birželis' => 'June',
        'Liepa' => 'July',
        'Rugpjūtis' => 'August',
        'Rugsėjis' => 'September',
        'Spalis' => 'October',
        'Lapkritis' => 'November',
        'Gruodis' => ' December',

        // Greek
        'Ιανουάριος' => 'January',
        'Φεβρουάριος' => 'February',
        'Μάρτιος' => 'March',
        'Απρίλιος' => 'April',
        'Μάιος' => 'May',
        'Ιούνιος' => 'June',
        'Ιούλιος' => 'July',
        'Αύγουστος' => 'August',
        'Σεπτέμβριος' => 'September',
        'Οκτώβριος' => 'October',
        'Νοέμβριος' => 'November',
        'Δεκέμβριος' => 'December',

        // Slovak - Slovakia
        "január" => "January",
        "február" => "February",
        "marec" => "March",
        "apríl" => "April",
        "máj" => "May",
        "jún" => "June",
        "júl" => "July",
        "august" => "August",
        "september" => "September",
        "október" => "October",
        "november" => "November",
        "december" => "December",
    );
    
    $pattern = array_keys($months);
    $replacement = array_values($months);

    foreach ($pattern as $key => $value) {
        
        $pattern[$key] = '/\b'.$value.'\b/i';
        
    }
    
    $replaced_event_date = preg_replace($pattern, $replacement, $event_date);

    $replaced_event_date = str_replace(' de ', ' ', $replaced_event_date);

    return $replaced_event_date;
    
}