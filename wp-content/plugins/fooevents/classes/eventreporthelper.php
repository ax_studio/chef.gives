<?php if(!defined('ABSPATH')) exit;

// WP_List_Table is not loaded automatically so we need to load it in our application
if(!class_exists('WP_List_Table')) {
    
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
    
}

class FooEvents_Event_Report_Helper extends WP_List_Table {
    
    public $Config;
    public $example_data;

    public function __construct($Config) {
        
        $this->Config = $Config;

        require_once($this->Config->classPath.'eventreporttable.php');
        require_once($this->Config->classPath.'checkintable.php');
        
        add_action('admin_menu',  array( $this, 'add_menu_item'));
        add_action('wp_ajax_fetch_tickets_sold', array($this, 'fetch_tickets_sold'));
        add_action('wp_ajax_fetch_tickets_revenue', array($this, 'fetch_tickets_revenue'));
        add_action('wp_ajax_fetch_revenue_formatted', array($this, 'fetch_revenue_formatted'));
        add_action('wp_ajax_fetch_check_ins', array($this, 'fetch_check_ins'));
        add_action('wp_ajax_fetch_check_ins_today', array($this, 'fetch_check_ins_today'));
        
    }
    
    /**
     * Add admin reports menu item
     * 
     */
    public function add_menu_item() {
        
        add_submenu_page('fooevents', __('Reports', 'woocommerce-events'), __('Reports', 'woocommerce-events'), 'edit_posts', 'fooevents-reports', array( $this, 'display_report_table_page'));
        add_submenu_page(NULL, __('Report', 'woocommerce-events'), 'Test', 'edit_posts', 'fooevents-event-report', array($this, 'display_report_page')); 
        
    }
    
    /**
     * Display ticket themes page
     * 
     */
    public function display_report_table_page() {

        $eventsListTable = new Events_List_Table();
        $eventsListTable->prepare_items();

        include($this->Config->templatePath.'reports_event_listing.php'); 
        
    }
    
    /**
     * Builds and displays the event report page
     * 
     */
    public function display_report_page() {
        
        $checkInListTable = new Check_in_List_Table();
        $checkInListTable->prepare_items();
        
        $id = sanitize_text_field($_GET['event']);
        $event = get_post($id);
        
        $dateFormat = get_option('date_format');
        
        if($dateFormat == 'd/m/Y') {
            
            $dateFormat = 'd-m-Y';
            
        }
        
        $todaysDate = date($dateFormat);
        $previousDate = date($dateFormat, strtotime('-10 days'));
        $previousMonth= date($dateFormat, strtotime('-30 days'));
        $previous90days= date($dateFormat, strtotime('-39 days'));
        $previousYear= date($dateFormat, strtotime('-365 days')); 
        $WooCommerceEventsDate = get_post_meta($id, 'WooCommerceEventsDate', true);
        $WooCommerceEventsLocation = get_post_meta($id, 'WooCommerceEventsLocation', true);
        
        if(isset($_POST['WooCommerceEventsDateFrom'])) {
            
            $previousDate = sanitize_text_field($_POST['WooCommerceEventsDateFrom']);
            
        }
        
        if(isset($_POST['WooCommerceEventsDateTo'])) {
            
            $todaysDate = sanitize_text_field($_POST['WooCommerceEventsDateTo']);
            
        }

        include($this->Config->templatePath.'reports_report_page.php'); 
        
    }
    
    /**
     * Fetches number check-in pre hour of today
     * 
     */
    public function fetch_check_ins_today() {
        
        $id = sanitize_text_field($_POST['eventID']);
        $dateFrom = $this->convert_month_to_english(sanitize_text_field($_POST['dateFrom']));
        $dateTo = $this->convert_month_to_english(sanitize_text_field($_POST['dateTo']));

        $requestedHours = $this->_fetch_all_hours_for_date($dateTo);
        $checkInsPerHour = array();
        
        foreach ($requestedHours as $hour) {
            
            $hourFormatted = date('H:00', $hour);
            $checkInsPerHour[$hourFormatted] = $this->_fetch_check_ins_on_hour($hour, $id);
            
        }
        
        echo json_encode($checkInsPerHour);
        
        exit();
        
    }
    
    /**
     * Fetches number check-ins per day between two dates
     * 
     */
    public function fetch_check_ins() {
        
        $id = sanitize_text_field($_POST['eventID']);
        $dateFrom = $this->convert_month_to_english(sanitize_text_field($_POST['dateFrom']));
        $dateTo = $this->convert_month_to_english(sanitize_text_field($_POST['dateTo']));
        
        $requestedDates = $this->_all_dates_between($dateFrom, $dateTo);
        $checkInsPerDay = array();
        
        foreach($requestedDates as $day) {
            
            $checkInsPerDay[$day] = $this->_fetch_check_ins_on_day($day, $id);
            
        }
        
        echo json_encode($checkInsPerDay);
        
        exit;
        
    }
    
    /**
     * Fetches number tickets sold per day between two dates
     * 
     */
    public function fetch_tickets_sold() {
        
        $id = sanitize_text_field($_POST['eventID']);
        $dateFrom = $this->convert_month_to_english(sanitize_text_field($_POST['dateFrom']));
        $dateTo = $this->convert_month_to_english(sanitize_text_field($_POST['dateTo']));
        $canceledTickets = sanitize_text_field($_POST['canceledTickets']);
        
        $filters['canceledTickets'] = $canceledTickets;
        
        $requestedDates = $this->_all_dates_between($dateFrom, $dateTo);
        $ticketsSoldPerDay = array();
        
        foreach($requestedDates as $day) {
            
            $ticketsSoldPerDay[$day] = $this->_fetch_tickets_sold_on_day($day, $id, $filters);
            
        }
        
        echo json_encode($ticketsSoldPerDay);
        
        exit;
    }
    
    /**
     * Fetches tickets revenue per day between two dates
     * 
     */
    public function fetch_tickets_revenue() {
        
        $id = sanitize_text_field($_POST['eventID']);
        $dateFrom = $this->convert_month_to_english(sanitize_text_field($_POST['dateFrom']));
        $dateTo = $this->convert_month_to_english(sanitize_text_field($_POST['dateTo']));
        
        $requestedDates = $this->_all_dates_between($dateFrom, $dateTo);
        $ticketsRevenuePerDay = array();
        
        foreach($requestedDates as $day) {
            
            $ticketsRevenuePerDay[$day] = $this->_fetch_tickets_revenue_on_day($day, $id);
            
        }
        
        echo json_encode($ticketsRevenuePerDay);
        
        exit();
    }
    
    /**
     * Formats revenue with store currency
     * 
     */
    public function fetch_revenue_formatted() {
        
        $total_revenue = sanitize_text_field($_POST['total_revenue']);
        echo wc_price($total_revenue);
        
        exit();
        
    }
    
    /**
     * Fetches the dates between two given days
     * 
     * @param string $previousDate
     * @param string $todaysDate
     * @param string $step
     * @param string $output_format
     * @return array
     */
    private function _all_dates_between($previousDate, $todaysDate, $step = '+1 day', $output_format = 'Y-m-d') {
        
        $dates = array();
        $current = strtotime($previousDate);
        $last = strtotime($todaysDate);

        while($current <= $last) {

            $dates[] = date($output_format, $current);
            $current = strtotime($step, $current);
            
        }

        return $dates;

    }
    
    /**
     * Fetches tickets sold on a particular day
     * 
     * @global object $wpdb
     * @param string $day
     * @param int $id
     * @return int
     */
    private function _fetch_tickets_sold_on_day($day, $ID, $filters) {

        
        $meta_query = array(
                        'relation' => 'AND',
                        array('key' => 'WooCommerceEventsProductID', 'value' => $ID), 
                    );

        if($filters['canceledTickets'] == 'no') {
            
            array_push($meta_query, array('key' => 'WooCommerceEventsStatus', 'value' => array('Canceled', 'Cancelled'), 'compare' => 'NOT IN'));
            
        } 
        
        $events_query = new WP_Query(array('post_type' => array('event_magic_tickets'), 'posts_per_page' => -1, 'date_query'=> array('column' => 'post_date', 'after' => $day, 'before' => $day, 'inclusive' => true), 'meta_query' => $meta_query));

        return $events_query->found_posts; 
        
        if(empty($num)) {

            $num = 0;

        }

        return $num;
        
    }
    
    /**
     * Fetches a product's revenue for a particular day
     * 
     * @global object $wpdb
     * @param string $day
     * @param int $id
     * @return int
     */
    private function _fetch_tickets_revenue_on_day($day, $id) {
        
        global $wpdb;
        $prefix = $wpdb->base_prefix;

        $sql = "SELECT SUM( order_item_meta__line_total.meta_value) 
                as order_item_amount 
                FROM ".$prefix."posts AS posts 
                INNER JOIN ".$prefix."woocommerce_order_items AS order_items ON posts.ID = order_items.order_id 
                INNER JOIN ".$prefix."woocommerce_order_itemmeta AS order_item_meta__line_total ON (order_items.order_item_id = order_item_meta__line_total.order_item_id)  AND (order_item_meta__line_total.meta_key = '_line_total') 
                INNER JOIN ".$prefix."woocommerce_order_itemmeta AS order_item_meta__product_id_array ON order_items.order_item_id = order_item_meta__product_id_array.order_item_id 
                WHERE posts.post_type IN ('shop_order','shop_order_refund') 
                    AND posts.post_status IN ('wc-completed') 
                    AND ((order_item_meta__product_id_array.meta_key IN ('_product_id','_variation_id')
                    AND DATE(posts.post_date) = '%s'
                    AND order_item_meta__product_id_array.meta_value IN ('%d')))";

        $num = $wpdb->get_var($wpdb->prepare($sql, $day, $id));
        
        if(empty($num)) {
           
           $num = 0;
           
       }
       
       return $num;

    }
    
    /**
     * Fetches check-ins on a particular day
     * 
     * @param string $day
     * @param int $event
     * @return int
     */
    private function _fetch_check_ins_on_day($day, $event) {
        
        global $wpdb;
        $table_name = $wpdb->prefix . 'fooevents_check_in';
        
        $dayBegin = strtotime($day);
        $dayEnd = strtotime("tomorrow", $dayBegin) - 1;

        $wpdb->get_results("
                    SELECT * FROM ".$table_name."
                    WHERE checkin BETWEEN ".$dayBegin." AND ".$dayEnd." 
                        AND eid = ".$event."
                "); 

        return $wpdb->num_rows;
        
    }
    
    /**
     * Fetch checkings for a particular hour
     * 
     * @param string $hour
     * @param int $event
     * @return int
     */
    private function _fetch_check_ins_on_hour($hour, $event) {
        
        global $wpdb;
        $table_name = $wpdb->prefix . 'fooevents_check_in';
        
        $hourBegin = $hour;
        $hourEnd = $hourBegin + 3599;
        
        $wpdb->get_results("
                    SELECT * FROM ".$table_name."
                    WHERE checkin BETWEEN ".$hourBegin." AND ".$hourEnd." 
                        AND eid = ".$event."
                "); 

        return $wpdb->num_rows;
        
    }
    
    /**
     * Fetches all hours for a particular day
     * 
     * @param string $date
     * @return array
     */
    private function _fetch_all_hours_for_date($date) {
        
        $dayBegin = strtotime($date);
        $hoursReturnArray = array(1 => $dayBegin);
        
        for ($x = 2; $x <= 24; $x++) {
            
            $hoursReturnArray[$x] = $hoursReturnArray[$x-1] + 3600;
            
        }
        
        return $hoursReturnArray;
        
    }
    
    /**
     * Array of month names for translation to English
     * 
     * @param string $event_date
     * @return string
     */
    private function convert_month_to_english($event_date) {
        
        $months = array(
            // French
            'janvier' => 'January',
            'février' => 'February',
            'mars' => 'March',
            'avril' => 'April',
            'mai' => 'May',
            'juin' => 'June',
            'juillet' => 'July',
            'aout' => 'August',
            'août' => 'August',
            'septembre' => 'September',
            'octobre' => 'October',

            // German
            'Januar' => 'January',
            'Februar' => 'February',
            'März' => 'March',
            'Mai' => 'May',
            'Juni' => 'June',
            'Juli' => 'July',
            'Oktober' => 'October',
            'Dezember' => 'December',

            // Spanish
            'enero' => 'January',
            'febrero' => 'February',
            'marzo' => 'March',
            'abril' => 'April',
            'mayo' => 'May',
            'junio' => 'June',
            'julio' => 'July',
            'agosto' => 'August',
            'septiembre' => 'September',
            'setiembre' => 'September',
            'octubre' => 'October',
            'noviembre' => 'November',
            'diciembre' => 'December',
            'novembre' => 'November',
            'décembre' => 'December',

            // Dutch
            'januari' => 'January',
            'februari' => 'February',
            'maart' => 'March',
            'april' => 'April',
            'mei' => 'May',
            'juni' => 'June',
            'juli' => 'July',
            'augustus' => 'August',
            'september' => 'September',
            'oktober' => 'October',
            'november' => 'November',
            'december' => 'December',

            // Italian
            'Gennaio' => 'January',
            'Febbraio' => 'February',
            'Marzo' => 'March',
            'Aprile' => 'April',
            'Maggio' => 'May',
            'Giugno' => 'June',
            'Luglio' => 'July',
            'Agosto' => 'August',
            'Settembre' => 'September',
            'Ottobre' => 'October',
            'Novembre' => 'November',
            'Dicembre' => 'December',

            // Polish
            'Styczeń' => 'January',
            'Luty' => 'February',
            'Marzec' => 'March',
            'Kwiecień' => 'April',
            'Maj' => 'May',
            'Czerwiec' => 'June',
            'Lipiec' => 'July',
            'Sierpień' => 'August',
            'Wrzesień' => 'September',
            'Październik' => 'October',
            'Listopad' => 'November',
            'Grudzień' => 'December',

            // Afrikaans
            'Januarie' => 'January',
            'Februarie' => 'February',
            'Maart' => 'March',
            'Mei' => 'May',
            'Junie' => 'June',
            'Julie' => 'July',
            'Augustus' => 'August',
            'Oktober' => 'October',
            'Desember' => 'December',

            // Turkish
            'Ocak' => 'January',
            'Şubat' => 'February',
            'Mart' => 'March',
            'Nisan' => 'April',
            'Mayıs' => 'May',
            'Haziran' => 'June',
            'Temmuz' => 'July',
            'Ağustos' => 'August',
            'Eylül' => 'September',
            'Ekim' => 'October',
            'Kasım' => 'November',
            'Aralık' => 'December',

            // Portuguese
            'janeiro' => 'January',
            'fevereiro' => 'February',
            'março' => 'March',
            'abril' => 'April',
            'maio' => 'May',
            'junho' => 'June',
            'julho' => 'July',
            'agosto' => 'August',
            'setembro' => 'September',
            'outubro' => 'October',
            'novembro' => 'November',
            'dezembro' => 'December',

            // Swedish
            'Januari' => 'January',
            'Februari' => 'February',
            'Mars' => 'March',
            'April' => 'April',
            'Maj' => 'May',
            'Juni' => 'June',
            'Juli' => 'July',
            'Augusti' => 'August',
            'September' => 'September',
            'Oktober' => 'October',
            'November' => 'November',
            'December' => 'December',

            // Czech
            'leden' => 'January',
            'únor' => 'February',
            'březen' => 'March',
            'duben' => 'April',
            'květen' => 'May',
            'červen' => 'June',
            'červenec' => 'July',
            'srpen' => 'August',
            'září' => 'September',
            'říjen' => 'October',
            'listopad' => 'November',
            'prosinec' => 'December',

            // Norwegian
            'januar' => 'January',
            'februar' => 'February',
            'mars' => 'March',
            'april' => 'April',
            'mai' => 'May',
            'juni' => 'June',
            'juli' => 'July',
            'august' => 'August',
            'september' => 'September',
            'oktober' => 'October',
            'november' => 'November',
            'desember' => 'December',

            // Danish
            'januar' => 'January',
            'februar' => 'February',
            'marts' => 'March',
            'april' => 'April',
            'maj' => 'May',
            'juni' => 'June',
            'juli' => 'July',
            'august' => 'August',
            'september' => 'September',
            'oktober' => 'October',
            'november' => 'November',
            'december' => 'December',

            // Finnish
            'tammikuu' => 'January',
            'helmikuu' => 'February',
            'maaliskuu' => 'March',
            'huhtikuu' => 'April',
            'toukokuu' => 'May',
            'kesäkuu' => 'June',
            'heinäkuu' => 'July',
            'elokuu' => 'August',
            'syyskuu' => 'September',
            'lokakuu' => 'October',
            'marraskuu' => 'November',
            'joulukuu' => 'December',

            // Russian
            'Январь' => 'January', 
            'Февраль' => 'February', 
            'Март' => 'March', 
            'Апрель' => 'April', 
            'Май' => 'May', 
            'Июнь' => 'June', 
            'Июль' => 'July', 
            'Август' => 'August', 
            'Сентябрь' => 'September', 
            'Октябрь' => 'October', 
            'Ноябрь' => 'November', 
            'Декабрь' => 'December',

            // Icelandic
            'Janúar' => 'January', 
            'Febrúar' => 'February', 
            'Mars' => 'March', 
            'Apríl' => 'April', 
            'Maí' => 'May', 
            'Júní' => 'June', 
            'Júlí' => 'July', 
            'Ágúst' => 'August', 
            'September' => 'September', 
            'Oktober' => 'October', 
            'Nóvember' => 'November', 
            'Desember' => 'December',

            // Latvian
            'janvāris' => 'January',
            'februāris' => 'February',
            'marts' => 'March',
            'aprīlis' => 'April',
            'maijs' => 'May',
            'jūnijs' => 'June',
            'jūlijs' => 'July',
            'augusts' => 'August',
            'septembris' => 'September',
            'oktobris' => 'October',
            'novembris' => 'November',
            'decembris' => 'December',

            // Lithuanian
            'Sausis' => 'January',
            'Vasaris' => 'February',
            'Kovas' => 'March',
            'Balandis' => 'April',
            'Gegužė' => 'May',
            'Birželis' => 'June',
            'Liepa' => 'July',
            'Rugpjūtis' => 'August',
            'Rugsėjis' => 'September',
            'Spalis' => 'October',
            'Lapkritis' => 'November',
            'Gruodis' => ' December',

            // Greek
            'Ιανουάριος' => 'January',
            'Φεβρουάριος' => 'February',
            'Μάρτιος' => 'March',
            'Απρίλιος' => 'April',
            'Μάιος' => 'May',
            'Ιούνιος' => 'June',
            'Ιούλιος' => 'July',
            'Αύγουστος' => 'August',
            'Σεπτέμβριος' => 'September',
            'Οκτώβριος' => 'October',
            'Νοέμβριος' => 'November',
            'Δεκέμβριος' => 'December',

            // Slovak - Slovakia
            "január" => "January",
            "február" => "February",
            "marec" => "March",
            "apríl" => "April",
            "máj" => "May",
            "jún" => "June",
            "júl" => "July",
            "august" => "August",
            "september" => "September",
            "október" => "October",
            "november" => "November",
            "december" => "December",
        );
        
        $pattern = array_keys($months);
        $replacement = array_values($months);

        foreach ($pattern as $key => $value) {
            
            $pattern[$key] = '/\b'.$value.'\b/i';
            
        }
        
        $replaced_event_date = preg_replace($pattern, $replacement, $event_date);

        $replaced_event_date = str_replace(' de ', ' ', $replaced_event_date);

        return $replaced_event_date;
        
    }
    
}