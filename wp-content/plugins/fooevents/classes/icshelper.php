<?php if(!defined('ABSPATH')) exit;
class FooEvents_ICS_helper {
    
    public $data;
    public $name;
    public $Config;
    private $ZoomAPIHelper;

    function __construct($Config) {
        
        $this->Config = $Config;

        //ZoomAPIHelper
        require_once($this->Config->classPath.'zoomapihelper.php');
        $this->ZoomAPIHelper = new FooEvents_Zoom_API_Helper($this->Config);
        
    }
    
    /**
     * Get bookings ICS data
     * 
     * @param int $event
     * @param int $ticket_id
     * @return array
     */
    function get_ICS_data_bookings($event, $ticket_id) {
        
        if (!function_exists('is_plugin_active') || !function_exists('is_plugin_active_for_network')) {

            require_once(ABSPATH.'/wp-admin/includes/plugin.php' );

        }
        
        $ics_data = array();
        if (is_plugin_active('fooevents_bookings/fooevents-bookings.php') || is_plugin_active_for_network('fooevents_bookings/fooevents-bookings.php')) {
            
            $Fooevents_Bookings = new Fooevents_Bookings();
            $ics_data = $Fooevents_Bookings->get_ics_data($event, $ticket_id);

        }
        
        return $ics_data;
        
    }
    
    /**
     * Get bookings ICS data
     * 
     * @param int $event
     * @param int $ticket_id
     * @return array
     */
    function get_ICS_data($event) {
        
        $processed_ics_data = array();
        
        $processed_ics_data['WooCommerceEventsEvent'] = get_post_meta($event, 'WooCommerceEventsEvent', true);
        $processed_ics_data['WooCommerceEventsDate'] = get_post_meta($event, 'WooCommerceEventsDate', true);
        $processed_ics_data['WooCommerceEventsEndDate'] = get_post_meta($event, 'WooCommerceEventsEndDate', true);
        $processed_ics_data['WooCommerceEventsHour'] = get_post_meta($event, 'WooCommerceEventsHour', true);
        $processed_ics_data['WooCommerceEventsMinutes'] = get_post_meta($event, 'WooCommerceEventsMinutes', true);
        $processed_ics_data['WooCommerceEventsPeriod'] = get_post_meta($event, 'WooCommerceEventsPeriod', true);
        $processed_ics_data['WooCommerceEventsHourEnd'] = get_post_meta($event, 'WooCommerceEventsHourEnd', true);
        $processed_ics_data['WooCommerceEventsMinutesEnd'] = get_post_meta($event, 'WooCommerceEventsMinutesEnd', true);
        $processed_ics_data['WooCommerceEventsLocation'] = get_post_meta($event, 'WooCommerceEventsLocation', true);
        $processed_ics_data['WooCommerceEventsEndPeriod'] = get_post_meta($event, 'WooCommerceEventsEndPeriod', true);
        $processed_ics_data['WooCommerceEventsTimeZone'] = get_post_meta($event, 'WooCommerceEventsTimeZone', true);
        $processed_ics_data['WooCommerceEventsTicketText'] = get_post_meta($event, 'WooCommerceEventsTicketText', true);
        $processed_ics_data['WooCommerceEventsEmail'] = get_post_meta($event, 'WooCommerceEventsEmail', true);
        $processed_ics_data['WooCommerceEventsTicketDisplayZoom'] = get_post_meta($event, 'WooCommerceEventsTicketDisplayZoom', true);
        $processed_ics_data['WooCommerceEventsTicketAddCalendarReminders'] = get_post_meta($event, 'WooCommerceEventsTicketAddCalendarReminders', true);
        
        return $processed_ics_data;
        
    }
    
    /**
     * Generate variables needed to build .ics file
     * 
     * @param int $event
     * @param string $registrant_email
     */
    function generate_ICS($event, $ticket_id, $registrant_email = '') {

        $this->data = '';
        
        /*error_reporting(E_ALL);
        ini_set('display_errors', 1);*/
        
        $WooCommerceEventsType = get_post_meta($event, 'WooCommerceEventsType', true);
        
        $ics_data = array();
        if($WooCommerceEventsType == 'bookings' && !empty($ticket_id)) {
            
            $ics_data = $this->get_ICS_data_bookings($event, $ticket_id, $registrant_email);
            
            
        } else {
            
            $ics_data = $this->get_ICS_data($event);
            
        }

        $post = get_post($event);
        
        $WooCommerceEventsHourEnd = '00';
        $WooCommerceEventsMinutesEnd = '00';
        $WooCommerceEventsEndPeriod = '';
        
        if(!empty($ics_data['WooCommerceEventsHourEnd'])) {
        
            $WooCommerceEventsHourEnd = $ics_data['WooCommerceEventsHourEnd'];
        
        }
        
        if(!empty($ics_data['WooCommerceEventsMinutesEnd'])) {
            
            $WooCommerceEventsMinutesEnd = $ics_data['WooCommerceEventsMinutesEnd'];
            
        }
        
        if(!empty($ics_data['WooCommerceEventsEndPeriod'])) {
            
            $WooCommerceEventsEndPeriod = $ics_data['WooCommerceEventsEndPeriod'];
            
        }
        
        $WooCommerceEventsEvent = '';
        if(!empty($ics_data['WooCommerceEventsEvent'])) {
            
            $WooCommerceEventsEvent = $ics_data['WooCommerceEventsEvent'];
            
        }
        
        $WooCommerceEventsDate = '';
        if(!empty($ics_data['WooCommerceEventsDate'])) {
            
            $WooCommerceEventsDate = $ics_data['WooCommerceEventsDate'];
            
        }
        
        $WooCommerceEventsEndDate = '';
        if(!empty($ics_data['WooCommerceEventsDate'])) {
            
            $WooCommerceEventsEndDate = $ics_data['WooCommerceEventsEndDate'];
        
        }
        
        $WooCommerceEventsHour = '00';
        if(!empty($ics_data['WooCommerceEventsHour'])) {
            
            $WooCommerceEventsHour = $ics_data['WooCommerceEventsHour'];
            
        }
        
        $WooCommerceEventsMinutes = '00';
        if(!empty($ics_data['WooCommerceEventsMinutes'])) {
            
            $WooCommerceEventsMinutes = $ics_data['WooCommerceEventsMinutes'];
            
        }
        
        $WooCommerceEventsPeriod = '';
        if(!empty($ics_data['WooCommerceEventsPeriod'])) {
            
            $WooCommerceEventsPeriod = $ics_data['WooCommerceEventsPeriod'];
        }
        
        $WooCommerceEventsLocation = '';
        if(!empty($ics_data['WooCommerceEventsLocation'])) {
            
            $WooCommerceEventsLocation          = $ics_data['WooCommerceEventsLocation'];
            
        }
        
        $WooCommerceEventsTimeZone = '';
        if(!empty($ics_data['WooCommerceEventsTimeZone'])) {
            
            $WooCommerceEventsTimeZone          = $ics_data['WooCommerceEventsTimeZone'];
            
        }
        
        $WooCommerceEventsTicketText = '';
        if(!empty($ics_data['WooCommerceEventsTicketText'])) {
            
            $WooCommerceEventsTicketText = $ics_data['WooCommerceEventsTicketText'];
        
        }
        
        $WooCommerceEventsEmail = '';
        if(!empty($ics_data['WooCommerceEventsEmail'])) {
            
            $WooCommerceEventsEmail = $ics_data['WooCommerceEventsEmail'];
            
        }
        
        $WooCommerceEventsTicketDisplayZoom = '';
        if(!empty($ics_data['WooCommerceEventsTicketDisplayZoom'])) {
            
            $WooCommerceEventsTicketDisplayZoom = $ics_data['WooCommerceEventsTicketDisplayZoom'];
            
        }
        
        $WooCommerceEventsTicketAddCalendarReminders = '';
        if(!empty($ics_data['WooCommerceEventsTicketAddCalendarReminders'])) {
            
            $WooCommerceEventsTicketAddCalendarReminders = $ics_data['WooCommerceEventsTicketAddCalendarReminders'];
        
        }

        $WooCommerceEventsTimeZone = str_replace('UTC', 'GMT', $WooCommerceEventsTimeZone);

        if ($WooCommerceEventsEndDate == '') {

            $WooCommerceEventsEndDate = $WooCommerceEventsDate;

        }
        
        if ($WooCommerceEventsEndPeriod == '') {

            $WooCommerceEventsEndPeriod = $WooCommerceEventsPeriod;

        }

        $WooCommerceEventsPeriod = $WooCommerceEventsHour > 12 ? '' : strtoupper(str_replace('.', '', $WooCommerceEventsPeriod));
        $WooCommerceEventsEndPeriod = $WooCommerceEventsHourEnd > 12 ? '' : strtoupper(str_replace('.', '', $WooCommerceEventsEndPeriod));
        
        if (!function_exists('is_plugin_active') || !function_exists('is_plugin_active_for_network')) {

            require_once(ABSPATH.'/wp-admin/includes/plugin.php' );

        }
        
        $multiDayType = '';
        
        if (is_plugin_active('fooevents_multi_day/fooevents-multi-day.php') || is_plugin_active_for_network('fooevents_multi_day/fooevents-multi-day.php')) {
            
            $Fooevents_Multiday_Events = new Fooevents_Multiday_Events();
            $multiDayType = $Fooevents_Multiday_Events->get_multi_day_type($event);

        }
        
        $description = get_bloginfo('name');

        if ( !empty($WooCommerceEventsTicketText) ) {

            $description .= '\n\n' . strip_tags(str_replace('<br/>', '\n', $WooCommerceEventsTicketText));

        }

        if ( !empty($WooCommerceEventsTicketDisplayZoom) && $WooCommerceEventsTicketDisplayZoom != 'off' ) {

            $description .= $this->ZoomAPIHelper->get_calendar_text($event, $registrant_email);

        }

        $date_format = get_option( 'date_format' ) . ' H:i';

        if($multiDayType == 'select') {
            
            $multiDayDates = $Fooevents_Multiday_Events->get_multi_day_selected_dates($event);
            
            foreach($multiDayDates as $dayDate) {
                
                $startPeriodFormat = $WooCommerceEventsPeriod != '' ? ' A' : '';
                $startPeriod = $WooCommerceEventsPeriod != '' ? ' ' . $WooCommerceEventsPeriod : '';

                $endPeriodFormat = $WooCommerceEventsEndPeriod != '' ? ' A' : '';
                $endPeriod = $WooCommerceEventsEndPeriod != '' ? ' ' . $WooCommerceEventsEndPeriod : '';

                $dayDate = $this->convert_month_to_english($dayDate);

                $tempStartDate = DateTime::createFromFormat($date_format.$startPeriodFormat, $dayDate." ".$WooCommerceEventsHour.':'.$WooCommerceEventsMinutes.$startPeriod);
                $tempEndDate = DateTime::createFromFormat($date_format.$endPeriodFormat, $dayDate." ".$WooCommerceEventsHourEnd.':'.$WooCommerceEventsMinutesEnd.$endPeriod);

                if ( $tempStartDate ) {

                    $startDate = date("Y-m-d H:i:s", $tempStartDate->getTimestamp());

                } else {

                    $WooCommerceEventsDate = str_replace('/', '-', $dayDate);
                    $WooCommerceEventsDate = str_replace(',', '', $WooCommerceEventsDate);

                    $startDate = date("Y-m-d H:i:s", strtotime($WooCommerceEventsDate." ".$WooCommerceEventsHour.':'.$WooCommerceEventsMinutes.' '.$WooCommerceEventsPeriod));
                    
                }

                if ( $tempEndDate ) {

                    $endDate = date("Y-m-d H:i:s", $tempEndDate->getTimestamp());

                } else {

                    $WooCommerceEventsDate = str_replace('/', '-', $dayDate);
                    $WooCommerceEventsDate = str_replace(',', '', $WooCommerceEventsDate);

                    $endDate = date("Y-m-d H:i:s", strtotime($WooCommerceEventsDate." ".$WooCommerceEventsHourEnd.':'.$WooCommerceEventsMinutesEnd.' '.$WooCommerceEventsEndPeriod));

                }
                
                $this->build_ICS($startDate, $endDate, $WooCommerceEventsTimeZone, $post->post_title, $description, $WooCommerceEventsLocation, $WooCommerceEventsTicketAddCalendarReminders, $WooCommerceEventsEmail); 
                
            }
            
        } else {
            
            $startPeriodFormat = $WooCommerceEventsPeriod != '' ? ' A' : '';
            $startPeriod = $WooCommerceEventsPeriod != '' ? ' ' . $WooCommerceEventsPeriod : '';

            $endPeriodFormat = $WooCommerceEventsEndPeriod != '' ? ' A' : '';
            $endPeriod = $WooCommerceEventsEndPeriod != '' ? ' ' . $WooCommerceEventsEndPeriod : '';
            
            $WooCommerceEventsDate = $this->convert_month_to_english($WooCommerceEventsDate);
            $WooCommerceEventsEndDate = $this->convert_month_to_english($WooCommerceEventsEndDate);

            $tempStartDate = DateTime::createFromFormat($date_format.$startPeriodFormat, $WooCommerceEventsDate." ".$WooCommerceEventsHour.':'.$WooCommerceEventsMinutes.$startPeriod);
            $tempEndDate = DateTime::createFromFormat($date_format.$endPeriodFormat, $WooCommerceEventsEndDate." ".$WooCommerceEventsHourEnd.':'.$WooCommerceEventsMinutesEnd.$endPeriodFormat);
            
            if ( $tempStartDate ) {

                $startDate = date("Y-m-d H:i:s", $tempStartDate->getTimestamp());

            } else {

                $WooCommerceEventsDate = str_replace('/', '-', $WooCommerceEventsDate);
                $WooCommerceEventsDate = str_replace(',', '', $WooCommerceEventsDate);

                $startDate = date("Y-m-d H:i:s", strtotime($WooCommerceEventsDate." ".$WooCommerceEventsHour.':'.$WooCommerceEventsMinutes.' '.$WooCommerceEventsPeriod));

            }
            
            if ( $tempEndDate ) {

                $endDate = date("Y-m-d H:i:s", $tempEndDate->getTimestamp());

            } else {

                $WooCommerceEventsEndDate = str_replace('/', '-', $WooCommerceEventsEndDate);
                $WooCommerceEventsEndDate = str_replace(',', '', $WooCommerceEventsEndDate);

                $endDate = date("Y-m-d H:i:s", strtotime($WooCommerceEventsEndDate." ".$WooCommerceEventsHourEnd.':'.$WooCommerceEventsMinutesEnd.' '.$WooCommerceEventsEndPeriod));

            }

            $this->build_ICS($startDate, $endDate, $WooCommerceEventsTimeZone, $post->post_title, $description, $WooCommerceEventsLocation, $WooCommerceEventsTicketAddCalendarReminders, $WooCommerceEventsEmail); 
            
        }

    }
    
    /**
     * Builds add to calendar .ics file
     * 
     * @param string $start
     * @param string $end
     * @param string $name
     * @param string $description
     * @param string $location
     */
    function build_ICS($start,$end,$timezone,$name,$description,$location = '', $reminders = '', $organizerEmail = '') {

        if ( $reminders == '' ) {

            $reminders = array(
                array('amount' => 1, 'unit' => 'weeks'),
                array('amount' => 1, 'unit' => 'days'),
                array('amount' => 1, 'unit' => 'hours'),
                array('amount' => 15, 'unit' => 'minutes')
            );

        }
        
        $this->name = $name;
        
        if(empty($this->name)) {
            
            $this->name = 'Event';
            
        }
        
        $start = (string) date("Ymd\THis",strtotime($start));
        $end = (string) date("Ymd\THis",strtotime($end));

        $domain = parse_url(get_site_url())['host'];
        
        $random = rand(111111,999999);

        $tzid_start = '';
        $tzid_end = '';
        $vtimezone = '';

        if (trim($timezone) != '') {
            $tzid_start = ";TZID=".$timezone;
            $tzid_end = ";TZID=".$timezone;
            $vtimezone = "VTIMEZONE:".$timezone;
        }

        $organizer = $organizerEmail != '' ? "ORGANIZER:mailto:" . $organizerEmail . "\r\n" : '';

        $this->data .= "BEGIN:VEVENT\r\nDTSTART".$tzid_start.":".$start."\r\nDTEND".$tzid_end.":".$end."\r\n".$vtimezone."\r\n".$organizer."LOCATION:".$location."\r\nTRANSP:OPAQUE\r\nSEQUENCE:0\r\nUID:".$start.$random."-fooevents@".$domain."\r\nDTSTAMP:".$start."\r\nSUMMARY:".$name."\r\nDESCRIPTION:".$description."\r\nPRIORITY:1\r\nCLASS:PUBLIC\r\n";
        
        foreach ( $reminders as $reminder ) {

            $minutes = 0;

            switch ( $reminder['unit'] ) {

                case 'minutes':
                $minutes = (int)$reminder['amount'];
                break;

                case 'hours':
                $minutes = (int)$reminder['amount'] * 60;
                break;

                case 'days':
                $minutes = (int)$reminder['amount'] * 1440;
                break;

                case 'weeks':
                $minutes = (int)$reminder['amount'] * 10080;
                break;

            }

            $this->data .= "BEGIN:VALARM\r\nTRIGGER:-PT" . $minutes . "M\r\nACTION:DISPLAY\r\nDESCRIPTION:Reminder\r\nEND:VALARM\r\n";

        }
        
        $this->data .= "END:VEVENT\r\n";
    
    }
    
    /**
     * Saves ICS file.
     * 
     * @param int $ticketID
     */
    function save($ticketID) {

        $data = "BEGIN:VCALENDAR\r\nVERSION:2.0\r\nMETHOD:PUBLISH\r\n".$this->data."END:VCALENDAR\r\n";

        file_put_contents($this->Config->icsPath.$ticketID.".ics", $data);
        
    }
    
    /**
     * Download the ICS file.
     * 
     */
    function show() {
        
        $data = "BEGIN:VCALENDAR\r\nVERSION:2.0\r\nMETHOD:PUBLISH\r\n".$this->data."END:VCALENDAR\r\n";
        
        header("Content-type:text/calendar");
        header('Content-Disposition: attachment; filename="'.$this->name.'.ics"');
        Header('Content-Length: '.strlen($data));
        Header('Connection: close');
        echo $data;
        
    }

    /**
     * Array of month names for translation to English
     * 
     * @param string $event_date
     * @return string
     */
    private function convert_month_to_english($event_date) {
        
        $months = array(
            // French
            'janvier' => 'January',
            'février' => 'February',
            'mars' => 'March',
            'avril' => 'April',
            'mai' => 'May',
            'juin' => 'June',
            'juillet' => 'July',
            'aout' => 'August',
            'août' => 'August',
            'septembre' => 'September',
            'octobre' => 'October',

            // German
            'Januar' => 'January',
            'Februar' => 'February',
            'März' => 'March',
            'Mai' => 'May',
            'Juni' => 'June',
            'Juli' => 'July',
            'Oktober' => 'October',
            'Dezember' => 'December',

            // Spanish
            'enero' => 'January',
            'febrero' => 'February',
            'marzo' => 'March',
            'abril' => 'April',
            'mayo' => 'May',
            'junio' => 'June',
            'julio' => 'July',
            'agosto' => 'August',
            'septiembre' => 'September',
            'setiembre' => 'September',
            'octubre' => 'October',
            'noviembre' => 'November',
            'diciembre' => 'December',
            'novembre' => 'November',
            'décembre' => 'December',

            // Dutch
            'januari' => 'January',
            'februari' => 'February',
            'maart' => 'March',
            'april' => 'April',
            'mei' => 'May',
            'juni' => 'June',
            'juli' => 'July',
            'augustus' => 'August',
            'september' => 'September',
            'oktober' => 'October',
            'november' => 'November',
            'december' => 'December',

            // Italian
            'Gennaio' => 'January',
            'Febbraio' => 'February',
            'Marzo' => 'March',
            'Aprile' => 'April',
            'Maggio' => 'May',
            'Giugno' => 'June',
            'Luglio' => 'July',
            'Agosto' => 'August',
            'Settembre' => 'September',
            'Ottobre' => 'October',
            'Novembre' => 'November',
            'Dicembre' => 'December',

            // Polish
            'Styczeń' => 'January',
            'Luty' => 'February',
            'Marzec' => 'March',
            'Kwiecień' => 'April',
            'Maj' => 'May',
            'Czerwiec' => 'June',
            'Lipiec' => 'July',
            'Sierpień' => 'August',
            'Wrzesień' => 'September',
            'Październik' => 'October',
            'Listopad' => 'November',
            'Grudzień' => 'December',

            // Afrikaans
            'Januarie' => 'January',
            'Februarie' => 'February',
            'Maart' => 'March',
            'Mei' => 'May',
            'Junie' => 'June',
            'Julie' => 'July',
            'Augustus' => 'August',
            'Oktober' => 'October',
            'Desember' => 'December',

            // Turkish
            'Ocak' => 'January',
            'Şubat' => 'February',
            'Mart' => 'March',
            'Nisan' => 'April',
            'Mayıs' => 'May',
            'Haziran' => 'June',
            'Temmuz' => 'July',
            'Ağustos' => 'August',
            'Eylül' => 'September',
            'Ekim' => 'October',
            'Kasım' => 'November',
            'Aralık' => 'December',

            // Portuguese
            'janeiro' => 'January',
            'fevereiro' => 'February',
            'março' => 'March',
            'abril' => 'April',
            'maio' => 'May',
            'junho' => 'June',
            'julho' => 'July',
            'agosto' => 'August',
            'setembro' => 'September',
            'outubro' => 'October',
            'novembro' => 'November',
            'dezembro' => 'December',

            // Swedish
            'Januari' => 'January',
            'Februari' => 'February',
            'Mars' => 'March',
            'April' => 'April',
            'Maj' => 'May',
            'Juni' => 'June',
            'Juli' => 'July',
            'Augusti' => 'August',
            'September' => 'September',
            'Oktober' => 'October',
            'November' => 'November',
            'December' => 'December',

            // Czech
            'leden' => 'January',
            'únor' => 'February',
            'březen' => 'March',
            'duben' => 'April',
            'květen' => 'May',
            'červen' => 'June',
            'červenec' => 'July',
            'srpen' => 'August',
            'září' => 'September',
            'říjen' => 'October',
            'listopad' => 'November',
            'prosinec' => 'December',

            // Norwegian
            'januar' => 'January',
            'februar' => 'February',
            'mars' => 'March',
            'april' => 'April',
            'mai' => 'May',
            'juni' => 'June',
            'juli' => 'July',
            'august' => 'August',
            'september' => 'September',
            'oktober' => 'October',
            'november' => 'November',
            'desember' => 'December',

            // Danish
            'januar' => 'January',
            'februar' => 'February',
            'marts' => 'March',
            'april' => 'April',
            'maj' => 'May',
            'juni' => 'June',
            'juli' => 'July',
            'august' => 'August',
            'september' => 'September',
            'oktober' => 'October',
            'november' => 'November',
            'december' => 'December',

            // Finnish
            'tammikuu' => 'January',
            'helmikuu' => 'February',
            'maaliskuu' => 'March',
            'huhtikuu' => 'April',
            'toukokuu' => 'May',
            'kesäkuu' => 'June',
            'heinäkuu' => 'July',
            'elokuu' => 'August',
            'syyskuu' => 'September',
            'lokakuu' => 'October',
            'marraskuu' => 'November',
            'joulukuu' => 'December',

            // Russian
            'Январь' => 'January', 
            'Февраль' => 'February', 
            'Март' => 'March', 
            'Апрель' => 'April', 
            'Май' => 'May', 
            'Июнь' => 'June', 
            'Июль' => 'July', 
            'Август' => 'August', 
            'Сентябрь' => 'September', 
            'Октябрь' => 'October', 
            'Ноябрь' => 'November', 
            'Декабрь' => 'December',

            // Icelandic
            'Janúar' => 'January', 
            'Febrúar' => 'February', 
            'Mars' => 'March', 
            'Apríl' => 'April', 
            'Maí' => 'May', 
            'Júní' => 'June', 
            'Júlí' => 'July', 
            'Ágúst' => 'August', 
            'September' => 'September', 
            'Oktober' => 'October', 
            'Nóvember' => 'November', 
            'Desember' => 'December',

            // Latvian
            'janvāris' => 'January',
            'februāris' => 'February',
            'marts' => 'March',
            'aprīlis' => 'April',
            'maijs' => 'May',
            'jūnijs' => 'June',
            'jūlijs' => 'July',
            'augusts' => 'August',
            'septembris' => 'September',
            'oktobris' => 'October',
            'novembris' => 'November',
            'decembris' => 'December',

            // Lithuanian
            'Sausis' => 'January',
            'Vasaris' => 'February',
            'Kovas' => 'March',
            'Balandis' => 'April',
            'Gegužė' => 'May',
            'Birželis' => 'June',
            'Liepa' => 'July',
            'Rugpjūtis' => 'August',
            'Rugsėjis' => 'September',
            'Spalis' => 'October',
            'Lapkritis' => 'November',
            'Gruodis' => ' December',

            // Greek
            'Ιανουάριος' => 'January',
            'Φεβρουάριος' => 'February',
            'Μάρτιος' => 'March',
            'Απρίλιος' => 'April',
            'Μάιος' => 'May',
            'Ιούνιος' => 'June',
            'Ιούλιος' => 'July',
            'Αύγουστος' => 'August',
            'Σεπτέμβριος' => 'September',
            'Οκτώβριος' => 'October',
            'Νοέμβριος' => 'November',
            'Δεκέμβριος' => 'December',

            // Slovak - Slovakia
            "január" => "January",
            "február" => "February",
            "marec" => "March",
            "apríl" => "April",
            "máj" => "May",
            "jún" => "June",
            "júl" => "July",
            "august" => "August",
            "september" => "September",
            "október" => "October",
            "november" => "November",
            "december" => "December",
        );
        
        $pattern = array_keys($months);
        $replacement = array_values($months);

        foreach ($pattern as $key => $value) {
            
            $pattern[$key] = '/\b'.$value.'\b/i';
            
        }
        
        $replaced_event_date = preg_replace($pattern, $replacement, $event_date);

        $replaced_event_date = str_replace(' de ', ' ', $replaced_event_date);

        return $replaced_event_date;
        
    }
    
}