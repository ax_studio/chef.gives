��    :      �      �      �     �  3   �  o    
   s     ~     �     �     �  	   �     �     �     �       %   (     N  �   T  
   �     �     �          &     -     <     U     h     {     �     �  :   �     �  
   �     �     �               2     @     I     [     q     }  =   �  
   �  4   �  =   	  &   K	  .   r	  -   �	  3   �	  ,   
     0
  0   D
     u
  N   y
  H   �
       %   ,  �  R       >   !  p  `     �     �     �     �          '     9     @  (   V  "        �     �  �   �     ^     p     �  /   �     �     �  "   �     
     #     =     E     X  ?   t     �     �     �     �     �     �          *  !   :     \     z     �  L   �  
   �  =   �  M   2  -   �  ,   �  4   �  C     4   T     �  3   �     �  R   �  I   /     y  &   �   Active WooInstant Actived window will open first when wooinstant open Are you tired of multi-step checkout process of WooCommerce? WooInstant is your solution. Install WooInstant and your multi-step checkout process will convert to One Page Checkout. Now, all your customer have to do is "Add to Cart", a popup will appear with the cart view. Your customer can then checkout and order from that single window!. No Page Reload whatsoever! BootPeople Cart Cart Toggler Background Cart Toggler Color Cart Toggler Hover Color Cart icon Checkout Choose Active Window Choose Cart Drawer Direction Choose Icon or Custom Image Choose how the window will looks like Close Control WooInstant z-index from this option. More about <a target="_blank" href="https://css-tricks.com/almanac/properties/z/z-index/">z-index</a> Custom CSS Custom Image Default Icon Default is: Theme Default Design Design Options Disable Ajax Add to Cart Disable Quick View Drawer Window Type General General Options Hide Close Button? If you want to make extra CSS then you can do it from here Left to Right Multi Step No Panel Background Panel Z-index Quick View Background Right to Left Settings Show On Cart Page Show On Checkout Page Single Step Upload image/icon Upload your cart icon. Recommended size of an icon is 30x30px WooInstant WooInstant - WooCommerce Instant / One Page Checkout WooInstant - WooCommerce Instant / One Page Checkout Settings WooInstant Cart Panel Background Color WooInstant Cart Panel Toggler Background Color WooInstant Cart Panel Toggler Text/Icon Color WooInstant Cart Panel Toggler Text/Icon Hover Color WooInstant Quick View Panel Background Color WooInstant Settings Wooinstant requires WooCommerce to be activated  Yes You can disable it if you already have ajax add to cart function in your theme You can disable it if you already have quick view function in your theme https://psdtowpservice.com https://psdtowpservice.com/wooinstant Project-Id-Version: WooInstant - WooCommerce Instant / One Page Checkout
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2020-07-09 09:23+0000
PO-Revision-Date: 2020-07-25 05:34+0000
Last-Translator: 
Language-Team: Spanish (Spain)
Language: es_ES
Plural-Forms: nplurals=2; plural=n != 1;
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco https://localise.biz/
X-Loco-Version: 2.4.0; wp-5.4.2 WooInstant activo La ventana activa se abrirá primero cuando se abra wooinstant Are you tired of multi-step checkout process of WooCommerce? WooInstant is your solution. Install WooInstant and your multi-step checkout process will convert to One Page Checkout. Now, all your customer have to do is "Add to Cart", a popup will appear with the cart view. Your customer can then checkout and order from that single window!. No Page Reload whatsoever!
 BootPeople
 Carro Fondo de carro Toggler Cart Toggler Color Carro Toggler Hover Color Ícono de carrito Revisa Elegir ventana activa Elija la dirección del cajón del carro Elija icono o imagen personalizada Elija cómo se verá la ventana Cerca Controle WooInstant z-index desde esta opción. Más sobre <a target="_blank" href="https://css-tricks.com/almanac/properties/z/z-index/">z-index</a> CSS personalizado Imagen personalizada Ícono predeterminado El valor predeterminado es: Tema predeterminado Diseño Opciones de diseño Desactivar Ajax Añadir al carrito Desactivar vista rápida Tipo de ventana de cajón General Opciones generales ¿Ocultar botón de cierre? Si quieres hacer CSS extra, entonces puedes hacerlo desde aquí De izquierda a derecha Paso múltiple No Fondo del panel Panel Z-index
 Vista rápida de fondo De derecha a izquierda Configuraciones Mostrar en la página del carrito Mostrar en la página de pago Un solo paso Subir imagen / icono Sube el ícono de tu carrito. El tamaño recomendado de un ícono es 30x30px WooInstant 'WooInstant - WooCommerce' Instantáneo / pago de una página Configuración de pago instantáneo / de una página WooInstant - WooCommerce Color de fondo del panel del carro WooInstant WooInstant Cart Panel Toggler Color de fondo WooInstant Cart Panel Toggler Texto / Color de icono WooInstant Cart Panel Toggler Texto / icono Color de desplazamiento Color de fondo del panel Vista rápida de WooInstant Configuración instantánea Wooinstant requiere que WooCommerce esté activado
 si Puede deshabilitarlo si ya tiene la función de agregar al carrito ajax en su tema Puede deshabilitarlo si ya tiene una función de vista rápida en su tema https://psdtowpservice.com
 https://psdtowpservice.com/wooinstant
 