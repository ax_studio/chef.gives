<?php

namespace Elementor;

use \Elementor\Controls_Manager;

class DCE_Controls_Manager extends Controls_Manager {

	public static $dce_token_types = [
		Controls_Manager::TEXT,
		Controls_Manager::TEXTAREA,
		Controls_Manager::WYSIWYG,
		Controls_Manager::NUMBER,
		Controls_Manager::URL,
		Controls_Manager::COLOR,
		Controls_Manager::SLIDER,
		Controls_Manager::MEDIA,
		Controls_Manager::GALLERY,
	];


	// get init data from original control_manager
	public function _clone_controls_manager( $controls_manager ) {

		$controls = $controls_manager->get_controls();
		foreach ( $controls as $key => $value ) {
			$this->controls[ $key ] = $value;
		}

		$control_groups = $controls_manager->get_control_groups();
		foreach ( $control_groups as $key => $value ) {
			$this->control_groups[ $key ] = $value;
		}

		$this->stacks = $controls_manager->get_stacks();
		$this->tabs = $controls_manager::get_tabs();
	}

	public $excluded_extensions = array();

	public function set_excluded_extensions( $extensions ) {
		$this->excluded_extensions = $extensions;
	}

	/**
	 * Add control to stack.
	 *
	 * This method adds a new control to the stack.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @param Controls_Stack $element      Element stack.
	 * @param string         $control_id   Control ID.
	 * @param array          $control_data Control data.
	 * @param array          $options      Optional. Control additional options.
	 *                                     Default is an empty array.
	 *
	 * @return bool True if control added, False otherwise.
	 */
	public function add_control_to_stack( Controls_Stack $element, $control_id, $control_data, $options = [] ) {
		$element_name = $element->get_name();
		if ( $element_name == 'form' ) {
			if ( \DynamicContentForElementor\DCE_Helper::is_elementorpro_active() ) {
				$active_form_extensions = \DynamicContentForElementor\DCE_Extensions::get_active_form_extensions();
				foreach ( $active_form_extensions as $akey => $a_form_ext ) {
					$exc_ext = ! isset($this->excluded_extensions[ $a_form_ext ]);
					$a_form_ext_class = \DynamicContentForElementor\DCE_Extensions::$namespace . $a_form_ext;
					if ( method_exists($a_form_ext_class, '_add_to_form') ) {
						$control_data = $a_form_ext_class::_add_to_form($element, $control_id, $control_data, $options);
					}
				}
			}
		} else if ( $element_name == 'video' ) {
			$extensions = \DynamicContentForElementor\DCE_Extensions::get_active_extensions();
			foreach ( $extensions as $akey => $a_ext ) {
				$exc_ext = ! isset($this->excluded_extensions[ $a_ext ]);
				$a_ext_class = \DynamicContentForElementor\DCE_Extensions::$namespace . $a_ext;
				if ( method_exists($a_ext_class, '_add_to_video') ) {
					$control_data = $a_ext_class::_add_to_video($element, $control_id, $control_data, $options);
				}
			}
		} // avoid EPRO Popup condition issue
		else if ( ! ($element_name === 'popup_triggers' || $element_name ===  'popup_timing' ) ) {
				//add Dynamic Tags to $control_data
				$control_data = self::_add_dynamic_tags($control_data);
		}

		return parent::add_control_to_stack($element, $control_id, $control_data, $options);
	}

	public static function _add_dynamic_tags( $control_data ) {
		if ( ! empty($control_data) ) {
			foreach ( $control_data as $key => $acontrol ) {
				if ( $key != 'dynamic' ) {
					if ( is_array($acontrol) ) {
						$control_data[ $key ] = self::_add_dynamic_tags($acontrol);
					}
				}
			}
		}
		if ( isset($control_data['type']) && ! is_array($control_data['type']) ) {
			$control_obj = \Elementor\Plugin::$instance->controls_manager->get_control( $control_data['type'] );
			if ( $control_obj ) {
				$dynamic_settings = $control_obj->get_settings( 'dynamic' );
				if ( ! empty($dynamic_settings) ) {
					if ( in_array($control_data['type'], self::$dce_token_types) ) {
						if ( ! isset($control_data['dynamic']) ) {
							$control_data['dynamic']['active'] = true;
						} else {
							if ( isset($control_data['dynamic']['active']) ) {
								// natively
								if ( ! $control_data['dynamic']['active'] ) {
									$control_data['dynamic']['active'] = true;
								}
							}
						}
					}
				}
			}
		}
		return $control_data;
	}
}
