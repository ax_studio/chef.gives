function dce_show_step(target, element, direction = 'next', scroll = false) {
    var form = jQuery('.elementor-element-' + element);
    var step = jQuery('.elementor-element-' + element + ' #dce-form-step-' + target);
    jQuery('.elementor-element-' + element + ' .elementor-error').removeClass('elementor-error');
    jQuery('.elementor-element-' + element + ' .dce-form-step').hide().addClass('elementor-hidden');
    jQuery('.elementor-element-' + element + ' .dce-step-active').removeClass('dce-step-active');

    jQuery('.elementor-element-' + element + ' .dce-step-active-progressbar').removeClass('dce-step-active-progressbar');
    jQuery('.elementor-element-' + element + ' #dce-form-step-' + target + '-progressbar').addClass('dce-step-active-progressbar');

    jQuery('.elementor-element-' + element + ' .dce-step-active-summary').removeClass('dce-step-active-summary');
    jQuery('.elementor-element-' + element + ' #dce-form-step-' + target + '-summary').addClass('dce-step-active-summary');
    jQuery('.elementor-element-' + element + ' #dce-form-step-' + target + '-summary').addClass('dce-step-filled-summary');
    if (!jQuery('.elementor-element-' + element + ' .dce-form-step-summary').not('.dce-step-filled-summary').length) {
        jQuery('.elementor-element-' + element + ' .dce-form-summary-wrapper .elementor-button-wrapper').show();
    }

    if (step.hasClass('dce-form-visibility-step')) {
        if ((step.hasClass('dce-form-visibility-step-hide-init') && !step.hasClass('dce-form-visibility-step-hide'))
                || (step.hasClass('dce-form-visibility-step-show-init') && !step.hasClass('dce-form-visibility-step-show'))) {

            dce_epro_applyStep(direction, step);
            console.log('skip step ' + target + ', direction ' + direction);
            var new_target = step.find('.elementor-button-' + direction).attr('data-target');
            return dce_show_step(new_target, element, direction, scroll);
        }
    }

    console.log('apply step ' + target );
    dce_epro_applyStep(direction, step);

    console.log('show step ' + target );
    step.css('display', 'flex').addClass('dce-step-active');
    step.removeClass('elementor-hidden');

    if (scroll) {
        console.log('scroll to top');
        jQuery('html, body').animate({
            scrollTop: jQuery('.elementor-element-' + element).offset().top
        }, 500);
    }

    return true;

}

function dce_epro_applyStep(direction, step) {
    if (direction == 'next') {
        step_direction = step.prev();
    } else {
        step_direction = step.next();
    }
    if (step_direction) {
        console.log('applyStep '+step_direction.attr('id'));
        if (step_direction.find('.e-form__buttons__wrapper__button-' + direction).length) {
            console.log('epro click');
            step_direction.find('.e-form__buttons__wrapper__button-' + direction).trigger('click');
            return true;
        }
    }
    return false;
}

function dce_valid_step(step) {

}
function dce_validate_step(step) {
    var isValid = true;

    step.find('.elementor-field-group [required]').each(function (index, el) {
    if (!el.checkValidity()) {
        el.reportValidity();
        return isValid = false;
      }
    });
    return isValid;


}

function dce_form_field_custom_id(type, id) {
    var tmp = id.split('-');
    switch (type) {
        case 'checkbox':
        //case 'acceptance':
        case 'radio':
            tmp.pop();
        default:
            return tmp.pop();
    }
    return id;
}
