// Adjust canvas coordinate space taking into account pixel ratio,
// to make it look crisp on mobile devices.
// This also causes canvas to be cleared.
function resizeCanvas(canvas) {
	// When zoomed out to less than 100%, for some very strange reason,
	// some browsers report devicePixelRatio as less than 1
	// and only part of the canvas is cleared then.
	var ratio = Math.max(window.devicePixelRatio || 1, 1);
	// This part causes the canvas to be cleared
	canvas.width = canvas.offsetWidth * ratio;
	canvas.height = canvas.offsetHeight * ratio;
	canvas.getContext("2d").scale(ratio, ratio);
}

function initializeSignaturePad(wrapper) {
	let clearButton = wrapper.querySelector("[data-action=clear]");
	let changeColorButton = wrapper.querySelector("[data-action=change-color]");
	let saveButton = wrapper.querySelector("[data-action=save]");
	let hiddenInput = wrapper.querySelector("input");
	let canvas = wrapper.querySelector("canvas");
	// This is necessary to keep the canvas physical dimensions after
	// the canvas is resized.
	canvas.style.width = "400px";
	canvas.style.height = "200px";
	let signaturePad = new SignaturePad(canvas, {
			penColor: canvas.getAttribute('data-pen-color'),
			backgroundColor: canvas.getAttribute('data-background-color')
	});
	clearButton.addEventListener("click", function(event) {
		signaturePad.clear();
	});
	saveButton.addEventListener("click", function(event) {
		hiddenInput.value = signaturePad.toDataURL();
	});
}

var WidgetElements_FormSignature = function($scope, $) {
	let wrappers = document.getElementsByClassName("dce-signature-wrapper");
	for (wrapper of wrappers) {
		initializeSignaturePad(wrapper);
	}
}

// Make sure you run this code under Elementor..
jQuery(window).on('elementor/frontend/init', function() {
	elementorFrontend.hooks.addAction('frontend/element_ready/form.default', WidgetElements_FormSignature);
});
