<?php

namespace DynamicContentForElementor;

use DynamicContentForElementor\Documents\PageSettings_Scrollify;
use DynamicContentForElementor\Documents\PageSettings_InertiaScroll;
use DynamicContentForElementor\DCE_Helper;
use DynamicContentForElementor\Core\Upgrade\Manager as UpgradeManager;
/**
 * Main Plugin Class
 * 
 * @since 0.0.1
 */
class DCE_Plugin {

	public static $namespace = '\\DynamicContentForElementor\\';
	public $dce_classes = [];
	public $dce_overrides = [];
	/** 
	 * @var UpgradeManager
	 */
	public $upgrade;

	/**
	 * Constructor
	 *
	 * @since 0.0.1
	 *
	 * @access public
	 */
	public function __construct() {
		$this->init();
	}

	public function init() {

		// Include extensions
		$this->includes();

		// Add WPML Compatibility
		add_action( 'wpml_loaded', array( $this, 'init_wpml_compatibility' ) );

		add_action( 'admin_menu', array( $this, 'add_dce_menu' ), 200 );

		// fire actions
		add_action( 'elementor/init', array( $this, 'add_dce_to_elementor' ), 0 );

		// Add a custom category for panel widgets
		add_action( 'elementor/init', function() {
			$excluded_extensions = DCE_Extensions::get_option_excluded_extensions();
			\Elementor\Plugin::$instance->controls_manager = $this->dce_overrides['dce_controls_manager'];
			\Elementor\Plugin::$instance->controls_manager->set_excluded_extensions($excluded_extensions);
		}, 0);

		add_filter( 'plugin_action_links_' . DCE_PLUGIN_BASE, '\DynamicContentForElementor\DCE_Plugin::dce_plugin_action_links_config' );

		add_filter( 'pre_handle_404', [ $this, 'dce_allow_posts_pagination' ], 999, 2 );

	}

	/**
	 * Includes
	 *
	 * @since 0.5.0
	 *
	 * @access private
	 */
	public function includes() {
		require_once DCE_PATH . '/core/upgrade/upgrades.php';
		require_once DCE_PATH . '/core/upgrade/manager.php';

		$traits = glob(DCE_PATH . '/class/trait/DCE_*.php');
		// include all classes
		foreach ( $traits as $key => $value ) {
			require_once $value;
		}

		$classes = glob(DCE_PATH . '/class/DCE_*.php');
		// include all classes
		foreach ( $classes as $key => $value ) {
			require_once $value;
		}
		// instance all classes
		foreach ( $classes as $key => $value ) {
			$name = pathinfo($value, PATHINFO_FILENAME);
			$class = self::$namespace . $name;
			$this->dce_classes[ strtolower($name) ] = new $class();
		}

		$overrides = glob(DCE_PATH . '/override/DCE_*.php');
		// include all classes
		foreach ( $overrides as $key => $value ) {
			require_once $value;
		}
		// instance all classes
		foreach ( $overrides as $key => $value ) {
			$name = pathinfo($value, PATHINFO_FILENAME);
			if ( $name == 'DCE_Query' ) {
				$class = self::$namespace . $name;
			} else {
				$class = '\\Elementor\\' . $name;
			}

			switch ( $name ) {
				case 'DCE_Widgets_Manager':
					//$_widget_types = \Elementor\Plugin::$instance->widgets_manager->get_widget_types();
					$this->dce_overrides[ strtolower($name) ] = new $class(array());
					break;
				case 'DCE_Controls_Manager':
					$controls_manager = \Elementor\Plugin::$instance->controls_manager;
					$this->dce_overrides[ strtolower($name) ] = new $class($controls_manager);
					break;
				default:
					$this->dce_overrides[ strtolower($name) ] = new $class();
			}
		}

		$settings = glob(DCE_PATH . '/includes/settings/DCE_*.php');
		// include all classes
		foreach ( $settings as $key => $value ) {
			require_once $value;
		}

	}

	private function init_wpml_compatibility() {
			include_once DCE_PATH . 'includes/compatibility/wpml/class-DCE-WPML.php';
			$this->dce_classes['wpml_compatibility'] = new Compatibility\DCE_WPML();
	}

	/**
	 * Add Actions
	 *
	 * @since 0.0.1
	 *
	 * @access private
	 */
	public function add_dce_to_elementor() {

		// Global Settings Panel
		//$this->register_settings_managers();
		\DynamicContentForElementor\Includes\Settings\DCE_Settings_Manager::init();

		$this->upgrade = UpgradeManager::instance();
		// Controls
		add_action('elementor/controls/controls_registered', [ $this->dce_classes['dce_controls'], 'on_controls_registered' ]);

		// Extensions
		$this->dce_classes['dce_extensions']->on_extensions_registered();
		//add_action('elementor/init', [ $this->dce_extensions, 'on_extensions_registered'] );

		// Documents
		$this->dce_classes['dce_documents']->on_documents_registered();

		// Widgets
		add_action('elementor/widgets/widgets_registered', [ $this->dce_classes['dce_widgets'], 'on_widgets_registered' ]);

	}

	public function add_dce_menu() {
		// TemplateSystem sotto Template
		if ( defined('\Elementor\TemplateLibrary\Source_Local::ADMIN_MENU_SLUG') && current_user_can('manage_options') ) {
			add_submenu_page(
				\Elementor\TemplateLibrary\Source_Local::ADMIN_MENU_SLUG,
				__('Dynamic Template System', 'dynamic-content-for-elementor'),
				__('Template System', 'dynamic-content-for-elementor'),
				'publish_posts',
				'dce_templatesystem',
				array(
					$this->dce_classes['dce_settings'],
					'dce_setting_templatesystem',
				)
			);
		}

		// Dynamic Content sotto Elementor
		add_submenu_page(
			\Elementor\Settings::PAGE_ID,
			__('Dynamic Content Settings', 'dynamic-content-for-elementor'),
			__('Dynamic Content', 'dynamic-content-for-elementor'),
			'manage_options',
			'dce_opt',
			[
				$this->dce_classes['dce_settings'],
				'dce_setting_page',
			]
		);

		// La pagina Informazioni che appare alla prima attivazione del plugin.
		add_submenu_page(
				'admin.php', __('Dynamic Content for Elementor', 'dynamic-content-for-elementor'), __('Dynamic Content for Elementor', 'dynamic-content-for-elementor'), 'manage_options', 'dce_info', array(
					$this->dce_classes['dce_info'],
					'dce_information_plugin',
				)
		);
	}

	public static function dce_plugin_action_links_config( $links ) {
		$links['config'] = '<a title="Configuration" href="' . admin_url() . 'admin.php?page=dce_opt">' . __('Configuration', 'dynamic-content-for-elementor') . '</a>';
		return $links;
	}

	public function dce_allow_posts_pagination( $preempt, $wp_query ) {

		if (
			$preempt
			|| empty( $wp_query->query_vars['page'] )
			|| empty( $wp_query->post )
			|| ! is_singular()
		) {
			return $preempt;
		}

		$allow_pagination = false;
		$document = '';
		$current_post_id = $wp_query->post->ID;
		// $current_page = $wp_query->query_vars['page'];
		$dce_posts_widgets = [ 'dyncontel-acfposts', 'dce-dynamicposts-v2', 'dyncontel-dynamicusers' ];

		// Check if current post/page is built with Elementor and check for DCE posts pagination
		if ( \Elementor\Plugin::$instance->db->is_built_with_elementor( $current_post_id ) && ! $allow_pagination ) {
			$allow_pagination = $this->dce_check_posts_pagination( $current_post_id, $dce_posts_widgets );
		}

		// Check if single DCE template is active and check for DCE posts pagination in template
		if ( ! get_option('dce_template_disable') && ! $allow_pagination ) {
			$options = get_option('dyncontel_options');
			$post_type = get_post_type($current_post_id);

			if ( $options[ 'dyncontel_field_single' . $post_type ] ) {
				$allow_pagination = $this->dce_check_posts_pagination( $options[ 'dyncontel_field_single' . $post_type ], $dce_posts_widgets );
			}
		}

		// Check if single Elementor Pro template is active and check for DCE posts pagination in template
		if ( DCE_Helper::is_elementorpro_active() && ! $allow_pagination ) {
			$locations = \ElementorPro\Modules\ThemeBuilder\Module::instance()->get_locations_manager()->get_locations();

			if ( isset($locations['single']) ) {
				$location_docs = \ElementorPro\Modules\ThemeBuilder\Module::instance()->get_conditions_manager()->get_documents_for_location('single');
				if ( ! empty($location_docs) ) {
					foreach ( $location_docs as $location_doc_id => $settings ) {
						if ( ( $wp_query->post->ID !== $location_doc_id ) && ! $allow_pagination ) {
							$allow_pagination = $this->dce_check_posts_pagination( $location_doc_id, $dce_posts_widgets );
							break;
						}
					}
				}
			}
		}

		if ( $allow_pagination ) {
			return $allow_pagination;
		} else {
			return $preempt;
		}

	}

	protected function dce_check_posts_pagination( $post_id, $dce_posts_widgets, $current_page = null ) {
		$pagination = false;

		if ( $post_id ) {
			$document = \Elementor\Plugin::$instance->documents->get( $post_id );
			$document_elements = $document->get_elements_data();

			// Check if DCE posts widgets are present and if pagination or infinite scroll is active
			\Elementor\Plugin::$instance->db->iterate_data( $document_elements, function( $element ) use ( &$pagination, $dce_posts_widgets, $current_page ) {
				if ( isset( $element['widgetType'] ) && in_array( $element['widgetType'], $dce_posts_widgets, true )
				) {
					if ( isset($element['settings']['pagination_enable']) ) {
						if ( $element['settings']['pagination_enable'] ) {
							$pagination = true;
						}
					}
					if ( isset($element['settings']['infiniteScroll_enable']) ) {
						if ( $element['settings']['infiniteScroll_enable'] ) {
							$pagination = true;
						}
					}
				}
			});
		}

		return $pagination;
	}

}
