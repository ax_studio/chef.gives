<?php

namespace DynamicContentForElementor\Extensions;

use Elementor\Core\Kits\Documents\Tabs\Global_Colors;
use Elementor\Controls_Manager;
use Elementor\Controls_Stack;
use DynamicContentForElementor\DCE_Helper;
use DynamicContentForElementor\DCE_Tokens;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Typography;
use Elementor\Group_Control_Text_Shadow;
use Elementor\Group_Control_Background;
use Elementor\Group_Control_Box_Shadow;
use ElementorPro\Modules\Forms\Fields;
use Elementor\Widget_Base;
use ElementorPro\Modules\Forms\Classes;
use ElementorPro\Modules\Forms\Widgets\Form;
use ElementorPro\Plugin;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

function _dce_extension_form_signature( $field ) {
	switch ( $field ) {
		case 'enabled':
			return true;
		case 'docs':
			return 'https://www.dynamic.ooo/widgets/signature-elementor-pro-form';
		case 'description':
			return __( 'Add a signature field to Elementor Pro Form and it will go directly to complete your PDF.', 'dynamic-content-for-elementor' );
	}
}

if ( ! DCE_Helper::is_plugin_active( 'elementor-pro' ) ) {

	class DCE_Extension_Form_Signature extends DCE_Extension_Prototype {

		public $name = 'Signature for Elementor Pro Form';
		private $is_common = false;
		public static $depended_plugins = [ 'elementor-pro' ];

		public static function is_enabled() {
			return _dce_extension_form_signature( 'enabled' );
		}

		public static function get_description() {
			return _dce_extension_form_signature( 'description' );
		}

		public function get_docs() {
			return _dce_extension_form_signature( 'docs' );
		}

	}

} else {
	class DCE_Extension_Form_Signature extends \ElementorPro\Modules\Forms\Fields\Field_Base {
		public $name = 'Signature for Elementor Pro Form';
		public static $depended_plugins = [ 'elementor-pro' ];
		public static $docs = 'https://www.dynamic.ooo/';
		private $is_common = false;
		public $has_action = false;
		public $depended_scripts = [ 'dce-signature-lib', 'dce-signature' ];

		public function __construct( ) {
			add_action('elementor/element/form/section_form_style/after_section_end', [ $this, 'add_control_section_to_form' ], 10, 2);
			parent::__construct();
		}

		public function add_control_section_to_form( $element, $args ) {

			$element->start_controls_section(
					'dce_section_signature_buttons_style',
					[
						'label' => __('Signature', 'dynamic-content-for-elementor'),
						'tab' => \Elementor\Controls_Manager::TAB_STYLE,
					]
			);

			$element->add_control(
				'signature_canvas_border_radius',
				[
					'label' => __( 'Pad Border Radius', 'dynamic-content-for-elementor' ),
					'type' => Controls_Manager::DIMENSIONS,
					'default' => [
						'top' => '3',
						'right' => '3',
						'bottom' => '3',
						'left' => '3',
						'size_units' => 'px',
					],
					'size_units' => [ 'px', '%' ],
					'selectors' => [
						'{{WRAPPER}} .dce-signature-canvas' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
					'separator' => 'before'
				]
			);

			$element->add_control(
				'signature_canvas_border_width',
				[
					'label' => __( 'Pad Border Width', 'dynamic-content-for-elementor' ),
					'type' => Controls_Manager::DIMENSIONS,
					'default' => [
						'top' => '1',
						'right' => '1',
						'bottom' => '1',
						'left' => '1',
						'size_units' => 'px',
					],
					'size_units' => [ 'px' ],
					'selectors' => [
						'{{WRAPPER}} .dce-signature-canvas' => 'border-width: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
				]
			);

			$element->add_control(
				'signature_canvas_background_color',
				[
					'label' => __( 'Pad Background Color', 'dynamic-content-for-elementor' ),
					'type' => Controls_Manager::COLOR,
					'default' => '#ffffff',
					'selectors' => [
						'{{WRAPPER}} .dce-signature-canvas' => 'background-color: {{VALUE}};',
					],
				]
			);

			$element->add_control(
				'signature_canvas_pen_color',
				[
					'label' => __( 'Pen Color', 'dynamic-content-for-elementor' ),
					'type' => Controls_Manager::COLOR,
					'default' => '#000000',
				]
			);

			$element->add_control(
				'signature_button_clear_background_color',
				[
					'label' => __( 'Clear Button Background Color', 'elementor' ),
					'type' => Controls_Manager::COLOR,
					'separator' => 'before',
					'selectors' => [
						'{{WRAPPER}} .dce-signature-button-clear' => 'background-color: {{VALUE}};',
					],
				]
			);
			$element->add_control(
				'signature_button_clear_text_color',
				[
					'label' => __( 'Clear Text Color', 'elementor' ),
					'type' => Controls_Manager::COLOR,
					'default' => '',
					'selectors' => [
						'{{WRAPPER}} .dce-signature-button-clear' => 'color: {{VALUE}};',
					],
				]
			);
			$element->add_group_control(
				Group_Control_Border::get_type(), [
					'name' => 'signature_clear_button_border',
					'label' => __('Clear Button Border', 'dynamic-content-for-elementor'),
					'selector' => '{{WRAPPER}} .dce-signature-button-clear',
				]
			);

			$element->add_control(
				'signature_button_save_background_color',
				[
					'label' => __( 'Save Button Background Color', 'elementor' ),
					'type' => Controls_Manager::COLOR,
					'separator' => 'before',
					'selectors' => [
						'{{WRAPPER}} .dce-signature-button-save' => 'background-color: {{VALUE}};',
					],
				]
			);
			$element->add_control(
				'signature_button_save_text_color',
				[
					'label' => __( 'Save Text Color', 'elementor' ),
					'type' => Controls_Manager::COLOR,
					'default' => '',
					'selectors' => [
						'{{WRAPPER}} .dce-signature-button-save' => 'color: {{VALUE}};',
					],
				]
			);
			$element->add_group_control(
				Group_Control_Border::get_type(), [
					'name' => 'signature_save_button_border',
					'label' => __('Save Button Border', 'dynamic-content-for-elementor'),
					'selector' => '{{WRAPPER}} .dce-signature-button-save',
				]
			);

			$element->add_group_control(
				Group_Control_Typography::get_type(),
				[
					'label' => __('Buttons Typography', 'dynamic-content-for-elementor'),
					'name' => 'signature_button_typography',
					'selector' => '{{WRAPPER}} .dce-signature-button',
					'separator' => 'before',
				]
			);

			$element->add_control(
				'signature_button_margin',
				[
					'label' => __( 'Buttons Margins', 'elementor-pro' ),
					'type' => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', '%', 'em' ],
					'selectors' => [
						'{{WRAPPER}} .dce-signature-button' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					]
				]
			);

			$element->add_group_control(
				Group_Control_Text_Shadow::get_type(),
				[
					'label' => __( 'Buttons Text Shadow', 'elementor-pro' ),
					'name' => 'signature_buttons_text_shadow',
					'selector' => '{{WRAPPER}} .dce-signature-button',
				]
			);

			$element->end_controls_section();
		}

		public static function is_enabled() {
			return _dce_extension_form_signature( 'enabled' );
		}

		public static function get_description() {
			return _dce_extension_form_signature( 'description' );
		}

		public function get_docs() {
			return _dce_extension_form_signature( 'docs' );
		}

		public static function get_plugin_depends() {
			return self::$depended_plugins;
		}

		public static function get_satisfy_dependencies() {
			return true;
		}

		public function get_script_depends() {
			return $this->depended_scripts;
		}

		public function get_name() {
			return 'Signature';
		}

		public function get_label() {
			return __( 'Form Signature', 'dynamic-content-for-elementor' );
		}

		public function get_type() {
			return 'dce_form_signature';
		}

		public function get_style_depends() {
			return $this->depended_styles;
		}

		public static function render_button( $action, $text ) { ?>
			<button type="button" class="dce-signature-button dce-signature-button-<?php echo $action ?>"
			data-action="<?php echo $action ?>">
			<span><span class="dce-signature-button-text"><?php echo $text ?></span></span>
			</button>
		<?php }

		/**
		 * @param      $item
		 * @param      $item_index
		 * @param Form $form
		 */
		public function render( $item, $item_index, $form ) {
			$settings = $form->get_settings_for_display();
			$form->add_render_attribute( 'input' . $item_index, 'type', 'hidden', true );
			$form->add_render_attribute( 'signature-canvas' . $item_index,
				'class', 'dce-signature-canvas' );
			// $form->add_render_attribute( 'signature-canvas' . $item_index,
			// 	'class', 'elementor-field' );
			$form->add_render_attribute( 'signature-canvas' . $item_index,
				'data-pen-color', $settings['signature_canvas_pen_color'] );
			$form->add_render_attribute( 'signature-canvas' . $item_index,
				'data-background-color', $settings['signature_canvas_background_color'] );
			$form->add_render_attribute( 'signature-canvas' . $item_index,
				'width', '400' );
			$form->add_render_attribute( 'signature-canvas' . $item_index,
				'height', '200' );
			$form->add_render_attribute( 'signature-canvas' . $item_index,
				'style', 'border-style: solid' );
			$form->add_render_attribute( 'signature-wrapper' . $item_index,
				'class', 'dce-signature-wrapper' );
			$form->add_render_attribute( 'signature-wrapper' . $item_index,
				'id', 'dce-signature-wrapper-' . $form->get_attribute_name( $item ) );
			echo '<div ' . $form->get_render_attribute_string( 'signature-wrapper' . $item_index ) . ' style="width: 100%">';
			echo '<p>';
			echo '<input ' . $form->get_render_attribute_string( 'input' . $item_index ) . '>';
			echo '<canvas ' . $form->get_render_attribute_string( 'signature-canvas' . $item_index ) . '></canvas>';
			echo '</p>';
			echo '<p class="dce-signature-actions">';
			self::render_button( 'clear', $item['signature_clear_label'] );
			self::render_button( 'save', $item['signature_save_label'] );
			echo '</p>';
			echo '</div>';
		}

		public function update_controls( $widget ) {
			$elementor = Plugin::elementor();

			$control_data = $elementor->controls_manager->get_control_from_stack( $widget->get_unique_name(), 'form_fields' );

			if ( is_wp_error( $control_data ) ) {
				return;
			}

			$field_controls = [
				'signature_clear_label' => [
					'name' => 'signature_clear_label',
					'label' => __( 'Clear Text', 'elementor-pro' ),
					'type' => Controls_Manager::TEXT,
					'default' => __('Clear', 'dynamic-content-for-elementor'),
					'condition' => [
						'field_type' => $this->get_type(),
					],
					'tab' => 'content',
					'inner_tab' => 'form_fields_content_tab',
					'tabs_wrapper' => 'form_fields_tabs',
				],
				'signature_save_label' => [
					'name' => 'signature_save_label',
					'label' => __( 'Save Text', 'elementor-pro' ),
					'type' => Controls_Manager::TEXT,
					'default' => __('Save', 'dynamic-content-for-elementor'),
					'condition' => [
						'field_type' => $this->get_type(),
					],
					'tab' => 'content',
					'inner_tab' => 'form_fields_content_tab',
					'tabs_wrapper' => 'form_fields_tabs',
				],
			];

			$control_data['fields'] = $this->inject_field_controls( $control_data['fields'], $field_controls );
			$widget->update_control( 'form_fields', $control_data );
		}

		/**
		 * validate uploaded file field
		 *
		 * @param array                $field
		 * @param Classes\Form_Record  $record
		 * @param Classes\Ajax_Handler $ajax_handler
		 */
		public function validation( $field, Classes\Form_Record $record, Classes\Ajax_Handler $ajax_handler ) {
			$id = $field['id'];
			if ( $field['required'] && $field['raw_value'] == '' ) {
				$ajax_handler->add_error( $id, __( 'This signature field is required. Please sign it and click the button Save before submitting the form.', 'dynamic-content-for-elementor' ) );
			}
		}

		public function sanitize_field($value, $field) {
			if ( preg_match( '&^data:image/png;base64,[\w/+]&', $value ) ) {
				return $value;
			}
			return '';
		}

		/**
		 * process file and move it to uploads directory
		 *
		 * @param array                $field
		 * @param Classes\Form_Record  $record
		 * @param Classes\Ajax_Handler $ajax_handler
		 */
		public function process_field( $field, Classes\Form_Record $record, Classes\Ajax_Handler $ajax_handler ) {
			// do nothing for now.
		}
	}

}
