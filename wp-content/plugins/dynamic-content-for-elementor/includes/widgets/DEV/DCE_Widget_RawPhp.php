<?php

namespace DynamicContentForElementor\Widgets;

use Elementor\Controls_Manager;

if ( ! defined('ABSPATH') ) {
	exit;
} // Exit if accessed directly

/**
 * Elementor PhpRaw
 *
 * Elementor widget for Dynamic Content for Elementor
 *
 */

class DCE_Widget_RawPhp extends DCE_Widget_Prototype {

	public function get_name() {
		return 'dce-rawphp';
	}

	public static function is_enabled() {
		return true;
	}

	public function show_in_panel() {

		if ( ! current_user_can('manage_options') ) {
			return false;
		}
		return true;
	}

	public function get_title() {
		return __('PHP Raw', 'dynamic-content-for-elementor');
	}
	public function get_description() {
		return __('Add PHP code directly in Elementor', 'dynamic-content-for-elementor');
	}
	public function get_docs() {
		return 'https://www.dynamic.ooo/widget/php-raw/';
	}
	public function get_icon() {
		return 'icon-dyn-phprow';
	}

	protected function _register_controls() {

		if ( current_user_can('manage_options') || ! is_admin() ) {
			$this->register_controls_content();
		} elseif ( ! current_user_can('manage_options') && is_admin() ) {
			$this->register_controls_non_admin_notice();
		}
	}

	protected function register_controls_content() {
		$this->start_controls_section(
			'section_rawphp',
			[
				'label' => __('PHP Raw', 'dynamic-content-for-elementor'),
			]
		);

		$this->add_control(
			'custom_php',
			[
				'label'   => __('Custom PHP', 'dynamic-content-for-elementor'),
				'type'    => Controls_Manager::CODE,
				'language' => 'php',
				'description' => '<div style="display: none;" class="alert notice warning dce-notice-phpraw dce-notice dce-error dce-notice-error"><strong>ALERT</strong>: PHP codes may have errors. Please check it before saving or your page will be corrupted by a fatal error!</div>',
			]
		);

		$this->end_controls_section();
	}

	protected function render() {
		$settings = $this->get_settings_for_display(null, true);

		if (
			current_user_can('manage_options')
			&& \Elementor\Plugin::$instance->editor->is_edit_mode()
			&& empty($settings['custom_php'])
		) {
			echo __('Add your Custom PHP Code to begin.', 'dynamic-content-for-elementor');
		} elseif (
			! current_user_can('manage_options')
			&& \Elementor\Plugin::$instance->editor->is_edit_mode()
		) {
			$this->render_non_admin_notice();
		} elseif (
			(
				current_user_can('manage_options')
				&& \Elementor\Plugin::$instance->editor->is_edit_mode()
				&& ! empty($settings['custom_php'])
			)
			||
			( ! is_admin()
				&& ! empty($settings['custom_php'])
			)
		) {
			$evalError = false;
			try {
				$this->execPhp($settings['custom_php']);
			} catch ( ParseError $e ) {
				$evalError = true;
			} catch ( Exception $e ) {
				$evalError = true;
			} catch ( Error $e ) {
				$evalError = true;
			} catch ( Error $e ) {
				$evalError = true;
			}

			if ( $evalError && \Elementor\Plugin::$instance->editor->is_edit_mode() ) {
				echo '<strong>';
				echo __('Please check your PHP code', 'dynamic-content-for-elementor');
				echo '</strong><br />';
				echo 'ERROR: ',  $e->getMessage(), "\n";
			}
		}
	}

	protected function execPhp( $code ) {
		@eval($code);
	}
}
