<?php
/**
 * Post single content
 *
 * @package OceanWP WordPress theme
 */

// Exit if accessed directly
if ( ! defined('ABSPATH') ) {
	exit;
} ?>

<div class="entry-content clr" itemprop="text">
	<?php

	$cptype = $post->post_type;

	$post_term_list = wp_get_post_terms(get_the_id(), 'category_realizzazioni'); //$args = array('orderby' => 'name', 'order' => 'ASC', 'fields' => 'all');


	if ( count($post_term_list) == 1 && has_term('atelier', 'category_realizzazioni') ) {
		echo do_shortcode('[elementor-template id="452"]');
	} else {
		the_content();
	}

	wp_link_pages(array(
		'before'      => '<div class="page-links">' . __('Pages:', 'oceanwp'),
		'after'       => '</div>',
		'link_before' => '<span class="page-number">',
		'link_after'  => '</span>',
	)); ?>
</div><!-- .entry -->
