<?php
/**
 * Displays post entry content
 *
 * @package OceanWP WordPress theme
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
} ?>

<div class="realizzazioni-entry-summary clr">
	<?php
	$cptype = $post->post_type;
	the_content();
	?>

</div><!-- .realizzazioni-entry-summary -->
