<?php
/**
 * Plugin Name: FooEvents Multi-Day
 * Description: Adds Multi-Day Event Functionality to FooEvents
 * Version: 1.4.0
 * Author: FooEvents
 * Plugin URI: https://www.fooevents.com/
 * Author URI: https://www.fooevents.com/
 * Developer: FooEvents
 * Developer URI: https://www.fooevents.com/
 * Text Domain: fooevents-multiday-events
 *
 * Copyright: © 2009-2020 FooEvents.
 * License: GNU General Public License v3.0
 * License URI: http://www.gnu.org/licenses/gpl-3.0.html
 */

require(WP_PLUGIN_DIR.'/fooevents_multi_day/config.php');

class Fooevents_Multiday_Events {
    
    private $Config;
    private $UpdateHelper;
    
    public function __construct() {

        $this->plugin_init();
        
        add_action( 'plugins_loaded', array( $this, 'load_text_domain' ) );
        add_action( 'admin_init', array( $this, 'register_scripts' ) );
        
    }
    
    /**
     * Initializes plugin
     * 
     */
    public function plugin_init() {
        
        //Main config
        $this->Config = new Fooevents_Multiday_Events_Config();
        
        //UpdateHelper
        require_once($this->Config->classPath.'updatehelper.php');
        $this->UpdateHelper = new Fooevents_Multiday_Events_Update_Helper($this->Config);
        
    }
    
    /**
     * Register plugin scripts.
     * 
     */
    public function register_scripts() {
        
        if((isset($_GET['action']) && $_GET['action'] == 'edit') || (isset($_GET['post_type']) && $_GET['post_type'] == 'product')) {
            
            global $wp_locale;

            wp_enqueue_script('jquery-ui-datepicker');
            wp_enqueue_script('wp-color-picker');
            wp_enqueue_script('events-multi-day-script', $this->Config->scriptsPath . 'events-multi-day-admin.js', array( 'jquery-ui-datepicker', 'wp-color-picker' ), '1.0.2', true );
            
            $dayTerm = '';
            if(!empty($_GET['post'])) {
                
                $post_id = sanitize_text_field($_GET['post']);
                $dayTerm = get_post_meta($post_id, 'WooCommerceEventsDayOverride', true);
                
            }
            
            if(empty($dayTerm)) {

                $dayTerm = get_option('WooCommerceEventsDayOverride', true);

            }

            if(empty($dayTerm) || $dayTerm == 1) {

                $dayTerm = __('Day', 'fooevents-multiday-events');

            }
            
            
            $localArgs = array(
                'closeText'         => __( 'Done', 'woocommerce-events' ),
                'currentText'       => __( 'Today', 'woocommerce-events' ),
                'monthNames'        => $this->_strip_array_indices( $wp_locale->month ),
                'monthNamesShort'   => $this->_strip_array_indices( $wp_locale->month_abbrev ),
                'monthStatus'       => __( 'Show a different month', 'woocommerce-events' ),
                'dayNames'          => $this->_strip_array_indices( $wp_locale->weekday ),
                'dayNamesShort'     => $this->_strip_array_indices( $wp_locale->weekday_abbrev ),
                'dayNamesMin'       => $this->_strip_array_indices( $wp_locale->weekday_initial ),
                'dateFormat'        => $this->_date_format_php_to_js( get_option( 'date_format' ) ),
                'firstDay'          => get_option( 'start_of_week' ),
                'isRTL'             => $wp_locale->is_rtl(),
                'dayTerm'           => esc_attr($dayTerm)
            );
            
            wp_localize_script('events-multi-day-script', 'localObj', $localArgs);
            
        }
        
    }
    
    /**
     * Loads text-domain for localization
     * 
     */
    public function load_text_domain() {
        
        $path = dirname(plugin_basename(__FILE__)).'/languages/';
        $loaded = load_plugin_textdomain( 'fooevents-multiday-events', false, $path);
        
    }
    
    /**
     * Displays event end date option
     * 
     * @param object $post
     * @return string
     */
    public function generate_end_date_option($post) {
        
        ob_start();
        
        $WooCommerceEventsEndDate = get_post_meta($post->ID, 'WooCommerceEventsEndDate', true);

        require($this->Config->templatePath.'end-date-option.php');

        $end_date_option = ob_get_clean();

        return $end_date_option;
        
    }
    
    /**
     * Displays event number of days option
     * 
     * @param object $post
     * @return string
     */
    public function generate_num_days_option($post) {
        
        ob_start();
        
        $WooCommerceEventsNumDays = get_post_meta($post->ID, 'WooCommerceEventsNumDays', true);
        
        require($this->Config->templatePath.'num-days-option.php');

        $num_days_option = ob_get_clean();

        return $num_days_option;
        
    }
    
    /**
     * Displays event multiday type option
     * 
     * @param object $post
     * @return string
     */
    public function generate_multiday_select_date_container($post) {
        
        if (!function_exists('is_plugin_active_for_network')) {
            
            require_once( ABSPATH . '/wp-admin/includes/plugin.php' );
            
        }
        
        $bookings_enabled = false;
        
        if ($this->is_plugin_active('fooevents_bookings/fooevents-bookings.php') || is_plugin_active_for_network('fooevents_bookings/fooevents-bookings.php')) {
            
            $bookings_enabled = true;
            
        }
        
        ob_start();

        $WooCommerceEventsType = get_post_meta($post->ID, 'WooCommerceEventsType', true);
        $WooCommerceEventsSelectDate = get_post_meta($post->ID, 'WooCommerceEventsSelectDate', true);

        $dayTerm = get_post_meta($post->ID, 'WooCommerceEventsDayOverride', true);

        if(empty($dayTerm)) {

            $dayTerm = get_option('WooCommerceEventsDayOverride', true);

        }

        if(empty($dayTerm) || $dayTerm == 1) {

            $dayTerm = __('Day', 'fooevents-multiday-events');

        }
        
        require($this->Config->templatePath.'multiday-select-date-container.php');
        
        $multiday_type_option = ob_get_clean();
        
        return $multiday_type_option;
        
    }
    
    /**
     * Displays event multiday term options
     * 
     * @param object $post
     * @return string
     */
    public function generate_multiday_term_option($post) {
        
        ob_start();
        
        $WooCommerceEventsDayOverride = get_post_meta($post->ID, 'WooCommerceEventsDayOverride', true);
        $WooCommerceEventsDayOverridePlural = get_post_meta($post->ID, 'WooCommerceEventsDayOverridePlural', true);
        
        require($this->Config->templatePath.'multiday-term-option.php');
        
        $multiday_term_option = ob_get_clean();
        
        return $multiday_term_option;
        
    }
    
    /**
     * Get event date range for display
     * 
     * @param int $ID
     * @return string
     */
    public function get_multi_day_date_range($ID) {
        
        $WooCommerceEventsType = get_post_meta($ID, 'WooCommerceEventsType', true);
        $event_date = '';
        
        if($WooCommerceEventsType == 'sequential') {
            
             $event_date = get_post_meta($ID, 'WooCommerceEventsDate', true); 
             $end_event_date = get_post_meta($ID, 'WooCommerceEventsEndDate', true); 
             
             if(!empty($end_event_date)) {
                 
                 $event_date = $event_date.' - '.$end_event_date;
                 
             } 
             
        } elseif($WooCommerceEventsType == 'select') {
            
            $WooCommerceEventsSelectDate = get_post_meta($ID, 'WooCommerceEventsSelectDate', true);
            
            $event_date = $WooCommerceEventsSelectDate[0];
            $end_event_date = $WooCommerceEventsSelectDate[count($WooCommerceEventsSelectDate) - 1];
            
            if(!empty($end_event_date)) {

             $event_date = $event_date.' - '.$end_event_date;

         } 
            
        }
        
        if(empty($event_date)) {
            
            $event_date = get_post_meta($ID, 'WooCommerceEventsDate', true);
            
        }
        
        return $event_date;
        
    }
    
    /**
     * Gets the multiday status for an event
     * 
     * @param int $ID
     * @return array
     */
    public function get_multiday_status($ID) {
        
        $WooCommerceEventsMultidayStatus = get_post_meta($ID, 'WooCommerceEventsMultidayStatus', true);

        if(!empty($WooCommerceEventsMultidayStatus) && !is_array($WooCommerceEventsMultidayStatus)) {

            $WooCommerceEventsMultidayStatus = json_decode($WooCommerceEventsMultidayStatus, true);
         
        } else {
            
            $WooCommerceEventsMultidayStatus = array();
            
        }       
        
        return $WooCommerceEventsMultidayStatus;
        
    }
    
    /**
     * Get the multiday checkins for day 
     * 
     * @param int $ID
     * @return array
     */
    public function get_multiday_check_in_times($ID) {
        
        $WooCommerceEventsMultidayCheckInTime = get_post_meta($ID, 'WooCommerceEventsMultidayCheckInTime', true);
        $WooCommerceEventsMultidayCheckInTimeReturn = array();
        
        if(!empty($WooCommerceEventsMultidayCheckInTime)) {
            
            $WooCommerceEventsMultidayCheckInTime = json_decode($WooCommerceEventsMultidayCheckInTime, true);
            
            foreach($WooCommerceEventsMultidayCheckInTime as $day => $time) {
                
                $WooCommerceEventsMultidayCheckInTimeReturn[$day] = date_i18n(get_option('date_format').' '.get_option('time_format').' (P)', $time);
                
            }
         
        }
        
        return $WooCommerceEventsMultidayCheckInTimeReturn;
        
    }
    
    /**
     * Displays the multi day meta on the ticket detail screen
     * 
     * @param type $ID
     * @return string
     */
    public function display_multiday_status_ticket_meta_all($ID) {
        
        ob_start();
        
        $WooCommerceEventsMultidayStatus = $this->get_multiday_status($ID);

        $WooCommerceEventsProductID = get_post_meta($ID, 'WooCommerceEventsProductID', true);
        $WooCommerceEventsStatus = get_post_meta($ID, 'WooCommerceEventsStatus', true);
        $WooCommerceEventsNumDays = get_post_meta($WooCommerceEventsProductID, 'WooCommerceEventsNumDays', true);
        
        if (empty($WooCommerceEventsNumDays)) {
            
            $WooCommerceEventsNumDays = 1;
            
        }
        
        for($x = 1; $x <= $WooCommerceEventsNumDays; $x++) {
            
            if(empty($WooCommerceEventsMultidayStatus[$x])) {
                
                $WooCommerceEventsMultidayStatus[$x] = 'Not Checked In';
                
            }

        }

        ksort($WooCommerceEventsMultidayStatus);
        
        $dayTerm = get_post_meta($WooCommerceEventsProductID, 'WooCommerceEventsDayOverride', true);
        

        if(empty($dayTerm)) {

            $dayTerm = get_option('WooCommerceEventsDayOverride', true);

        }

        if(empty($dayTerm) || $dayTerm == 1) {

            $dayTerm = __('Day', 'fooevents-multiday-events');

        }

        if(!empty($WooCommerceEventsMultidayStatus) && $WooCommerceEventsStatus != 'Unpaid' && $WooCommerceEventsStatus != 'Canceled' && $WooCommerceEventsStatus != 'Cancelled') {
            
            require($this->Config->templatePath.'display-multiday-status-ticket-meta-all.php');
            
        }
        
        $multiday_status = ob_get_clean();

        return $multiday_status;

    }
    
    /**
     * Output multi-day checking times
     * 
     * @post int ID
     * @return string
     */
    public function display_multiday_check_in_times($ID) {
        
        ob_start();
        
        $WooCommerceEventsMultidayCheckInTime = $this->get_multiday_check_in_times($ID);
        $WooCommerceEventsProductID = get_post_meta($ID, 'WooCommerceEventsProductID', true);
        $WooCommerceEventsNumDays = get_post_meta($WooCommerceEventsProductID, 'WooCommerceEventsNumDays', true);

        if (empty($WooCommerceEventsNumDays)) {
            
            $WooCommerceEventsNumDays = 1;
            
        }
        
        for($x = 1; $x <= $WooCommerceEventsNumDays; $x++) {
            
            if(empty($WooCommerceEventsMultidayCheckInTime[$x])) {
                
                $WooCommerceEventsMultidayCheckInTime[$x] = '';
                
            }

        }

        ksort($WooCommerceEventsMultidayCheckInTime);
        
        $dayTerm = get_post_meta($WooCommerceEventsProductID, 'WooCommerceEventsDayOverride', true);

        if(empty($dayTerm)) {

            $dayTerm = get_option('WooCommerceEventsDayOverride', true);

        }

        if(empty($dayTerm)) {

            $dayTerm = __('Day', 'fooevents-multiday-events');

        }
 
        require($this->Config->templatePath.'display-multiday-check_in_times.php');
        
        $multiday_times = ob_get_clean();

        return $multiday_times;
        
    }

    /**
     * Gets an array of check-ins, used in the CSV export
     * 
     * @param int $ID
     * @param int $WooCommerceEventsNumDays
     */
    public function get_array_of_check_ins($ID, $WooCommerceEventsNumDays) {
        
        if(empty($WooCommerceEventsNumDays)) {
            
            $WooCommerceEventsNumDays = 1;
            
        }
        
        $WooCommerceEventsMultidayStatus = $this->get_multiday_status($ID);
        
        if(empty($WooCommerceEventsMultidayStatus)) {
            
            for($x = 1; $x <= $WooCommerceEventsNumDays; $x++) {
                
                $WooCommerceEventsMultidayStatus[$x] = 'Not Checked In';
                
            }
            
        }
        
        for($x = 1; $x <= $WooCommerceEventsNumDays; $x++) {
            
            if(empty($WooCommerceEventsMultidayStatus[$x])) {
                
                $WooCommerceEventsMultidayStatus[$x] = 'Not Checked In';
                
            }
            
        }
        
        $WooCommerceEventsMultidayStatusProcessed = array();
        
        foreach($WooCommerceEventsMultidayStatus as $day => $value) {
            
            if($day <= $WooCommerceEventsNumDays) {
                
                $WooCommerceEventsMultidayStatusProcessed[sprintf(__( 'Day %s', 'woocommerce-events' ), $day)] = $value;
                
            }

        }
        
        return $WooCommerceEventsMultidayStatusProcessed;
        
    }
    
    /**
     * Processes muli-day functionality with express check-ins
     * 
     * @param int $ID
     * @param string $multiday
     * @param string $day
     * @return string
     */
    public function display_multiday_status_ticket_meta($ID, $multiday, $day) {
        
        ob_start();
        
        $WooCommerceEventsMultidayStatus = $this->get_multiday_status($ID);
        
        $WooCommerceEventsProductID = get_post_meta($ID, 'WooCommerceEventsProductID', true);
        $WooCommerceEventsStatus = get_post_meta($ID, 'WooCommerceEventsStatus', true);
        $WooCommerceEventsNumDays = get_post_meta($WooCommerceEventsProductID, 'WooCommerceEventsNumDays', true);

        if (empty($WooCommerceEventsNumDays)) {
            
            $WooCommerceEventsNumDays = 1;
            
        }

        if(empty($WooCommerceEventsMultidayStatus[$day])) {
            
            //$WooCommerceEventsMultidayStatus[$day] = 'Not Checked In';
            return;
            
        }
        
        ksort($WooCommerceEventsMultidayStatus);
        
        if(!empty($WooCommerceEventsMultidayStatus) && $WooCommerceEventsStatus != 'Unpaid' && $WooCommerceEventsStatus != 'Canceled' && $WooCommerceEventsStatus != 'Cancelled') {
            
            $dayTerm = get_post_meta($WooCommerceEventsProductID, 'WooCommerceEventsDayOverride', true);
            
            if(empty($dayTerm)) {

                $dayTerm = get_option('WooCommerceEventsDayOverride', true);

            }

            if(empty($dayTerm) || $dayTerm == 1) {

                $dayTerm = __('Day', 'fooevents-multiday-events');

            }
            
            require($this->Config->templatePath.'display-multiday-status-ticket-meta.php');
            
        }
        
        $multiday_status = ob_get_clean();

        return $multiday_status;
        
    }
    
    /**
     * Processes muli-day functionality with express check-ins
     * 
     * @param int $ID
     * @param string $multiday
     * @param string $day
     * @return string
     */
    public function display_multiday_status_ticket_meta_day($ID, $multiday, $day) {
        
        $WooCommerceEventsMultidayStatus = $this->get_multiday_status($ID);
        $WooCommerceEventsProductID = get_post_meta($ID, 'WooCommerceEventsProductID', true);
        $WooCommerceEventsStatus = get_post_meta($ID, 'WooCommerceEventsStatus', true);
        $WooCommerceEventsNumDays = get_post_meta($WooCommerceEventsProductID, 'WooCommerceEventsNumDays', true);

        if(empty($WooCommerceEventsNumDays)) {
            
            $WooCommerceEventsNumDays = 1;
            
        }
        
        if(empty($WooCommerceEventsMultidayStatus[$day])) {
            
            $WooCommerceEventsMultidayStatus[$day] = 'Not Checked In';
            
        }
        
        return $WooCommerceEventsMultidayStatus[$day];
 
    }
    
    /**
     * Undoes autocompleted check-ins in the express check-in plugin
     * 
     * @param int $ID
     * @param string $multiday
     * @param string $day
     */
    public function undo_express_check_in_status_auto_complete($ID, $multiday, $day) {
        
        $WooCommerceEventsMultidayStatus = get_post_meta($ID, 'WooCommerceEventsMultidayStatus', true);
        $WooCommerceEventsMultidayStatus[$day] = 'Not Checked In';

        update_post_meta($ID, 'WooCommerceEventsMultidayStatus', $WooCommerceEventsMultidayStatus);
        
    }
    
    /**
     * Capture multi-day status
     * 
     * @param int $post_ID
     */
    public function capture_multiday_status_ticket_meta($post_ID) {
        
        global $wpdb;
        $table_name = $wpdb->prefix . 'fooevents_check_in';
        
        if (isset($_POST) && isset($_POST['ticket_status']) && $_POST['ticket_status'] == 'true' && isset($_POST['WooCommerceEventsStatusMultidayEvent'])) {
            
            $WooCommerceEventsMultidayStatus_original = get_post_meta($post_ID, 'WooCommerceEventsMultidayStatus', true);
            $WooCommerceEventsProductID = get_post_meta($post_ID, 'WooCommerceEventsProductID', true);
            $WooCommerceEventsMultidayStatus_original = json_decode($WooCommerceEventsMultidayStatus_original, true);
            $WooCommerceEventsMultidayStatus = array_map('sanitize_text_field', $_POST['WooCommerceEventsStatusMultidayEvent']);
            $WooCommerceEventsMultidayStatus_encoded = json_encode($WooCommerceEventsMultidayStatus);
            
            $dates_to_checkin = array();
            
            if(!empty($WooCommerceEventsMultidayStatus_original)) {

                foreach($WooCommerceEventsMultidayStatus_original as $k => $v) {

                    if($WooCommerceEventsMultidayStatus_original[$k] != $WooCommerceEventsMultidayStatus[$k]) {

                        $dates_to_checkin[$k] = $WooCommerceEventsMultidayStatus[$k];

                    }

                }

            } else {
                
                $dates_to_checkin = $WooCommerceEventsMultidayStatus;
                
            }

            foreach($dates_to_checkin as $k => $v) {

                if($v == 'Checked In') {
                    
                    $timestamp = current_time('timestamp');
                    $wpdb->insert($table_name, array(
                            'tid' => $post_ID,
                            'eid' => $WooCommerceEventsProductID,
                            'day' => $k,
                            'checkin' => $timestamp
                        ));
                    
                }
                
            }

            update_post_meta( $post_ID, 'WooCommerceEventsMultidayStatus', $WooCommerceEventsMultidayStatus_encoded); 

        }

    }
    
    /**
     * Displays multi-day settings in the express check-in plugin
     * 
     * @return string
     */
    public function display_multiday_express_check_in_options() {
        
        ob_start();
        
        require($this->Config->templatePath.'display-multiday-express-check-in-options.php');
        
        $multiday_options = ob_get_clean();

        return $multiday_options;
        
    }
    
    /**
     * Processes checkings the express check-in plugin
     * 
     * @param int $ID
     * @param string $update_value
     * @param string $multiday
     * @param string $day
     */
    public function update_express_check_in_status($ID, $update_value, $multiday, $day) {
        
        global $wpdb;
        $table_name = $wpdb->prefix . 'fooevents_check_in';
        
        if($multiday) {
            
            $timestamp = current_time('timestamp');
            $day = sanitize_text_field($day);
            $WooCommerceEventsMultidayStatus = get_post_meta($ID, "WooCommerceEventsMultidayStatus", true);
            $eventID = get_post_meta($ID, 'WooCommerceEventsProductID', true);
            $WooCommerceEventsMultidayStatus = json_decode($WooCommerceEventsMultidayStatus, true);

            if(!is_array($WooCommerceEventsMultidayStatus)) {
                
                $WooCommerceEventsMultidayStatus = array();
                
            }

            if($update_value == 'Checked In') {
                
                $WooCommerceEventsMultidayCheckInTime[$day] = $timestamp;
                
                $wpdb->insert($table_name, array(
                    'tid' => $ID,
                    'eid' => $eventID,
                    'day' => $day,
                    'checkin' => $timestamp
                ));

            }

            $WooCommerceEventsMultidayStatus[$day] = sanitize_text_field($update_value);
            $WooCommerceEventsMultidayStatus = json_encode($WooCommerceEventsMultidayStatus);
            update_post_meta($ID, 'WooCommerceEventsMultidayStatus', $WooCommerceEventsMultidayStatus);

            return true;
            
        }        

    }
    
    /**
     * Processes auto check-ins in the express check-in plugin. NB: USING update_express_check_in_status() RATHER
     * 
     * @param int $ID
     * @param string $update_value
     * @param string $multiday
     * @param string $day
     * @return boolean
     */
    public function update_express_check_in_status_auto_complete($ID, $update_value, $multiday, $day) {
        
        /*if($multiday) {
            
            $ID = sanitize_text_field($ID);
            $day = sanitize_text_field($day);
            
            $WooCommerceEventsMultidayStatus = get_post_meta($ID, "WooCommerceEventsMultidayStatus", true);
            
             if(!is_array($WooCommerceEventsMultidayStatus)) {
                
                $WooCommerceEventsMultidayStatus = array();
                
            }
            
            if(!empty($WooCommerceEventsMultidayStatus[$day])) {
                
                if($WooCommerceEventsMultidayStatus[$day] == 'Checked In') {
                    
                    return false;
                    
                }
                
            }
            
            $WooCommerceEventsMultidayStatus[$day] = sanitize_text_field($update_value);
            
            update_post_meta($ID, 'WooCommerceEventsMultidayStatus', $WooCommerceEventsMultidayStatus);
            
            return true;
            
        }*/
        
    }
    
    /**
     * Displays the multi-day form in the ticket status meta box
     * 
     * @param int $ID
     * @return string
     */
    public function display_multiday_status_ticket_form_meta($ID) {
        
        ob_start();

        $WooCommerceEventsMultidayStatus = $this->get_multiday_status($ID);
        
        $WooCommerceEventsProductID = get_post_meta($ID, 'WooCommerceEventsProductID', true);
        $WooCommerceEventsNumDays = get_post_meta($WooCommerceEventsProductID, 'WooCommerceEventsNumDays', true);
        $WooCommerceEventsStatus = '';
        
        if (empty($WooCommerceEventsNumDays)) {
            
            $WooCommerceEventsNumDays = 1;
            
        }
        
        for($x = 1; $x <= $WooCommerceEventsNumDays; $x++) {
            
            if(empty($WooCommerceEventsMultidayStatus[$x])) {
                
                $WooCommerceEventsMultidayStatus[$x] = 'Not Checked In';
                
            }
            
        }
        
        ksort($WooCommerceEventsMultidayStatus);

        $dayTerm = get_post_meta($WooCommerceEventsProductID, 'WooCommerceEventsDayOverride', true);

        if(empty($dayTerm)) {

            $dayTerm = get_option('WooCommerceEventsDayOverride', true);

        }

        if(empty($dayTerm) || $dayTerm == 1) {

            $dayTerm = __('Day', 'woocommerce-events');

        }
        
        require($this->Config->templatePath.'display-multiday-selection-form.php');
        
        $display_multiday_selection_form = ob_get_clean();
        
        return $display_multiday_selection_form;
        
    }
    
    /**
     * Returns the event end date
     * 
     * @param in $ID
     * @return string
     */
    public function get_end_date($ID) {
        
        return get_post_meta($ID, 'WooCommerceEventsEndDate', true);
        
    }
    
    
    /**
     * Returns the multi-day type
     * 
     * @param type $ID
     */
    public function get_multi_day_type($ID) {

        return get_post_meta($ID, 'WooCommerceEventsType', true);
        
    }
    
    /**
     * Returns the multi-day status
     * 
     * @param type $ID
     * @return type
     */
    public function get_multi_day_selected_dates($ID) {

        return get_post_meta($ID, 'WooCommerceEventsSelectDate', true);

    }
    
    /**
     * Formats the end date for calendar display
     * 
     * @param int $ID
     * @return string
     */
    public function format_end_date($ID, $end_date = true, $list_view = false) {
        
        $event_end_date = '';
        if($end_date) {
            
            $event_end_date = get_post_meta($ID, 'WooCommerceEventsEndDate', true);
            
        } else {
            
            $event_end_date = get_post_meta($ID, 'WooCommerceEventsDate', true);
            
        }

        $event_hour = get_post_meta($ID, 'WooCommerceEventsHourEnd', true);
        $event_minutes = get_post_meta($ID, 'WooCommerceEventsMinutesEnd', true);
        $event_period = get_post_meta($ID, 'WooCommerceEventsEndPeriod', true);
        
        if($list_view == 'calendar') {
            
            $list_view = false;
            
        }

        if($list_view == false) {
            
            $event_end_date = $event_end_date.' '.'23'.':'.'00';
            
        } else {

            $event_end_date = $event_end_date.' '.$event_hour.':'.$event_minutes.$event_period;
            
        }

        $event_end_date = str_replace('/', '-', $event_end_date);
        $event_end_date = str_replace(',', '', $event_end_date);
        $event_end_date = date('Y-m-d H:i:s', strtotime($event_end_date));

        $globalFooEventsAllDayEvent = get_option( 'globalFooEventsAllDayEvent' );
        if($globalFooEventsAllDayEvent == 'yes') {

            $event_end_date = date('Y-m-d H:i:s', strtotime($event_end_date . ' +1 day'));

        }
        
        $event_end_date = str_replace(' ', 'T', $event_end_date);
        
        return $event_end_date;
        
    }
     
    public function process_events_calendar($events, $attributes = array()) {

        $processed_events = array();
        
        $x = 0;
        
        if(!empty($events['events'])) {
        
            foreach($events['events'] as $key => $event) {

                if(isset($event['multi_day']) && $event['multi_day'] == 'selected') {
                    $processed_events['events'][$x] = $event;
                    $x++;
                    continue;

                }

                if(!empty($event['unformated_date']) && !empty($event['unformated_end_date'])) {

                    $processed_events['events'][$x] = $event;

                    if($event['unformated_date'] != $event['unformated_end_date']) {

                        if(isset($attributes['defaultView']) && $attributes['defaultView'] == 'listWeek') {

                            $end = $this->format_end_date($event['post_id'], false, true);
                            $processed_events['events'][$x]['end'] = $end;
                            $num_days = get_post_meta($event['post_id'], 'WooCommerceEventsNumDays', true);

                            for($i = 1; $i<$num_days; $i++) {

                                $x++;
                                $datetimeStart = new DateTime($event['start']);
                                $datetimeStart->modify('+'.$i.' day');
                                $processedStart = $datetimeStart->format('Y-m-d H:i:s');

                                $datetimeEnd = new DateTime($end);
                                $datetimeEnd->modify('+'.$i.' day');
                                $processedEnd = $datetimeEnd->format('Y-m-d H:i:s');

                                $processed_events['events'][$x] = $event;
                                $processed_events['events'][$x]['start'] = $processedStart;

                                $processed_events['events'][$x]['end'] = $processedEnd;


                            }

                        }

                    }


                } else {

                    $processed_events['events'][$x] = $event;

                }

                $x++;

            }
        
        }
        
        return $processed_events;
        
    }
    
    /**
    * Format array for the datepicker
    *
    * WordPress stores the locale information in an array with a alphanumeric index, and
    * the datepicker wants a numerical index. This function replaces the index with a number
    * 
    * @param array $ArrayToStrip
    * @param return $NewArray
    */
    private function _strip_array_indices($ArrayToStrip) {
        
        foreach($ArrayToStrip as $objArrayItem) {
            
            $NewArray[] =  $objArrayItem;
            
        }

        return($NewArray);
        
    }
    
    /**
    * Convert the php date format string to a js date format
    * 
    * @param string $sFormat
    */
    private function _date_format_php_to_js($sFormat) {

        switch( $sFormat ) {
            //Predefined WP date formats
            case 'D d-m-y':
            return( 'D dd-mm-yy' );
            break;
            
            case 'D d-m-Y':
            return( 'D dd-mm-yy' );
            break;
            
            case 'l d-m-Y':
            return( 'DD dd-mm-yy' );
            break;
        
            case 'jS F Y':
            return( 'd MM, yy' );
            break;
        
            case 'F j, Y':
            return( 'MM dd, yy' );
            break;
        
            case 'j F Y':
            return( 'd MM yy' );
            break;
        
            case 'Y/m/d':
            return( 'yy/mm/dd' );
            break;
        
            case 'm/d/Y':
            return( 'mm/dd/yy' );
            break;
        
            case 'd/m/Y':
            return( 'dd/mm/yy' );
            break;
        
            case 'Y-m-d':
            return( 'yy-mm-dd' );
            break;
        
            case 'm-d-Y':
            return( 'mm-dd-yy' );
            break;
        
            case 'd-m-Y':
            return( 'dd-mm-yy' );
            break;
        
            case 'j. FY':
            return( 'd. MMyy' );
            break;

            case 'j. F Y':
            return( 'd. MM yy' );
            break;

            case 'j \d\e F \d\e Y':
            return( "d 'de' MM 'de' yy" );
            break;
        
            default:
            return( 'yy-mm-dd' );
        }
        
    }
    
    private function num_days_between_two_dates($start, $end) {
        
        $start = $this->convert_month_to_english($start);
        $end = $this->convert_month_to_english($end);
        
        $start = strtotime($start);
        $end = strtotime($end);
        
        $datediff = $end - $start;
        
        return round($datediff / (60 * 60 * 24));
        
    }
    
    /**
     * Array of month names for translation to English
     * 
     * @param string $event_date
     * @return string
     */
    private function convert_month_to_english($event_date) {
        
        $months = array(
            // French
            'janvier' => 'January',
            'février' => 'February',
            'mars' => 'March',
            'avril' => 'April',
            'mai' => 'May',
            'juin' => 'June',
            'juillet' => 'July',
            'aout' => 'August',
            'août' => 'August',
            'septembre' => 'September',
            'octobre' => 'October',

            // German
            'Januar' => 'January',
            'Februar' => 'February',
            'März' => 'March',
            'Mai' => 'May',
            'Juni' => 'June',
            'Juli' => 'July',
            'Oktober' => 'October',
            'Dezember' => 'December',

            // Spanish
            'enero' => 'January',
            'febrero' => 'February',
            'marzo' => 'March',
            'abril' => 'April',
            'mayo' => 'May',
            'junio' => 'June',
            'julio' => 'July',
            'agosto' => 'August',
            'septiembre' => 'September',
            'setiembre' => 'September',
            'octubre' => 'October',
            'noviembre' => 'November',
            'diciembre' => 'December',
            'novembre' => 'November',
            'décembre' => 'December',

            // Dutch
            'januari' => 'January',
            'februari' => 'February',
            'maart' => 'March',
            'april' => 'April',
            'mei' => 'May',
            'juni' => 'June',
            'juli' => 'July',
            'augustus' => 'August',
            'september' => 'September',
            'oktober' => 'October',
            'november' => 'November',
            'december' => 'December',

            // Italian
            'Gennaio' => 'January',
            'Febbraio' => 'February',
            'Marzo' => 'March',
            'Aprile' => 'April',
            'Maggio' => 'May',
            'Giugno' => 'June',
            'Luglio' => 'July',
            'Agosto' => 'August',
            'Settembre' => 'September',
            'Ottobre' => 'October',
            'Novembre' => 'November',
            'Dicembre' => 'December',

            // Polish
            'Styczeń' => 'January',
            'Luty' => 'February',
            'Marzec' => 'March',
            'Kwiecień' => 'April',
            'Maj' => 'May',
            'Czerwiec' => 'June',
            'Lipiec' => 'July',
            'Sierpień' => 'August',
            'Wrzesień' => 'September',
            'Październik' => 'October',
            'Listopad' => 'November',
            'Grudzień' => 'December',

            // Afrikaans
            'Januarie' => 'January',
            'Februarie' => 'February',
            'Maart' => 'March',
            'Mei' => 'May',
            'Junie' => 'June',
            'Julie' => 'July',
            'Augustus' => 'August',
            'Oktober' => 'October',
            'Desember' => 'December',

            // Turkish
            'Ocak' => 'January',
            'Şubat' => 'February',
            'Mart' => 'March',
            'Nisan' => 'April',
            'Mayıs' => 'May',
            'Haziran' => 'June',
            'Temmuz' => 'July',
            'Ağustos' => 'August',
            'Eylül' => 'September',
            'Ekim' => 'October',
            'Kasım' => 'November',
            'Aralık' => 'December',

            // Portuguese
            'janeiro' => 'January',
            'fevereiro' => 'February',
            'março' => 'March',
            'abril' => 'April',
            'maio' => 'May',
            'junho' => 'June',
            'julho' => 'July',
            'agosto' => 'August',
            'setembro' => 'September',
            'outubro' => 'October',
            'novembro' => 'November',
            'dezembro' => 'December',

            // Swedish
            'Januari' => 'January',
            'Februari' => 'February',
            'Mars' => 'March',
            'April' => 'April',
            'Maj' => 'May',
            'Juni' => 'June',
            'Juli' => 'July',
            'Augusti' => 'August',
            'September' => 'September',
            'Oktober' => 'October',
            'November' => 'November',
            'December' => 'December',

            // Czech
            'leden' => 'January',
            'únor' => 'February',
            'březen' => 'March',
            'duben' => 'April',
            'květen' => 'May',
            'červen' => 'June',
            'červenec' => 'July',
            'srpen' => 'August',
            'září' => 'September',
            'říjen' => 'October',
            'listopad' => 'November',
            'prosinec' => 'December',

            // Norwegian
            'januar' => 'January',
            'februar' => 'February',
            'mars' => 'March',
            'april' => 'April',
            'mai' => 'May',
            'juni' => 'June',
            'juli' => 'July',
            'august' => 'August',
            'september' => 'September',
            'oktober' => 'October',
            'november' => 'November',
            'desember' => 'December',

            // Danish
            'januar' => 'January',
            'februar' => 'February',
            'marts' => 'March',
            'april' => 'April',
            'maj' => 'May',
            'juni' => 'June',
            'juli' => 'July',
            'august' => 'August',
            'september' => 'September',
            'oktober' => 'October',
            'november' => 'November',
            'december' => 'December',

            // Finnish
            'tammikuu' => 'January',
            'helmikuu' => 'February',
            'maaliskuu' => 'March',
            'huhtikuu' => 'April',
            'toukokuu' => 'May',
            'kesäkuu' => 'June',
            'heinäkuu' => 'July',
            'elokuu' => 'August',
            'syyskuu' => 'September',
            'lokakuu' => 'October',
            'marraskuu' => 'November',
            'joulukuu' => 'December',

            // Russian
            'Январь' => 'January', 
            'Февраль' => 'February', 
            'Март' => 'March', 
            'Апрель' => 'April', 
            'Май' => 'May', 
            'Июнь' => 'June', 
            'Июль' => 'July', 
            'Август' => 'August', 
            'Сентябрь' => 'September', 
            'Октябрь' => 'October', 
            'Ноябрь' => 'November', 
            'Декабрь' => 'December',

            // Icelandic
            'Janúar' => 'January', 
            'Febrúar' => 'February', 
            'Mars' => 'March', 
            'Apríl' => 'April', 
            'Maí' => 'May', 
            'Júní' => 'June', 
            'Júlí' => 'July', 
            'Ágúst' => 'August', 
            'September' => 'September', 
            'Oktober' => 'October', 
            'Nóvember' => 'November', 
            'Desember' => 'December',

            // Latvian
            'janvāris' => 'January',
            'februāris' => 'February',
            'marts' => 'March',
            'aprīlis' => 'April',
            'maijs' => 'May',
            'jūnijs' => 'June',
            'jūlijs' => 'July',
            'augusts' => 'August',
            'septembris' => 'September',
            'oktobris' => 'October',
            'novembris' => 'November',
            'decembris' => 'December',

            // Lithuanian
            'Sausis' => 'January',
            'Vasaris' => 'February',
            'Kovas' => 'March',
            'Balandis' => 'April',
            'Gegužė' => 'May',
            'Birželis' => 'June',
            'Liepa' => 'July',
            'Rugpjūtis' => 'August',
            'Rugsėjis' => 'September',
            'Spalis' => 'October',
            'Lapkritis' => 'November',
            'Gruodis' => ' December',

            // Greek
            'Ιανουάριος' => 'January',
            'Φεβρουάριος' => 'February',
            'Μάρτιος' => 'March',
            'Απρίλιος' => 'April',
            'Μάιος' => 'May',
            'Ιούνιος' => 'June',
            'Ιούλιος' => 'July',
            'Αύγουστος' => 'August',
            'Σεπτέμβριος' => 'September',
            'Οκτώβριος' => 'October',
            'Νοέμβριος' => 'November',
            'Δεκέμβριος' => 'December',

            // Slovak - Slovakia
            "január" => "January",
            "február" => "February",
            "marec" => "March",
            "apríl" => "April",
            "máj" => "May",
            "jún" => "June",
            "júl" => "July",
            "august" => "August",
            "september" => "September",
            "október" => "October",
            "november" => "November",
            "december" => "December",
        );
        
        $pattern = array_keys($months);
        $replacement = array_values($months);

        foreach ($pattern as $key => $value) {
            $pattern[$key] = '/\b'.$value.'\b/i';
        }
        
        $replaced_event_date = preg_replace($pattern, $replacement, $event_date);

        $replaced_event_date = str_replace(' de ', ' ', $replaced_event_date);

        return $replaced_event_date;
        
    }
    
    /**
    * Checks if a plugin is active.
    * 
    * @param string $plugin
    * @return boolean
    */
    private function is_plugin_active($plugin) {

        return in_array( $plugin, (array) get_option( 'active_plugins', array() ) );

    }
    
}

$Fooevents_Multiday_Events = new Fooevents_Multiday_Events();