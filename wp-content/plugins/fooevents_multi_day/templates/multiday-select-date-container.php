<div class="options_group" id ="WooCommerceEventsSelectDateContainer">
    <?php if(!empty($WooCommerceEventsSelectDate)) :?>
    <?php $x = 1; ?>
    <?php foreach($WooCommerceEventsSelectDate as $eventDate) :?>
    <p class="form-field">
        <label><?php echo $dayTerm; ?> <?php echo $x; ?></label>
        <input type="text" class="WooCommerceEventsSelectDate" name="WooCommerceEventsSelectDate[]" value="<?php echo esc_attr($eventDate); ?>"/>
    </p>
    <?php $x++; ?>
    <?php endforeach; ?>
    <?php endif; ?>
</div>