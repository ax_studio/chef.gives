<p class="form-field fooevents-custom-text-inputs">
    <label><?php _e('Day:', 'fooevents-multiday-events'); ?></label>
    <input type="text" id="WooCommerceEventsDayOverride" name="WooCommerceEventsDayOverride" value="<?php echo esc_attr($WooCommerceEventsDayOverride); ?>"/>
    <input type="text" id="WooCommerceEventsDayOverridePlural" name="WooCommerceEventsDayOverridePlural" value="<?php echo esc_attr($WooCommerceEventsDayOverridePlural); ?>"/>
    <img class="help_tip" data-tip="<?php _e("Change 'Day' to your own custom text for this event.", 'woocommerce-events'); ?>" src="<?php echo plugins_url(); ?>/woocommerce/assets/images/help.png" height="16" width="16" />
</p>