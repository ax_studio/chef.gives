<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package AX_studio
 */

?>
<!doctype html>

<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <link rel="profile" href="https://gmpg.org/xfn/11">

    <?php wp_head(); ?>
    <script
            src="https://code.jquery.com/jquery-3.5.1.min.js"
            integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
            crossorigin="anonymous"></script>

</head>

<body <?php body_class(); ?>>
<div class="site" id="butter">

    <header id="header" class="header">
        <div class="header-toggle">
            <div class="header-toggle_menu">
                <span class="menu-cross"></span>
            </div>
        </div>
        <div class="header-branding">
            <?php the_custom_logo(); ?>
        </div>

        <div class="header-title">
            <img src="https://chef.gives/wp-content/uploads/2021/02/CHEF-text-short.png"/>
        </div>

        <div id="header-navigation" class="header-navigation">
            <div class="row header-navigation_container">
                <div class="header-navigation__nav_main col-12 col-md-2 offset-md-2">
                    <?php
                    wp_nav_menu(array(
                        'theme_location' => 'primary',
                        'menu_id' => 'header-menu',
                    ));
                    ?>
                </div>
                <div class="header-navigation__image col-md-5 col-xs-6 center-vertically">

                    <?php echo do_shortcode('[elementor-template id="1213"]'); ?>
                </div>
                <div class="header-navigation__nav_bottom col-12">
                    <?php
                    wp_nav_menu(array(
                        'theme_location' => 'footer',
                        'menu_id' => 'footer-menu',
                    )); ?>
                </div>
            </div>
        </div>

        <?php if (in_array('woocommerce/woocommerce.php', apply_filters('active_plugins', get_option('active_plugins')))) {

            $count = WC()->cart->cart_contents_count;
            ?>
            <div class="header-cart ">
                <a class="cart-contents" id="open-modal-cart"><?php
                    if ($count > 0) {
                        ?>
                        <i class="las la-shopping-bag"></i>

                        <span class="cart-contents-count"><?php echo esc_html($count); ?></span>
                        <?php
                    }
                    ?></a>
            </div>

            <div class="header-cart-modal">
                <?php echo do_shortcode( '[elementor-template id="1279"]' ); ?>
                <?php echo do_shortcode( '[elementor-template id="1924"]' ); ?>
            </div>

        <?php } ?>



        <div class="header-give">
            <div class="header-give_container">
                <span><a href="<?php echo site_url(); ?>/donate">Donate</a></span>
            </div>

        </div>


    </header>

    <div class="site-content">
