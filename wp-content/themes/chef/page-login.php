<?php

/*
Template Name: AX - Login Page
*/

get_header();
?>
<?php if (!is_user_logged_in()) { ?>

    <main class="login">

        <section>
            <div class="login-container">
                <div class="padding-b-default padding-t-30">
                    <h1 class="text-center">Log In</h1>
                    <br/>


                    <?php
                    $args = array(
                        'redirect' => home_url() . '/wp-admin/',
                        'form_id' => 'loginform-custom',
                        'label_username' => __('Email Address'),
                        'label_password' => __('Password'),
                        'label_remember' => __('Remember Me'),
                        'label_log_in' => __('Log In'),
                        'remember' => true
                    ); ?>

                    <?php wp_login_form($args); ?>
                    <p class="small text-align-center"><a href="/forgot-password">Lost your
                            password?</a></p>

                </div>
            </div>

        </section>

    </main>
<?php } ?>

<?php if (is_user_logged_in()) { ?>
    <?php wp_redirect(site_url() . '/wp-admin/', 301);
    exit; ?>

<?php } ?>

<?php
get_footer(); ?>
