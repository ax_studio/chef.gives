<?php
/**
 * The Template for displaying all single Give Forms.
 *
 * Override this template by copying it to yourtheme/give/single-give-forms.php
 *
 * @package       Give/Templates
 * @version       1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header(); ?>

<section>
    <div class="innovator-container">
        <div class="innovator-container_breadcrumbs">

        </div>
        <div class="innovator-container_wrapper">
            <p class="small innovator-container_wrapper_breadcrumb"><a href="<?php echo site_url(); ?>">Home</a> <span class="margin-lr-5"> > </span> <a href="<?php echo site_url(); ?>/innovators">Innovators</a> <span class="margin-lr-5"> > </span> <span style="color: #FFD090 !important;"><?php the_title(); ?></span></p>

            <?php

            /**
             * Fires in single form template, before the main content.
             *
             * Allows you to add elements before the main content.
             *
             * @since 1.0
             */
            do_action( 'give_before_main_content' );

            while ( have_posts() ) :
                the_post();

                give_get_template_part( 'single-give-form/content', 'single-give-form' );

            endwhile; // end of the loop.

            /**
             * Fires in single form template, after the main content.
             *
             * Allows you to add elements after the main content.
             *
             * @since 1.0
             */
            do_action( 'give_after_main_content' );

            /**
             * Fires in single form template, on the sidebar.
             *
             * Allows you to add elements to the sidebar.
             *
             * @since 1.0
             */
            do_action( 'give_sidebar' );
            ?>
        </div>
    </div>
</section>


<?php get_footer();
