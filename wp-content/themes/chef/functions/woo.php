<?php

add_filter( 'woocommerce_continue_shopping_redirect', 'my_changed_woocommerce_continue_shopping_redirect', 10, 1 );
function my_changed_woocommerce_continue_shopping_redirect( $return_to ){
    $page = '/innovators';
    $return_to = wc_get_page_permalink( 'shop' );

    return $return_to;
}


add_filter( 'wc_add_to_cart_mlsessage_html', 'my_changed_wc_add_to_cart_message_html', 10, 2 );
function my_changed_wc_add_to_cart_message_html($message, $products){

    if (strpos($message, 'Continue shopping') !== false) {
        $message = str_replace("Continue shopping", "Choose more Innovators", $message);
    }

    return $message;

}


function my_header_add_to_cart_fragment( $fragments ) {

    ob_start();
    $count = WC()->cart->cart_contents_count;
    ?><a class="cart-contents" id="open-modal-cart"><?php
    if ( $count > 0 ) {
        ?>
        <i class="las la-shopping-bag"></i>

        <span class="cart-contents-count"><?php echo esc_html( $count ); ?></span>
        <?php
    }
    ?></a><?php

    $fragments['a.cart-contents'] = ob_get_clean();

    return $fragments;
}
add_filter( 'woocommerce_add_to_cart_fragments', 'my_header_add_to_cart_fragment' );





// Edit Single Product Page Add to Cart

add_filter( 'woocommerce_product_single_add_to_cart_text', 'axstudio_custom_add_cart_button_single_product' );

function axstudio_custom_add_cart_button_single_product( $label ) {

    foreach( WC()->cart->get_cart() as $cart_item_key => $values ) {
        $product = $values['data'];
        if( get_the_ID() == $product->get_id() ) {
            $label = __('Already in Cart', 'woocommerce');
        }
    }

    return $label;

}

// Edit Loop Pages Add to Cart

add_filter( 'woocommerce_product_add_to_cart_text', 'axstudio_custom_add_cart_button_loop', 99, 2 );

function axstudio_custom_add_cart_button_loop( $label, $product ) {

    if ( $product->get_type() == 'simple' && $product->is_purchasable() && $product->is_in_stock() ) {

        foreach( WC()->cart->get_cart() as $cart_item_key => $values ) {
            $_product = $values['data'];
            if( get_the_ID() == $_product->get_id() ) {
                $label = __('Already in Cart', 'woocommerce');
            }
        }

    }

    return $label;

}




add_filter( 'woocommerce_thankyou_order_received_text', 'axstudio_thank_you' );
function axstudio_thank_you() {
    $added_text = '<h2>Thank you for your support.</h2>';
    return $added_text ;
}



function ace_add_to_cart_message_html( $message, $products ) {

    $count = 0;
    $titles = array();
    foreach ( $products as $product_id => $qty ) {
        $titles[] = ( $qty > 1 ? absint( $qty ) . ' &times; ' : '' ) . sprintf( _x( '&ldquo;%s&rdquo;', 'Item name in quotes', 'woocommerce' ), strip_tags( get_the_title( $product_id ) ) );
        $count += $qty;
    }

    $titles     = array_filter( $titles );
    $added_text = sprintf( _n(
        'Your donation for %s is ready.', // Singular
        'Your donation for %s are ready.', // Plural
        $count, // Number of products added
        'woocommerce' // Textdomain
    ), wc_format_list_of_items( $titles ) );
    $message    = sprintf( '<a href="%s" class="button wc-forward">%s</a> %s', esc_url( wc_get_page_permalink( 'cart' ) ), esc_html__( 'View cart', 'woocommerce' ), esc_html( $added_text ) );


    return $message;
}

add_filter( 'wc_add_to_cart_message_html', 'ace_add_to_cart_message_html', 10, 2 );


add_filter( 'wc_add_to_cart_message_html', '__return_null' );

remove_action( 'woocommerce_before_single_product', 'woocommerce_output_all_notices', 10 );