function headerToggle() {
    $('.header-toggle').click(function() {
        $('.header-toggle_menu').toggleClass('active');
        if ($('.header-toggle_menu').hasClass('active')) {
            $('.header-navigation').addClass('active');
        } else {
            $('.header-navigation').removeClass('active');
        }
    });
}


function headerOnScroll() {
    $(window).scroll(function () {
        if ($(this).scrollTop() > 0) {
            $('.header').addClass('header-fixed');
        } else {
            $('.header').removeClass('header-fixed');
        }

    });
}

function cartModal() {
    $('.header-cart').click(function () {
        $('.header-cart-modal').addClass('active');
        // if ($(this).hasClass('active')) {
        //
        // } else {
        //
        // }
    });

    $('.modal-close').click(function () {
        $('.header-cart-modal').removeClass('active');
    });
}


function textareaAutoGrow() {


    $('textarea').attr('rows', '1');

    $('textarea').each(function () {
        this.setAttribute('style', 'height:' + (this.scrollHeight) + 'px;overflow-y:hidden;');
    }).on('input', function () {
        this.style.height = 'auto';
        this.style.height = (this.scrollHeight) + 'px';
    });
}

function buttonDisabled() {
    $('.single_add_to_cart_button').click(function () {
        if ($.cookie('the_cookie') == null) {
            //do your thing
            $.cookie('the_cookie', 'the_value', {expires: 1});
        } else if ($.cookie('the_cookie') != null) {
            e.preventDefault();
            $(".single_add_to_cart_button").attr("disabled", true);
        }

    });
}


function cookieBanner() {

    // Cookie Compliancy BEGIN
    function GetCookie(name) {
        var arg=name+"=";
        var alen=arg.length;
        var clen=document.cookie.length;
        var i=0;
        while (i<clen) {
            var j=i+alen;
            if (document.cookie.substring(i,j)==arg)
                return "here";
            i=document.cookie.indexOf(" ",i)+1;
            if (i==0) break;
        }
        return null;
    }
    function testFirstCookie(){
        var offset = new Date().getTimezoneOffset();
        if ((offset >= -180) && (offset <= 0)) { // European time zones
            var visit=GetCookie("cookieCompliancyAccepted");
            if (visit==null){
                $(".cookie-compliance").addClass('js-show-cookie-banner');	// Show warning
            } else {
                // Already accepted
            }
        }
    }
    $(document).ready(function(){
        $(".cookie-compliance__submit").click(function(){
            console.log('Understood');
            var expire=new Date();
            expire=new Date(expire.getTime()+7776000000);
            document.cookie="cookieCompliancyAccepted=here; expires="+expire+";path=/";
            $(".cookie-compliance").removeClass('js-show-cookie-banner');
        });
        testFirstCookie();
    });
// Cookie Compliancy END




}


function headerScrollAppear() {
    var didScroll;
    var lastScrollTop = 0;
    var delta = 0;
    var navbarHeight = $('.header').outerHeight();

    $(window).scroll(function (event) {
        didScroll = true;
    });

    setInterval(function () {
        if (didScroll) {
            hasScrolled();
            didScroll = false;
        }
    }, 300);

    function hasScrolled() {
        var st = $(this).scrollTop();

        if (!$('.header-toggle_menu').hasClass('active')) {
            // Make sure they scroll more than delta
            if (Math.abs(lastScrollTop - st) <= delta)
                return;

            // If they scrolled down and are past the navbar, add class .nav-up.
            // This is necessary so you never see what is "behind" the navbar.
            if (st > lastScrollTop && st > navbarHeight) {

                if (st > 300) {
                    $('.header').addClass('slideUp');
                } else {
                    $('.header').addClass('slideUp');
                }

                // Scroll Down

            } else {
                // Scroll Up
                if (st + $(window).height() < $(document).height()) {
                    $('.header').removeClass('slideUp');
                }
            }
        }

        lastScrollTop = st;
    }
}