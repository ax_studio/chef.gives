<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package AX_studio
 */

?>

</div>


<div class="cookie-compliance">
    <div class="container">
        <p>We use "cookies" (own or third parties authorized) for analytical purposes and to show you personalized
            advertising based on a profile of your web surfing habits (for example, visited pages). More information on
            our <a href="/cookies-policy/" class="text-decoration-underline">Cookie Policy</a>.</p>
        <div class="text-align-center">
            <a class="cookie-compliance__submit">Accept</a>
        </div>

    </div>
</div>
<footer class="footer">
    <div class="footer-logo">
        <img src="https://chef.gives/wp-content/uploads/2021/02/CHEF-text-short_black.png"/>
    </div>
    <div id="footer-navigation" class="footer-navigation">
        <?php
        wp_nav_menu(array(
            'theme_location' => 'footer',
            'menu_id' => 'footer-menu',
        ));
        ?>
    </div>

    <div class="footer-copyright">
        <p class="small">© <?php echo date("Y"); ?> C.H.E.F - All rights reserved. © Registered charity number 1193044.
            Website by <a href="https://axstudio.uk" target="_blank">AX Studio</a></p>
    </div>
<!--    <div class="footer_bottom">-->
<!--        <div class="footer-socialmedia">-->
<!--            <a href="https://www.facebook.com/Chef.gives/" target="_blank"><i class="fab fa-facebook-square"></i></a>-->
<!--            <a href="https://www.instagram.com/c_h_e_fund/" target="_blank"><i class="fab fa-instagram"></i></a>-->
<!--        </div>-->
<!--    </div>-->
</footer>
</div>

<?php wp_footer(); ?>

</body>
</html>
